import json
import os
import pathlib
import random
import string
import traceback
from datetime import datetime, timedelta
from django.db.models import Q
import requests
from django.shortcuts import redirect
import openpyxl
from chat_api import settings
from chat_api.settings import REDIRECT_BASE, FB_CLIENT_ID, VK_CLIENT_ID
from chat_api.settings import UPLOAD_PATH
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import hashlib
import uuid
from django.db.models.aggregates import Max
from django.db.models import Sum
from django.db.models import Count
from .models import *


class UsersVk():
    users = {}


class UsersFacebook():
    users = {}


def uuid():
    return f"{datetime.now().timestamp()}-" + "-".join(
        "".join(random.choices(string.ascii_letters, k=4)) for _ in range(4))


def have_all_fields(dictionary, keys):
    for key in keys:
        if key not in dictionary or not dictionary[key]:
            return False
    return True


def send_admin(text):
    try:
        token = '659999916:AAFsbqu-RTrsvg0sqgcwUi6q8bDdadfKW90'
        chat_id = '-317584812'
        bot = telebot.TeleBot(token)
        bot.send_message(chat_id, text, None, None)
    except:
        pass


class ApiException(Exception):
    pass


class AuthManager():

    @classmethod
    def check_user_token(cls, user_token):
        if user_token in [user.user_token for user in User.objects.all()]:
            return True
        raise ApiException("Wrong user_token")

    @classmethod
    def get_user_token(cls, login, password):
        try:
            user = User.objects.get(email=login, password=password)
        except Exception as error:
            raise ApiException(str(error))
        if not user.user_token:
            user.user_token = uuid()
            user.save()
        return user.user_token

    @classmethod
    def get_manager(cls, user_token, manager_id):
        cls.check_user_token(user_token)
        try:
            manager = Manager.objects.get(id=manager_id)
        except Exception as error:
            raise ApiException(str(error))
        if manager.user.user_token == user_token:
            return manager
        raise ApiException("Access denied")


class BaseApiMethod():
    methods = ("POST",)
    required_params = ()
    optional_params = ()

    def __init__(self):
        self.methods = self.__class__.methods
        self.required_params = self.__class__.required_params
        self.optional_params = self.__class__.optional_params

    def debug(func):
        def new_func(self, request):
            try:
                return func(self, request)
            except Exception:
                return HttpResponse(str(traceback.format_exc()))

        return new_func

    @csrf_exempt
    @debug
    def view(self, request):
        if request.method in self.methods:
            get_data = request.GET.dict()
            post_data = request.POST.dict()
            data = {**get_data, **post_data}

            if not have_all_fields(data, self.required_params):
                return HttpResponse(json.dumps({
                    "ok": False,
                    "desc": f"Not enough data {self.required_params}. Received {tuple(data.keys())}"
                }))

            for param in self.required_params + tuple(param for param in self.optional_params if param in data):
                if f"clear_{param}" in dir(self):
                    result = self.__getattribute__(f"clear_{param}")(data[param])
                    if not result["ok"]:
                        return HttpResponse(json.dumps({
                            "ok": False, **result
                        }))
                    data[param] = result["value"]

            if "clear" in dir(self):
                result = self.clear(data)
                if not result["ok"]:
                    return HttpResponse(json.dumps({
                        "ok": False, **result
                    }))
                data = result["value"]

            res = self.proccess(data)
            if isinstance(res, HttpResponseRedirect):
                return res
            return HttpResponse(json.dumps({**res}))

        return HttpResponse(json.dumps({
            "ok": False,
            "desc": f"Used unavailabled method {request.method}. Available: {self.methods}",
        }))

    def proccess(self, data):
        return {"ok": True}


class GetUserToken(BaseApiMethod):
    required_params = ("login", "password",)

    def proccess(self, data):
        try:
            user_token = AuthManager.get_user_token(data["login"], data["password"])
        except Exception as error:
            return {"ok": False, "desc": str(error)}
        return {"ok": True, "user_token": user_token}


class CreateUser(BaseApiMethod):
    required_params = ("login", "password",)
    optional_params = ("ref", 'utm_source')

    def proccess(self, data):
        try:
            user = User(email=data["login"], password=data["password"])
            user.user_token = uuid()
            utm = data.get('utm_source')
            if utm:
                user.utm_source = utm
            ref = data.get('ref')
            if ref.lower() != 'none':
                ref = User.objects.filter(email=ref).first()
                user.ref = ref.id if ref else None
            user.save()
            text = f'email: "{user.email}", password: "{user.password}", data_joined: {user.date_joined}, ' \
                   f'utm: {user.utm_source}, ref: {user.ref} '
            send_admin(text)
        except Exception as error:
            return {"ok": False, "desc": str(error)}
        return {"ok": True, "user_token": user.user_token}


class GetRefLink(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User(user_token=data["user_token"])
            if user:
                return {"ok": True, "ref_url": f"my.chatlead.io/SignUp?ref={user.id}"}
        except Exception as error:
            return {"ok": False, "desc": str(error)}
        return {"ok": False}


def manager_info(manager):
    return {
        "id": manager.id,
        "name": manager.name,
        "amocrm_domain": manager.amocrm_domain,
        "bitrix_key": manager.bitrix_key,
        "bitrix_domain": manager.bitrix_domain,
        "application_email": manager.application_email,
        "application_whatsapp_id": manager.application_whatsapp_id,
        "application_telegram_id": manager.application_telegram_id,
        "application_will_send": manager.application_will_send,
        "facebook_token": manager.facebook_token,
        "facebook_group_id": manager.facebook_group_id,
        "telegram_token": manager.telegram_token,
        "vk_group_id": manager.vk_group_id,
        "vk_group_access_token": manager.vk_group_access_token,
        "whatsapp_token": manager.whatsapp_token,
        "whatsapp_instance": manager.whatsapp_instance,
        "welcome_message": manager.welcome_message,
        "subscribe_message": manager.subscribe_message,
        "unsubscribe_message": manager.unsubscribe_message,
        "default_response": manager.default_response,
        "facebook_name": manager.facebook_name,
        "vk_name": manager.vk_name,
        "telegram_name": manager.telegram_name,
        "whatsapp_status": manager.whatsapp_status,
        "payed": manager.payed,
        "payed_end_date": manager.payed_end_date
    }


class GetUserManagers(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data["user_token"])
        except Exception as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "managers": [manager_info(manager) for manager in Manager.objects.filter(user=user)]
        }


class CreateManager(BaseApiMethod):
    required_params = ("user_token",)
    optional_params = ("name",)

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data["user_token"])
            is_exist = Manager.objects.filter(user=user).exists()
            period = 0 if is_exist else 3

            manager = Manager(user=user, name=data.get("name", ""), payed_end_date=period)
            manager.save()
        except Exception as error:
            return {"ok": False, "desc": str(error)}
        return {"ok": True, "manager": manager_info(manager)}


class GetManager(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "manager": manager_info(manager)
        }


class GetPaymentLink(BaseApiMethod):
    required_params = ("user_token", "manager_id", "amount", "description")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            return {"ok": True, "url": manager.get_pay_link(data['amount'], data['description'])}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class EditManager(BaseApiMethod):
    required_params = ("user_token", "manager_id")
    optional_params = (
        "name", "amocrm_domain", "bitrix_key", "bitrix_domain", "application_email", "application_whatsapp_id",
        "application_telegram_id", "facebook_token", "facebook_group_id",
        "telegram_token", "vk_group_id", "vk_group_access_token", "whatsapp_token", "whatsapp_instance",
        "welcome_message", "default_response", 'application_will_send', 'payed_end_date', 'unsubscribe_message',
        'subscribe_message'
    )

    def proccess(self, data):
        print('EditManager start!')
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
        except ApiException as error:
            return {"ok": False, "desc": str(error)}

        print('data: ', data)

        for field in self.optional_params:
            value = data.get(field)
            if value:
                if value == 'true':
                    value = 'True'
                elif value == 'false':
                    value = "False"

                any_manager = Manager.objects.exclude(id=manager.id)
                if len(value) > 0 and (
                        (field == 'facebook_token' and any_manager.filter(facebook_token=value).exists())
                        or (field == 'telegram_token' and any_manager.filter(telegram_token=value).exists())
                        or (field == 'vk_group_access_token' and any_manager.filter(
                    vk_group_access_token=value).exists())
                        or (field == 'whatsapp_token' and any_manager.filter(whatsapp_token=value).exists())
                ):
                    print('dubl bot')
                    return {"ok": False, "desc": 'данный бот уже авторизован'}

                setattr(manager, field, value)
        # restart = False
        # for field in ["facebook_token", "facebook_group_id", "telegram_token", "vk_group_id", "vk_group_access_token",
        #               "whatsapp_token", "whatsapp_instance"]:
        #     if field in data:
        #         restart = True
        manager.stop()
        if manager.payed_end_date > 0:
            manager.start('all')

        try:
            any_manager = Manager.objects.filter(
                (
                        Q(telegram_token=manager.telegram_token)
                        | (Q(whatsapp_token=manager.whatsapp_token) &
                           Q(whatsapp_instance=manager.whatsapp_instance))
                        | (Q(facebook_token=manager.facebook_token) &
                           Q(facebook_group_id=manager.facebook_group_id))
                        | (Q(vk_group_access_token=manager.vk_group_access_token) &
                           Q(vk_group_id=manager.vk_group_id))
                ) & ~Q(id=manager.id)
            )

            print('query: ', any_manager.query)
            if any_manager.exists():
                print('exist!')
            else:
                print('not exist!')
        except Exception as e:
            print('EditManager: ', e)
        manager.save()
        return {"ok": True, "manager": manager_info(manager)}


class DeleteManager(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            manager.delete()
            return {"ok": True}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetTagsMessages(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            tags = {}
            for message in BroadcastMessages.objects.filter(manager=manager):
                tag = message.tag
                tags[tag] = {"count": 0, "socials": {}}
                for social, users in [("whatsapp", WhatsAppUsers), ("telegram", TelegramUsers), ("vk", VkUsers),
                                      ("facebook", FacebookUsers)]:
                    count = len(users.objects.filter(tag__in=tag))
                    tags[tag]["count"] += count
                    tags[tag]["social"][social] = count
            return {"ok": True, "tags": tags}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def broadcast_info(broadcast):
    return {
        "id": broadcast.id,
        "scenario": scenario_info(broadcast.scenario),
        "tag": broadcast.tag,
        "time": broadcast.time,
        "users_count": broadcast.users_count,
        "sent": broadcast.sent,
        "proccessing": broadcast.proccessing,
        "for_group": broadcast.is_group
    }


class GetBroadcastMessages(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "broadcasts": [
                broadcast_info(broadcast)
                for broadcast in BroadcastMessages.objects.filter(manager=manager)
            ]
        }


class CreateBroadcast(BaseApiMethod):
    required_params = ("user_token", "manager_id", "scenario_id", "time")
    optional_params = ("tag", "for_group")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            broadcast = BroadcastMessages(
                manager=manager,
                scenario=Scenario.objects.get(manager=manager, id=data['scenario_id']),
                tag=data.get('tag', ''),
                time=data["time"],
                is_group=data.get('for_group', 'False').lower() in ('true', '1')
            )
            broadcast.save()
            return {"ok": True, 'broadcast': broadcast_info(broadcast)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class EditBroadcast(BaseApiMethod):
    required_params = ("user_token", "manager_id", "broadcast_id")
    optional_params = ("tag", "scenario_id", "time", "sent", "for_group")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            broadcast = BroadcastMessages.objects.get(id=data['broadcast_id'], manager=manager)
            scenario_id = data.get('scenario_id')
            if scenario_id:
                broadcast.scenario = Scenario.objects.get(id=scenario_id, manager=manager)
            tag = data.get('tag')
            if tag:
                broadcast.tag = tag
            time = data.get('time')
            if time:
                broadcast.time = time
            sent = data.get('sent')
            if sent:
                broadcast.sent = sent
                broadcast.proccessing = sent

            for_group = data.get('for_group')
            if for_group is not None:
                broadcast.is_group = for_group.lower() in ('true', '1')

            broadcast.save()
            return {"ok": True,
                    'broadcast': broadcast_info(broadcast)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteBroadcast(BaseApiMethod):
    required_params = ("user_token", "manager_id", "broadcast_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            broadcast = BroadcastMessages.objects.get(manager=manager, id=data['broadcast_id'])
            broadcast.delete()
            return {"ok": True}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class FacebookAuth(BaseApiMethod):
    required_params = ("user_token", "manager_id")
    optional_params = ("data",)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            UsersFacebook.users.update({manager.id: {
                "manager": manager,
                "state": "get_user_code"
            }})
            print('FB_CLIENT_ID: ', FB_CLIENT_ID)
            url = requests.Request("GET", "https://www.facebook.com/v3.2/dialog/oauth", params={
                "client_id": FB_CLIENT_ID,
                "response_type": "code",
                "redirect_uri": f"{REDIRECT_BASE}/fb_api",
                "state": manager.id,
                "scope": "public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions",
            }).prepare().url
            return {'state': 'get_user_code', 'url': url}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class UploadFile(BaseApiMethod):
    required_params = ("user_token", "manager_id", "type", "file")

    def send_response(self, response):
        return HttpResponse(json.dumps(response))

    def random_filename(self, filename=None, length=8):
        ext = pathlib.Path(filename).suffix
        letters = string.ascii_lowercase
        return f"{''.join(random.choice(letters) for i in range(length))}{ext}"

    @csrf_exempt
    def view(self, request):
        try:
            data = request.POST.dict()
            file = request.FILES.get('file')
            token = data.get("user_token")
            manager_id = data.get("manager_id")
            file_type = data.get('type')
            if not (file and token and manager_id and file_type in ['photo', 'audio', 'video', 'file']):
                return self.send_response({'ok': False, 'desk': "fill all params"})
            manager = AuthManager.get_manager(token, manager_id)
            if manager:
                url = os.path.join(str(manager.user.id), str(manager.id), file_type, self.random_filename(file.name))
                filename = os.path.join(UPLOAD_PATH, url)
                if not os.path.exists(os.path.dirname(filename)):
                    os.makedirs(os.path.dirname(filename))
                with open(filename, 'wb') as save_file:
                    uploaded_file = file.read()
                    save_file.write(uploaded_file)
                return self.send_response(
                    {'ok': True, "message": {file_type: {'filename': filename, "url": f"/app/media/{url}"}}})
            return self.send_response({'ok': False, 'desk': 'some wrong'})
        except Exception as e:
            return self.send_response({'ok': False, 'desk': str(e)})


class VkAuth(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])

            UsersVk.users.update({manager.id: {"manager": manager, "state": "get_user_code"}})

            print('VkAuth: ', UsersVk.users)
            print(f"{REDIRECT_BASE}/vk_api/{manager.id}")

            print('vk_auth: ', UsersVk.users[manager.id])
            print('vk_api: ', UsersVk.users)
            url = requests.Request("GET", "https://oauth.vk.com/authorize", params={
                "client_id": VK_CLIENT_ID,
                "display": "page",
                "redirect_uri": f"{REDIRECT_BASE}/vk_api/{manager.id}",
                "scope": "groups",
                "response_type": "code",
                "v": "5.102"}).prepare().url
            # print(url)
            return {'ok': True, "url": url}
        except ApiException as error:
            # print(traceback.format_exc())
            return {"ok": False, "desc": str(error)}


class FacebookAuthGroup(BaseApiMethod):
    required_params = ("user_token", "manager_id", "facebook_token", "facebook_group_id", "facebook_name")

    def proccess(self, data):
        try:
            print('------------', data)
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            manager.facebook_token = data['facebook_token']
            manager.facebook_group_id = data['facebook_group_id']
            manager.facebook_name = data['facebook_name']
            manager.save(force_update=True)

            BotsManager.stop(manager, ['facebook'])
            BotsManager.start(manager, ['facebook'])
            return {"ok": True, "url": f'https://my.chatlead.io/bots/{data["manager_id"]}/setup'}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class VkAuthGroup(BaseApiMethod):
    required_params = ("user_token", "manager_id", "group_id", "group_name")

    def proccess(self, data):
        print('------------', data)
        manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
        # UsersVk.users[manager.id].update({
        #     "group_id": data.get('group_id'),
        #     "state": "get_group_code",
        #     "group_name": data["group_name"]
        # })
        UsersVk.users.update({manager.id: {"manager": manager,
                                           "group_id": data.get('group_id'),
                                           "state": "get_group_code",
                                           "group_name": data["group_name"]
                                           }})
        print('VkAuthGroup: ', UsersVk.users)
        print(f"{REDIRECT_BASE}/vk_api/{manager.id}")
        url = requests.Request("GET", "https://oauth.vk.com/authorize", params={
            "client_id": settings.VK_CLIENT_ID,
            "scope": "messages,docs,manage,photos",
            "group_ids": data.get('group_id'),
            "redirect_uri": f"{REDIRECT_BASE}/vk_api/{manager.id}",
            "response_type": "code",
            "v": "5.102",
        }).prepare().url

        return {"ok": True, "url": url}


def trigger_info(trigger):
    return {'id': trigger.id, 'keyboard': trigger.keyboard, 'messages': trigger.messages, 'caption': trigger.caption,
            'tags': [{'action': tag.action, 'tag_name': tag.tag_name} for tag in trigger.tags.all()]}


def scenario_info(scenario):
    return {
        "id": scenario.id,
        "destination": scenario.destination,
        "trigger_text": scenario.trigger_text,
        "triggers": [trigger_info(trigger) for trigger in
                     Trigger.objects.filter(scenario=scenario).order_by('id')]
    }


class GetScenarios(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])

        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "scenarios": [scenario_info(scenario) for scenario in Scenario.objects.filter(manager=manager)]
        }


class GetScreenshot(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            if manager.get_bot('whatsapp') and manager.get_bot('whatsapp').instance and manager.get_bot(
                    'whatsapp').token:
                return {
                    "ok": True,
                    "url": manager.get_bot('whatsapp').get_screenshot()
                }
            return {
                "ok": False,
                "desk": "whatsapp bot not found"
            }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetQRCodeUrl(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            if manager.get_bot('whatsapp') and manager.get_bot('whatsapp').instance and manager.get_bot(
                    'whatsapp').token:
                return {
                    "ok": True,
                    "url": manager.get_bot('whatsapp').get_qr()
                }
            return {
                "ok": False,
                "desk": "whatsapp bot not found"
            }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class Logout(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            if manager.get_bot('whatsapp') and manager.get_bot('whatsapp').instance and manager.get_bot(
                    'whatsapp').token:
                return {
                    "ok": True,
                    "url": manager.get_bot('whatsapp').logout()
                }
            return {
                "ok": False,
                "desk": "whatsapp bot not found"
            }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetStatus(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            if manager.get_bot('whatsapp') and manager.get_bot('whatsapp').instance and manager.get_bot(
                    'whatsapp').token:
                return {
                    "ok": True,
                    "status": manager.get_bot('whatsapp').get_status()
                }
            return {
                "ok": False,
                "desk": "whatsapp bot not found"
            }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class CreateScenario(BaseApiMethod):
    required_params = ("user_token", "manager_id", "trigger_text")
    optional_params = ('destination',)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            text = ','.join(data['trigger_text'].split(", "))
            scenario = Scenario(manager=manager, trigger_text=text)
            if data.get('destination'):
                scenario.destination = data.get('destination', '')
            scenario.save()
            Trigger(scenario=scenario, caption='Начальный шаг').save()
            return {"ok": True, 'scenario': scenario_info(scenario)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetScenario(BaseApiMethod):
    required_params = ("user_token", "manager_id", "scenario_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            scenario = Scenario.objects.get(manager=manager, id=data['scenario_id'])
            return {"ok": True, 'scenario': scenario_info(scenario)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class EditScenario(BaseApiMethod):
    required_params = ("user_token", "manager_id", "scenario_id", "trigger_text")

    def proccess(self, data):
        try:
            print("data['trigger_text']: ", data['trigger_text'])
            text = ','.join(data['trigger_text'].split(", "))

            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            scenario = Scenario.objects.get(manager=manager, id=data['scenario_id'])
            scenario.trigger_text = text
            scenario.save()
            return {"ok": True, 'scenario': scenario_info(scenario)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteScenario(BaseApiMethod):
    required_params = ("user_token", "manager_id", "scenario_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            scenario = Scenario.objects.get(manager=manager, id=data['scenario_id'])
            scenario.delete()
            return {"ok": True}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def mlp_info(mlp):
    return {
        "id": str(mlp.link),
        "create_date": mlp.create_date,
        "settings": {
            "title": mlp.setting_title,
            "autoride_id": mlp.autoride.id if mlp.autoride else None
        },
        "content": {
            "socialList": mlp.social,
            "media": {
                "image": mlp.media_image,
                "video": mlp.media_video
            },
            "description": {
                "firstSection": {
                    "title": mlp.first_title,
                    "phone": mlp.first_phone
                },
                "secondSection": {
                    "title": mlp.second_title,
                    "phone": mlp.second_phone
                },
                "actionTitle": mlp.action_title
            }
        },
        "code": {
            "scriptForHead": mlp.script_for_head,
            "scriptForBody": mlp.script_for_body
        }
    }


def get_mlp(manager):
    return [mlp_info(x) for x in MLP.objects.filter(manager=manager)]


class UpdateMLP(BaseApiMethod):
    required_params = ("user_token", "bot_id", "data")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])

            info = json.loads(data['data'])
            mlp = MLP.objects.get(manager=manager, link=info['id'])

            mlp = update_mlp(mlp, data['data'])

            mlp.save()
            return {"ok": True, 'mlp': mlp_info(mlp)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteMLP(BaseApiMethod):
    required_params = ("user_token", "bot_id", "mlp_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            mlp = MLP.objects.get(manager=manager, link=data['mlp_id'])
            mlp.delete()
            return {"ok": True, "all_mlp": get_mlp(manager)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetMLP(BaseApiMethod):
    required_params = ("user_token", "bot_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            return {"ok": True, "all_mlp": get_mlp(manager)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class CreateMLP(BaseApiMethod):
    required_params = ("user_token", "bot_id",)
    optional_params = ('data',)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            mlp = MLP()
            mlp.manager = manager

            info = data.get('data')
            if info:
                mlp = update_mlp(mlp, info)

            mlp.save()

            return {"ok": True, 'mlp': mlp_info(mlp)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetMlpGuest(BaseApiMethod):
    required_params = ("key",)

    def proccess(self, data):
        try:
            mlp = MLP.objects.get(link=data['key'])

            return {"ok": True, 'mlp': mlp_info(mlp)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def update_mlp(mlp, info):
    info = json.loads(info)

    print("info: ", info)
    settings = info.get('settings', {})
    mlp.setting_title = settings.get("title")

    autoride_id = settings.get("autoride_id")
    mlp.autoride = AutoRide.objects.get(id=int(autoride_id)) if autoride_id else None

    content = info.get('content', {})

    media = content.get('media', {})
    mlp.media_image = media.get("image")
    mlp.media_video = media.get("video")

    mlp.social = json.dumps(content.get('socialList'))

    description = content.get('description', {})

    firstSection = description.get('firstSection', {})
    mlp.first_title = firstSection.get('title')
    mlp.first_phone = firstSection.get('phone')

    secondSection = description.get('secondSection', {})
    mlp.second_title = secondSection.get('title')
    mlp.second_phone = secondSection.get('phone')

    mlp.action_title = description.get('actionTitle')

    code = info.get('code', {})

    mlp.script_for_head = code.get('scriptForHead')
    mlp.script_for_body = code.get('scriptForBody')
    return mlp


def get_socials(autoride):
    manager = autoride.manager
    auto_ride_text = 'autoride_' + str(autoride.id)  # auto_ride.trigger_text
    return [
        {"social": "vk", "url": f"vk.com/write-{manager.vk_group_id}?ref_source={auto_ride_text}"},
        {"social": "telegram", "url": f"t.me/{manager.telegram_name}?start={auto_ride_text}"},
        {"social": "facebook", "url": f"m.me/{manager.facebook_group_id}?ref={auto_ride_text}"},
        {"social": "whatsapp",
         "url": f"https://api.whatsapp.com/send?phone={manager.whatsapp_instance}&text={auto_ride_text}"},
    ]


def create_tags(data, trigger):
    for field, target in [("AddTags", 'add'), ("RemoveTags", 'remove')]:
        for tag_name in data.get(field, '').split(','):
            if tag_name:
                if not Tag.objects.filter(action=target, tag_name=tag_name.strip(), trigger=trigger).exists():
                    tag = Tag(action=target, tag_name=tag_name.strip(), trigger=trigger)
                    tag.save()


class CreateTrigger(BaseApiMethod):
    required_params = ("user_token", "manager_id", "scenario_id", "messages")
    optional_params = ("caption", "AddTags", "RemoveTags", 'keyboard')

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            scenario = Scenario.objects.get(id=data.get('scenario_id'), manager=manager)
            step = f"{len(Trigger.objects.filter(scenario=scenario)) + 1} шаг"
            trigger = Trigger(caption=data.get('caption', step) or step, scenario=scenario, social='telegram',
                              messages=json.loads(data.get('messages', '[]')),
                              keyboard=json.loads(data.get('keyboard', '{}')))
            trigger.save()
            create_tags(data, trigger)
            return {"ok": True, 'trigger': trigger_info(trigger)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class EditTrigger(BaseApiMethod):
    required_params = ("user_token", "trigger_id")
    optional_params = ("messages", "caption", "AddTags", "RemoveTags", "clear_tags", 'keyboard')

    def proccess(self, data):
        try:
            # manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            trigger = Trigger.objects.get(id=data['trigger_id'])
            user = User.objects.get(user_token=data['user_token'])

            if not trigger.scenario.manager.user == user:
                raise ApiException('Bad request 403')

            caption = data.get('caption')
            if caption:
                trigger.caption = caption
            create_tags(data, trigger)
            for tag_name in data.get('clear_tags', '').split(','):
                if tag_name:
                    tag = Tag.objects.filter(trigger=trigger, tag_name=tag_name.strip())
                    if tag.exists():
                        tag[0].delete()
            messages = data.get('messages')
            keyboard = json.loads(data.get('keyboard', '{}'))
            if keyboard:
                trigger.keyboard = keyboard
            if messages:
                trigger.messages = json.loads(messages)
            trigger.save()
            return {"ok": True, 'trigger': trigger_info(trigger)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteTrigger(BaseApiMethod):
    required_params = ("user_token", "trigger_id")

    def proccess(self, data):
        try:
            # manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            user = User.objects.get(user_token=data['user_token'])
            trigger = Trigger.objects.get(id=data['trigger_id'])
            if not trigger.scenario.manager.user == user:
                raise ApiException('Bad request 403')
            trigger.delete()
            return {"ok": True}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetTrigger(BaseApiMethod):
    required_params = ("user_token", "trigger_id")

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data['user_token'])
            trigger = Trigger.objects.get(id=data['trigger_id'])
            if not trigger.scenario.manager.user == user:
                raise ApiException('Bad request 403')
            return {"ok": True, 'trigger': trigger_info(trigger)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class LaunchBots(BaseApiMethod):
    required_params = ("user_token", "manager_id", "state")
    optional_params = ("telegram", "whatsapp", "vk", "facebook")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            bots = [bot for bot in ["whatsapp", "telegram", "vk", "facebook"] if data.get(bot)]
            if not bots:
                bots = 'all'
            if data.get('state') == 'start':
                manager.start(bots)
            else:
                manager.stop(bots)

        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {"ok": True}


class StateBots(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            manager.fix_work_status()
            return {"ok": True,
                    'states': {'whatsapp': manager.whatsapp, 'facebook': manager.facebook, 'telegram': manager.telegram,
                               'vk': manager.vk}}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def auto_ride_info(auto_ride):
    return {"id": auto_ride.id, "trigger_text": auto_ride.trigger_text,
            "scenario": scenario_info(auto_ride.scenario)}


class GetAutoRide(BaseApiMethod):
    required_params = ("user_token", "manager_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "auto_rides": [auto_ride_info(auto_ride) for auto_ride in AutoRide.objects.filter(manager=manager)]
        }


class GetAutoRideLink(BaseApiMethod):
    required_params = ("user_token", "manager_id", "autoride_id", "social")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            auto_ride = AutoRide.objects.get(manager=manager, id=data['autoride_id'])
            social = data['social']
            # auto_ride_text = 'autoride_' + auto_ride.trigger_text
            auto_ride_text = 'autoride_' + str(auto_ride.id)  # auto_ride.trigger_text
            print('auto_ride_text:', auto_ride_text)
            if social == 'vk':
                link = f"vk.com/write-{manager.vk_group_id}?ref_source={auto_ride_text}"
            elif social == 'telegram':
                link = f"t.me/{manager.telegram_name}?start={auto_ride_text}"
            elif social == 'facebook':
                link = f"m.me/{manager.facebook_group_id}?ref={auto_ride_text}"
            elif social == 'whatsapp':
                link = f"https://api.whatsapp.com/send?phone={manager.whatsapp_instance}&text={auto_ride_text}"
            else:
                return {"ok": True, "links": {
                    "vk": f"vk.com/write-{manager.vk_group_id}?ref_source={auto_ride_text}",
                    'telegram': f"t.me/{manager.telegram_name}?start={auto_ride_text}",
                    'facebook': f"m.me/{manager.facebook_group_id}?ref={auto_ride_text}",
                    'whatsapp': f"https://api.whatsapp.com/send?phone={manager.whatsapp_instance}&text={auto_ride_text}"
                }}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}
        return {
            "ok": True,
            "link": link
        }


class CreateAutoRide(BaseApiMethod):
    required_params = ("user_token", "manager_id", "trigger_text", "scenario_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            auto_ride = AutoRide(manager=manager, trigger_text=data['trigger_text'],
                                 scenario=Scenario.objects.get(id=data['scenario_id']))
            auto_ride.save()
            return {"ok": True, 'auto_ride': auto_ride_info(auto_ride)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class EditAutoRide(BaseApiMethod):
    required_params = ("user_token", "manager_id", "auto_ride_id",)
    optional_params = ("trigger_text", "scenario_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            auto_ride = AutoRide.objects.get(manager=manager, id=data['auto_ride_id'])
            trigger_text = data.get('trigger_text')
            if trigger_text:
                auto_ride.trigger_text = trigger_text
            scenario = data.get('scenario_id')
            if scenario:
                auto_ride.scenario = Scenario.objects.get(id=scenario)
            auto_ride.save()
            return {"ok": True, 'auto_ride': auto_ride_info(auto_ride)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteAutoRide(BaseApiMethod):
    required_params = ("user_token", "manager_id", "auto_ride_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            auto_ride = AutoRide.objects.get(manager=manager, id=data['auto_ride_id'])
            auto_ride.delete()
            return {"ok": True}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetAnalytics(BaseApiMethod):
    required_params = ("manager_id", "user_token")
    optional_params = ("start_date", "end_date")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["manager_id"])
            start_date = 0
            end_date = 0
            days = []
            try:
                start_date = int(data['start_date']) if data['start_date'] else 0
                end_date = int(data['end_date']) if data['end_date'] else 0
            except Exception:
                start_date = 0
                end_date = 0

            telegram_users_all = 0
            telegram_unsub_users_all = 0
            telegram_applications_all = 0
            telegram_users_by_days = []
            telegram_unsub_users_by_days = []
            telegram_applications_by_days = []

            vk_users_all = 0
            vk_unsub_users_all = 0
            vk_applications_all = 0
            vk_users_by_days = []
            vk_unsub_users_by_days = []
            vk_applications_by_days = []

            whatsapp_users_all = 0
            whatsapp_unsub_users_all = 0
            whatsapp_applications_all = 0
            whatsapp_users_by_days = []
            whatsapp_unsub_users_by_days = []
            whatsapp_applications_by_days = []

            facebook_users_all = 0
            facebook_unsub_users_all = 0
            facebook_applications_all = 0
            facebook_users_by_days = []
            facebook_unsub_users_by_days = []
            facebook_applications_by_days = []

            if start_date != 0 and end_date != 0:
                telegram_users = TelegramUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                facebook_users = FacebookUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                vk_users = VkUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                whatsapp_users = WhatsAppUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                telegram_unsub_users = TelegramUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                facebook_unsub_users = FacebookUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                vk_uunsub_sers = VkUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                whatsapp_unsub_users = WhatsAppUsers.objects \
                    .filter(manager=manager, subscrib_data__lte=end_date) \
                    .values('id', 'subscrib_data')

                telegram_applications = FormLogging.objects \
                    .filter(manager=manager, source='telegram', submit_date_int__lte=end_date) \
                    .values('id', 'submit_date_int')

                vk_applications = FormLogging.objects \
                    .filter(manager=manager, source='vk', submit_date_int__lte=end_date) \
                    .values('id', 'submit_date_int')

                whatsapp_applications = FormLogging.objects \
                    .filter(manager=manager, source='whatsapp', submit_date_int__lte=end_date) \
                    .values('id', 'submit_date_int')

                facebook_applications = FormLogging.objects \
                    .filter(manager=manager, source='facebook', submit_date_int__lte=end_date) \
                    .values('id', 'submit_date_int')

                while start_date <= end_date:
                    telegram_users_by_days.append(len([x for x in telegram_users if x['subscrib_data'] < start_date]))
                    facebook_users_by_days.append(len([x for x in facebook_users if x['subscrib_data'] < start_date]))
                    vk_users_by_days.append(len([x for x in vk_users if x['subscrib_data'] < start_date]))
                    whatsapp_users_by_days.append(len([x for x in whatsapp_users if x['subscrib_data'] < start_date]))

                    telegram_unsub_users_by_days.append(
                        len([x for x in telegram_users if x['subscrib_data'] < start_date]))
                    facebook_unsub_users_by_days.append(
                        len([x for x in facebook_users if x['subscrib_data'] < start_date]))
                    vk_unsub_users_by_days.append(len([x for x in vk_users if x['subscrib_data'] < start_date]))
                    whatsapp_unsub_users_by_days.append(
                        len([x for x in whatsapp_users if x['subscrib_data'] < start_date]))

                    telegram_applications_by_days.append(len([x for x in telegram_applications
                                                              if x['submit_date_int'] < start_date]))
                    whatsapp_applications_by_days.append(len([x for x in whatsapp_applications
                                                              if x['submit_date_int'] < start_date]))

                    vk_applications_by_days.append(len([x for x in vk_applications
                                                        if x['submit_date_int'] < start_date]))
                    facebook_applications_by_days.append(len([x for x in facebook_applications
                                                              if x['submit_date_int'] < start_date]))

                    days.append(start_date)
                    start_date = int((datetime.fromtimestamp(start_date) + timedelta(days=1)).timestamp())

                forms = FormLogging.objects.filter(manager=manager, submit_date_int__gte=start_date,
                                                   submit_date_int__lte=end_date)
            else:
                forms = FormLogging.objects.filter(manager=manager)

            telegram_users_all = TelegramUsers.objects.filter(manager=manager).count()
            telegram_unsub_users_all = TelegramUsers.objects.filter(manager=manager, is_unsub=True).count()
            telegram_applications_all = FormLogging.objects.filter(manager=manager, source='telegram').count()

            vk_users_all = VkUsers.objects.filter(manager=manager).count()
            vk_unsub_users_all = VkUsers.objects.filter(manager=manager, is_unsub=True).count()
            vk_applications_all = FormLogging.objects.filter(manager=manager, source='vk').count()

            whatsapp_users_all = WhatsAppUsers.objects.filter(manager=manager).count()
            whatsapp_unsub_users_all = WhatsAppUsers.objects.filter(manager=manager, is_unsub=True).count()
            whatsapp_applications_all = FormLogging.objects.filter(manager=manager, source='whatsapp').count()

            facebook_users_all = FacebookUsers.objects.filter(manager=manager).count()
            facebook_unsub_users_all = FacebookUsers.objects.filter(manager=manager, is_unsub=True).count()
            facebook_applications_all = FormLogging.objects.filter(manager=manager, source='facebook').count()

            return {
                "ok": True,
                "telegram": {
                    "subscribers": telegram_users_all,
                    "unsubscribers": telegram_unsub_users_all,
                    "subscribe_by_day": telegram_users_by_days,
                    "unsubscribe_by_day": telegram_unsub_users_by_days,
                    "applications": telegram_applications_all,
                    "applications_by_day": telegram_applications_by_days,
                },
                "vk": {
                    "subscribers": vk_users_all,
                    "unsubscribers": vk_unsub_users_all,
                    "subscribe_by_day": vk_users_by_days,
                    "unsubscribe_by_day": vk_unsub_users_by_days,
                    "applications": vk_applications_all,
                    "applications_by_day": vk_applications_by_days,
                },
                "whatsapp": {
                    "subscribers": whatsapp_users_all,
                    "unsubscribers": whatsapp_unsub_users_all,
                    "subscribe_by_day": whatsapp_users_by_days,
                    "unsubscribe_by_day": whatsapp_unsub_users_by_days,
                    "applications": whatsapp_applications_all,
                    "applications_by_day": whatsapp_applications_by_days,
                },
                "facebook": {
                    "subscribers": facebook_users_all,
                    "unsubscribers": facebook_unsub_users_all,
                    "subscribe_by_day": facebook_users_by_days,
                    "unsubscribe_by_day": facebook_unsub_users_by_days,
                    "applications": facebook_applications_all,
                    "applications_by_day": facebook_applications_by_days,
                },
                "forms": [{"submit_date": form.submit_date_int, "form": [form.form]} for form in forms],
                "days": days
            }

        except ApiException as error:
            print('GetAnalytics: ', error)
            return {"ok": False, "desc": str(error)}


class AddPayment(BaseApiMethod):
    required_params = ("user_token", "payment",)
    optional_params = ("balance",)

    def proccess(self, data):
        try:
            balance = data.get('balance')
            val = '{"val":' + data.get("payment") + '}'
            print("val: ", val)
            val = json.loads(val)
            all_to_pay = 0
            user_token = data["user_token"]
            premium = {1: 49, 12: 468}
            standard = {1: 19, 12: 180}
            for payment in val.get('val'):
                period = int(payment.get("period", 0))
                plan = payment.get("plan")
                bot_id = payment["bot_id"]
                manager = AuthManager.get_manager(user_token, bot_id)
                # manager.payed_end_date += 30 if period == 1 else 365 if period == 12 else 0

                if plan == "premium":
                    to_pay = period
                    to_pay = premium[to_pay]
                else:
                    to_pay = period
                    to_pay = standard[to_pay]

                manager.save()
                all_to_pay += to_pay

            payment = Transactions(user=User.objects.get(user_token=user_token),
                                   to_pay=all_to_pay,
                                   bots=str(val.get('val')),
                                   status='Not paid')
            payment.save()
            payment_link = self.link_to_pay(all_to_pay, payment.id, payment.id)
            return {"ok": True,
                    "order_id": payment.id,
                    "to_pay": all_to_pay,
                    "payment_link": payment_link}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}

    # Генерация ссылки для оплаты: генерируется подпись, затем пришивается в один string и возвращается
    def link_to_pay(self, amount, order, payment_id):
        description = 'Order'
        # order = '2'
        example = {'v': 'payment.php', 'pg_amount': amount, 'pg_currency': 'USD', 'pg_description': description,
                   'pg_language': 'ru', 'pg_merchant_id': 518428,
                   'pg_order_id': order, 'pg_salt': 0, 'secret_key': 'SEtYyfytS3uVvBVS',
                   # 'pg_result_url': f'https://api.chatlead.io/app/paybox_webhook',
                   }

        m = hashlib.md5()
        res = ";".join(str(value) for value in example.values())
        m.update(res.encode('utf-8'))
        md5 = m.hexdigest()

        link = "https://api.paybox.money/payment.php?pg_merchant_id=518428" \
               + "&pg_amount=" + str(amount) \
               + "&pg_currency=USD&pg_description=" + str(description) \
               + "&pg_salt=0&pg_language=ru&pg_order_id=" + str(order) + \
               "&pg_sig=" + md5 + ""
        # f"&pg_result_url=https://api.chatlead.io/app/paybox_webhook" +\

        return link


class AddPaymentTest(BaseApiMethod):
    required_params = ("user_token", "payment",)

    def proccess(self, data):
        try:
            val = '{"val":' + data.get("payment") + '}'
            print("val: ", val)
            val = json.loads(val)
            all_to_pay = 0
            user_token = data["user_token"]
            premium = {1: 49, 12: 468}
            standard = {1: 19, 12: 180}
            for payment in val.get('val'):
                period = int(payment.get("period", 0))
                plan = payment.get("plan")
                bot_id = payment["bot_id"]
                manager = AuthManager.get_manager(user_token, bot_id)
                # manager.payed_end_date += 30 if period == 1 else 365 if period == 12 else 0

                if plan == "premium":
                    to_pay = period
                    to_pay = premium[to_pay]
                else:
                    to_pay = period
                    to_pay = standard[to_pay]

                manager.save()
                all_to_pay += to_pay

            payment = Transactions(user=User.objects.get(user_token=user_token),
                                   to_pay=all_to_pay,
                                   bots=str(val.get('val')),
                                   status='Not paid')
            payment.save()
            payment_link = self.link_to_pay(all_to_pay, payment.id, payment.id)
            return {"ok": True,
                    "order_id": payment.id,
                    "to_pay": all_to_pay,
                    "payment_link": payment_link}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}

    # Генерация ссылки для оплаты: генерируется подпись, затем пришивается в один string и возвращается
    def link_to_pay(self, amount, order, payment_id):
        description = 'Order'
        # order = '2'
        example = {'v': 'payment.php',
                   'pg_amount': amount,
                   'pg_currency': 'USD',
                   'pg_description': description,
                   'pg_language': 'ru',
                   'pg_merchant_id': 518428,
                   'pg_order_id': order, 'pg_salt': 0,
                   'pg_testing_mode': 1,
                   'secret_key': 'SEtYyfytS3uVvBVS',
                   }

        m = hashlib.md5()
        res = ";".join(str(value) for value in example.values())
        m.update(res.encode('utf-8'))
        md5 = m.hexdigest()

        link = "https://api.paybox.money/payment.php?pg_merchant_id=518428&" \
               + "&pg_amount=" + str(amount) \
               + "&pg_currency=USD" \
               + "&pg_description=" + str(description) \
               + "&pg_salt=0" \
               + "&pg_language=ru" \
               + "&pg_order_id=" + str(order) \
               + "&pg_testing_mode=1" \
               + "&pg_sig=" + md5 + ""
        # f"&pg_result_url=https://api.chatlead.io/app/paybox_webhook" +\

        return link


class GetTransactions(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User.objects.filter(user_token=data["user_token"]).first()

            if not user:
                raise ApiException("Wrong user_token")

            managers = Manager.objects.filter(user=user)

            return {"ok": True,
                    "transactions": [
                        {"bot_id": tr.manager_id, "plan": tr.plan, "period": tr.period, "date": tr.add_date} for tr in
                        Transactions.objects.filter(manager__in=managers).order_by('add_date')
                    ]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class StopTran(BaseApiMethod):
    optional_params = ("stop",)

    def proccess(self, data):
        try:
            print('stop_bots')
            for manager in Manager.objects.all().order_by('-id'):
                print("manager stop:", manager)
                BotsManager.stop(manager)

            return {"ok": True, "desc": "seccess stop"}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class StartTran(BaseApiMethod):
    optional_params = ("stop",)

    def proccess(self, data):
        try:
            print('start_bots')
            from .models import Manager, BotsManager
            for manager in Manager.objects.all().order_by('-id'):  # [:100]:
                # order_by('id'): #.order_by('-id')[:20]:
                print("manager start:", manager)
                BotsManager.start(manager)

            return {"ok": True, "desc": "seccess start"}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def get_users(manager, messenger):
    users = None
    if messenger == "whatsapp":
        users = WhatsAppUsers.objects.filter(manager=manager)
    elif messenger == "telegram":
        users = TelegramUsers.objects.filter(manager=manager)
    elif messenger == "vk":
        users = VkUsers.objects.filter(manager=manager)
    elif messenger == "facebook":
        users = FacebookUsers.objects.filter(manager=manager)

    return users


class ExportUsers(BaseApiMethod):
    required_params = ("user_token", "bot_id", "messenger")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            messenger = data["messenger"]
            users = get_users(manager, messenger)

            if not users:
                return {"ok": False, "desc": f"empty for {manager}-{messenger}"}

            directory = ""
            for x in ['/home/admin/chatlead/uploads/', f'{manager.user_id}/', f'{manager.id}/', 'users/']:
                directory += x
                if not os.path.exists(directory):
                    os.mkdir(directory)

            wb = openpyxl.Workbook()
            sheet = wb.active
            sheet["A1"] = "user_id"
            sheet["B1"] = "username"
            sheet["C1"] = "first_name"
            sheet["D1"] = "join_date"
            count = 2
            for x in users:
                # try:
                user_id = str(x.user_id).replace('@c.us', '')
                username = str(json.loads(x.user_data).get("username")).replace("\n",
                                                                                "") if x.user_data != 'main' and json.loads(
                    x.user_data).get("username") else ''
                first_name = json.loads(x.user_data).get("first_name") if x.user_data != 'main' and json.loads(
                    x.user_data).get("first_name") else ''
                join_date = str(x.subscrib_data) if x.subscrib_data else ''

                sheet.append([user_id, username, first_name, join_date])
                count += 1

            path = os.path.join(directory, f"{manager.id}_whatsapp.xlsx")
            if os.path.isfile(path):
                os.remove(path)
            wb.save(path)
            return {"ok": True,
                    "filename": f"{REDIRECT_BASE}/media/{manager.user_id}/{manager.id}/users/{manager.id}_whatsapp.xlsx",
                    "users_count": users.count()}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class ImportUsers(BaseApiMethod):
    required_params = ("user_token", "bot_id", "messenger", "filename", "user_id_field")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            filename = data.get("filename")
            if not os.path.exists(filename):
                return {"ok": False, "desc": "file not exists"}

            f = open(filename)
            line = f.readline().replace('\n', '').split(",")

            # return {"ok": False, "desc": line}
            messenger = data.get("messenger")
            users = get_users(manager, messenger)

            user_id_index = -1
            user_id_field = data.get("user_id_field")
            if user_id_field:
                user_id_index = line.index(user_id_field)

            count_skip = 0
            count_error = 0
            count_seccess = 0
            while True:
                try:
                    line = f.readline()
                    if not line:
                        break

                    line = line.split(",")
                    user_id = line[user_id_index]

                    if messenger == "whatsapp" and user_id.find('@c.us') == -1:
                        user_id += '@c.us'
                    user = users.filter(user_id=user_id).first()
                    if user:
                        count_skip += 1
                        continue

                    user = TelegramUsers() if messenger == "telegram" \
                        else WhatsAppUsers() if messenger == "whatsapp" \
                        else VkUsers() if messenger == "vk" \
                        else FacebookUsers()

                    user.manager = manager
                    user.user_id = user_id

                    user.save()
                    count_seccess += 1
                except Exception as e:
                    count_error += 1

            return {"ok": True, "count_seccess": count_seccess, "count_skip": count_skip, "count_error": count_error}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetRedirectStatistic(BaseApiMethod):
    required_params = ("user_token", "bot_id",)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            statistic = PinLink.objects.filter(manager=manager).values('text', 'is_redirect') \
                .annotate(count=Count('text')).order_by('text')

            result = {}
            for x in PinLink.objects.filter(manager=manager).values('text'):
                result[x['text']] = {"count_redirect": 0, 'count_not_redirect': 0}

            for x in statistic:
                text = x['text']
                count = x['count']
                last = result[text]
                if x['is_redirect']:
                    result.update({text: {"count_redirect": count,
                                          "count_not_redirect": last.get('count_not_redirect', 0)}})
                else:
                    result.update({text: {"count_redirect": last.get('count_not_redirect', 0),
                                          "count_not_redirect": count}})
            return {"ok": True, "statistic": [result]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetTagStatistic(BaseApiMethod):
    required_params = ("user_token", "bot_id",)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            res = tag_statistic(manager)
            return {"ok": True,
                    "tags": res}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def tag_statistic(manager):
    res = []
    for x in UserTag.objects.filter(manager=manager):
        count = UserTag.objects.filter(manager=manager, id=x.id).annotate(count=Count('user_tags_tg'))[0].count + \
                UserTag.objects.filter(manager=manager, id=x.id).annotate(count=Count('user_tags_wa'))[0].count + \
                UserTag.objects.filter(manager=manager, id=x.id).annotate(count=Count('user_tags_vk'))[0].count + \
                UserTag.objects.filter(manager=manager, id=x.id).annotate(count=Count('user_tags_fb'))[0].count
        res.append({"name": x.text, "count": count})

    return res


class GetTags(BaseApiMethod):
    required_params = ("user_token", "bot_id",)
    optional_params = ("need_statistic",)

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            need_statistic = data.get("need_statistic", False)
            res = []
            if need_statistic:
                res = tag_statistic(manager)
                return {"ok": True,
                        "tags": res}
            else:
                return {"ok": True,
                        "tags": [{"name": tag.text}
                                 for tag in UserTag.objects.filter(manager=manager)]
                        }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class AddTags(BaseApiMethod):
    required_params = ("user_token", "bot_id", "tag")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            if not data.get('tag'):
                return {"ok": False, "desc": 'tag is empty'}

            tag = UserTag(manager=manager, text=data["tag"].lower())
            tag.save()
            return {"ok": True,
                    "tags": [{"name": tag.text}
                             for tag in UserTag.objects.filter(manager=manager)]
                    }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class DeleteTag(BaseApiMethod):
    required_params = ("user_token", "bot_id", "tag")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            if not data.get('tag'):
                return {"ok": False, "desc": 'tag is empty'}

            tag = data["tag"].lower()

            user_tag = UserTag.objects.filter(manager=manager, text=tag).first()
            user_tag.delete()
            if not user_tag:
                return {"ok": False, "desc": f'{tag} not found'}
            return {"ok": True,
                    "tags": [{"name": tag.text}
                             for tag in UserTag.objects.filter(manager=manager)]
                    }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetUrl(BaseApiMethod):
    required_params = ("code",)

    def proccess(self, data):
        try:
            link = PinLink.objects.filter(link=data['code']).first()
            if link:
                link.watch_count += 1
                link.is_redirect = link.watch_count > 1

                manager = link.manager  # Manager.objects.filter(id=link.manager).first()
                bot = manager.get_bot(link.bot_place)

                if manager:
                    set_tag = link.add_tags.split(',') if link.add_tags else []
                    remove_tag = link.delete_tags.split(',') if link.delete_tags else []
                    bot.update_tag(link.user_id, set_tag, remove_tag)

                link.save()

                user = bot.BotUsers.objects.filter(manager=manager, user_id=link.user_id).first()
                text = str(link.text).replace('{uuid}', str(user.link))
                print('url: ', text)
                return {"ok": True, "url": text}

            return {"ok": False, "desc": 'link is empty'}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetUserInfo(BaseApiMethod):
    required_params = ("key",)

    def proccess(self, data):
        try:
            key = data['key']
            bot_place = 'telegram'
            user = TelegramUsers.objects.filter(link=key).first()
            if not user:
                user = VkUsers.objects.filter(link=key).first()
                bot_place = 'vk'
            if not user:
                user = WhatsAppUsers.objects.filter(link=key).first()
                bot_place = 'whatsapp'
            if not user:
                user = FacebookUsers.objects.filter(link=key).first()
                bot_place = 'facebook'
            if not user:
                return {"ok": False, "desc": 'invalid key'}

            user = user.manager.get_bot(bot_place).get_user_icon(user.link)
            user_data = json.loads(user.user_data) if user.user_data != 'main' else {}
            return {
                "ok": True,
                "user": {
                    "username": user_data.get('username', ''),
                    "first_name": user_data.get('first_name', ''),
                    "last_name": user_data.get('last_name', ''),
                    "icon": f"{REDIRECT_BASE}/media/{user.icon.url.split('uploads/')[-1]}" if user.icon else None,
                    "bot_place": bot_place,
                    # "icon_url": user.icon_url,
                    # "temp": [{f'{x}': x} for x in info],
                    "tags": [x.text for x in user.user_tag.all()]
                }
            }
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetDialogs(BaseApiMethod):
    required_params = ("user_token", "bot_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            return {"ok": True, "dialogs": [manager.get_bot('whatsapp').get_dialogs()]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class ReadChat(BaseApiMethod):
    required_params = ("user_token", "bot_id", "chat_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            chat_id = data.get('chat_id')
            return {"ok": True, "info": manager.get_bot('whatsapp').read_chat(chat_id)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class CopyTrigger(BaseApiMethod):
    required_params = ("user_token", "bot_id", "scenario_id", "trigger_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            scenario = Scenario.objects.get(manager=manager, id=data['scenario_id'])
            trigger = Trigger.objects.get(scenario=scenario, id=data['trigger_id'])

            count = Trigger.objects.filter(scenario=scenario, caption__icontains='копия').count()
            trigger.id = None
            trigger.caption = f'Копия - {count}'
            trigger.save()

            return {"ok": True,
                    'trigger': trigger_info(trigger)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class CopyAutoRide(BaseApiMethod):
    required_params = ("user_token", "bot_id", "autoride_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            autoride = AutoRide.objects.get(manager=manager, id=data['autoride_id'])
            autoride.scenario = copy_scenario(manager, autoride.scenario_id)

            count = AutoRide.objects.filter(manager=manager, trigger_text__icontains='копия').count()
            autoride.id = None
            autoride.trigger_text = f'Копия - {count}'
            autoride.save()

            return {"ok": True,
                    "scenarios": [scenario_info(scenario) for scenario in Scenario.objects.filter(manager=manager)]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class CopyScenario(BaseApiMethod):
    required_params = ("user_token", "bot_id", "scenario_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            copy_scenario(manager, data['scenario_id'])
            return {"ok": True,
                    "scenarios": [scenario_info(scenario) for scenario in Scenario.objects.filter(manager=manager)]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def copy_scenario(manager, scenario_id):
    scenario = Scenario.objects.get(manager=manager, id=scenario_id)
    scenario.id = None
    count = Scenario.objects.filter(manager=manager, trigger_text__icontains='копия').count()
    scenario.trigger_text = f'Копия - {count}'
    scenario.save()

    for x in Trigger.objects.filter(scenario=Scenario.objects.get(id=scenario_id)):
        x.id = None
        x.scenario = scenario
        x.save()

    return scenario


def get_received_msg(messenger):
    received_msg = TelegramReceivedMsg if messenger == "telegram" else WhatsappReceivedMsg if messenger == "whatsapp" else VkReceivedMsg if messenger == "vk" else FacebookReceivedMsg
    return received_msg


def message_info(messages):
    return [{"id": x.id,
             "is_bot": x.is_bot,
             "text": x.text,
             "sender_name": x.sender_name,
             "time": x.time,
             "chat_id": x.chat_id,
             "message_type": x.message_type}
            for x in messages]


class GetChatMessages(BaseApiMethod):
    required_params = ("user_token", "bot_id", "messenger", "chat_id", "page")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])

            page = int(data['page'])
            received_msg = get_received_msg(data['messenger'])

            messages = received_msg.objects.filter(manager=manager, chat_id=data['chat_id'])

            current_page = messages.order_by('-id')[(page - 1) * 50: page * 50]

            return {"ok": True,
                    "count_page": int(messages.count() / 50) + 1,
                    "current_page": page,
                    "messages": message_info(current_page)}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class SendMessage(BaseApiMethod):
    required_params = ("user_token", "bot_id", "messenger", "chat_id", "need_messages", 'text')

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            bot = manager.get_bot(data['messenger'])
            bot.send_message(data['chat_id'], data['text'])

            if not data['need_messages']:
                return {"ok": True}

            received_msg = get_received_msg(data['messenger'])
            messages = received_msg.objects.filter(manager=manager, chat_id=data['chat_id'])

            current_page = messages.order_by('-id')[0:50]

            return {"ok": True,
                    "count_page": int(messages.count() / 50) + 1,
                    "current_page": 1,
                    "messages": message_info(current_page)}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetChatsId(BaseApiMethod):
    required_params = ("user_token", "bot_id", "messenger", "page")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])
            page = int(data['page'])
            messengers = data['messenger']
            messengers = ['telegram', 'whatsapp', 'vk', 'facebook'] if messengers == 'all' else [messengers]

            chats = []

            for messenger in messengers:
                received_msg = get_received_msg(data['messenger']).objects.filter(manager=manager, is_bot=False) \
                    .values('chat_id', 'sender_name').annotate(time_max=Max('time'))

                chats += [
                    {"chat_id": x['chat_id'], "name": x['sender_name'], "messenger": messenger, "time": x['time_max']}
                    for x in received_msg]

            all = len(chats)
            chats = chats[(page - 1) * 10:page * 10]

            for chat in chats:
                msg = get_received_msg(chat['messenger']).objects.filter(manager=manager,
                                                                         chat_id=chat['chat_id']).order_by(
                    '-id').first()
                chat['msg'] = msg.text
                chat['msg_type'] = msg.message_type

                bot = manager.get_bot(chat['messenger'])
                user = bot.BotUsers.objects.filter(user_id=chat['chat_id']).first()
                try:
                    chat['key'] = user.link if user else None
                    user = bot.get_user_icon(user.link) if user else None
                    url = f"{REDIRECT_BASE}/media/{user.icon.url.split('uploads/')[-1]}" if user and user.icon else None
                    chat['icon'] = url
                except:
                    chat['icon'] = None

                print('icon: ', chat['icon'])

            return {"ok": True,
                    "current_page": page,
                    "count_pages": int(all / 10) + 1,
                    "messages": [{"chat_id": x['chat_id'],
                                  "name": x['name'],
                                  "time": x['time'],
                                  "msg": x['msg'],
                                  "msg_type": x['msg_type'],
                                  "messenger": x['messenger'],
                                  "key": x['key'],
                                  "icon": x['icon']
                                  } for x in chats]}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def get_user(user):
    return {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "phone": user.phone,
        "password": user.password,
        "photo": f"{REDIRECT_BASE}/media/{user.photo.split('uploads/')[-1]}" if user.photo else None,
    }


class UpdateProfile(BaseApiMethod):
    required_params = ("user_token", 'profile')

    def proccess(self, data):
        try:

            user = User.objects.get(user_token=data['user_token'])
            profile = json.loads(data.get('profile', {}))
            user.first_name = profile.get('first_name', '')
            user.last_name = profile.get('last_name', '')
            user.email = profile.get('email', '')
            user.phone = profile.get('phone', '')
            user.photo = profile.get('photo', '')
            user.password = profile.get('password', '123')
            user.save()
            return {"ok": True,
                    "profile": get_user(user)}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetProfile(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data['user_token'])
            return {"ok": True,
                    "profile": get_user(user)}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class MLPSendPhone(BaseApiMethod):
    required_params = ("key", "phone")

    def proccess(self, data):
        try:
            mlp = MLP.objects.get(link=data['key'])
            manager = mlp.manager
            phone = data["phone"]

            for admin in str(manager.application_email).split(','):
                if admin:
                    from .mail import send_message
                    send_message(to=admin, message=phone, subject='МЛП. Номер телефона.')

            for admin in str(manager.application_telegram_id).split(','):
                if admin:
                    bot = telebot.TeleBot('990694770:AAHnFf2J0cjNOj7YTV1DxaEiLPCAPgivOvY', threaded=False)
                    bot.send_message(admin, f'МЛП. Номер телефона. {phone}')

            form = MLPForm(manager=manager, mlp=mlp, phone=phone)
            form.save()

            return {"ok": True}

        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetMLPForms(BaseApiMethod):
    required_params = ("user_token", "bot_id")

    def proccess(self, data):
        try:
            manager = AuthManager.get_manager(data["user_token"], data["bot_id"])

            forms = MLPForm.objects.filter(manager=manager)

            return {"ok": True,
                    "forms": [{
                        "mlp_id": x.mlp.link,
                        "phone": x.phone,
                    } for x in forms]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


def get_balance(user):
    pays = PayRef.objects.filter(user=user)
    sum_paid_out = pays.filter(is_approved=True).aggregate(Sum('amount'))['amount__sum'] or 0
    sum_in_processing = pays.filter(user=user, is_approved=False).aggregate(Sum('amount'))['amount__sum'] or 0

    return str(user.amount), str(sum_in_processing), str(sum_paid_out)


class GetBalance(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data["user_token"])
            balance, sum_in_processing, sum_paid_out = get_balance(user)
            return {"ok": True,
                    "balance": balance,
                    "in_processing": sum_in_processing,
                    "paid_out": sum_paid_out}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class AddMoneyRequest(BaseApiMethod):
    required_params = ("user_token", 'amount', 'card_number', 'type')

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data["user_token"])
            amount = float(data["amount"])
            card_number = data["card_number"]
            type = data["type"]

            if user.amount < amount:
                return {"ok": False, "desc": 'Сумма вывода превышает баланс'}

            PayRef(user=user, amount=amount, card_number=card_number, type=type).save()
            user.amount = float(user.amount) - amount
            user.save()

            text = f"user: {user.id} \namount: {amount}\ncard_number: {card_number}"
            send_admin(text)

            balance, sum_in_processing, sum_paid_out = get_balance(user)
            return {"ok": True,
                    "balance": balance,
                    "in_processing": sum_in_processing,
                    "paid_out": sum_paid_out}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class GetMoneyRequests(BaseApiMethod):
    required_params = ("user_token",)

    def proccess(self, data):
        try:
            user = User.objects.get(user_token=data["user_token"])
            pays = PayRef.objects.filter(user=user)

            return {"ok": True,
                    "pays": [{
                        "amount": str(pay.amount),
                        "card_number": pay.card_number,
                        "date": pay.request_date,
                        "is_approved": pay.is_approved
                    } for pay in pays]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


class FacebookAutoLogin(BaseApiMethod):
    required_params = ("login", "password")

    def proccess(self, data):
        try:
            domain = 'https://apps.facebook.com/'
            redirect_domain = 'https://my.chatlead.io/bots'
            return {"ok": True,
                    "pays": [{
                        "amount": str(pay.amount),
                        "card_number": pay.card_number,
                        "date": pay.request_date,
                        "is_approved": pay.is_approved
                    } for pay in pays]}
        except ApiException as error:
            return {"ok": False, "desc": str(error)}


all_methods = [
    FacebookAutoLogin,


    GetMoneyRequests,
    AddMoneyRequest,
    GetBalance,

    MLPSendPhone,
    GetMLPForms,

    GetProfile,
    UpdateProfile,
    GetChatMessages,
    SendMessage,
    GetChatsId,

    CopyTrigger,
    CopyScenario,
    CopyAutoRide,

    GetMlpGuest,
    GetMLP,
    UpdateMLP,
    DeleteMLP,
    CreateMLP,
    ReadChat,
    GetDialogs,
    GetUserInfo,
    GetUrl,
    GetTags,
    AddTags,
    DeleteTag,
    CreateUser,
    GetUserToken,
    GetRefLink,

    StopTran,
    StartTran,
    GetAutoRide,
    CreateAutoRide,
    EditAutoRide,
    DeleteAutoRide,
    GetAutoRideLink,
    GetUserManagers,
    CreateManager,
    GetManager,
    EditManager,
    DeleteManager,

    GetBroadcastMessages,
    CreateBroadcast,
    EditBroadcast,
    DeleteBroadcast,

    GetScenarios,
    CreateScenario,
    GetScenario,
    EditScenario,
    DeleteScenario,

    CreateTrigger,
    GetTrigger,
    EditTrigger,
    DeleteTrigger,
    GetPaymentLink,
    UploadFile,
    LaunchBots,
    StateBots,
    GetTagsMessages,
    FacebookAuth,
    FacebookAuthGroup,
    VkAuth,
    VkAuthGroup,

    GetQRCodeUrl,
    GetScreenshot,
    GetStatus,
    Logout,

    GetAnalytics,
    AddPayment,
    AddPaymentTest,
    GetTransactions,
    ExportUsers,
    ImportUsers,

    GetTagStatistic,
    GetRedirectStatistic,

]
