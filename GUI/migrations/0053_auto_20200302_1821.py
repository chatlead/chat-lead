# Generated by Django 2.2.2 on 2020-03-02 12:21

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0052_auto_20200302_0302'),
    ]

    operations = [
        migrations.AddField(
            model_name='facebookreceivedmsg',
            name='sender_name',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
        migrations.AddField(
            model_name='facebookreceivedmsg',
            name='sender_username',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
        migrations.AddField(
            model_name='vkreceivedmsg',
            name='sender_name',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
        migrations.AddField(
            model_name='vkreceivedmsg',
            name='sender_username',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
        migrations.AddField(
            model_name='whatsappreceivedmsg',
            name='sender_username',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 3, 2, 18, 21, 17, 57892), null=True),
        ),
        migrations.AlterField(
            model_name='mlp',
            name='create_date',
            field=models.PositiveIntegerField(default=1583151677.056465),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1583151677.03375),
        ),
    ]
