# Generated by Django 2.2.2 on 2020-01-24 21:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0016_auto_20200125_0328'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 1, 25, 3, 37, 58, 383109), null=True),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1579901878.362044),
        ),
        migrations.DeleteModel(
            name='PinLink',
        ),
    ]
