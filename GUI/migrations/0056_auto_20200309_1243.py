# Generated by Django 2.2.2 on 2020-03-09 06:43

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0055_auto_20200303_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formlogging',
            name='submit_date',
            field=models.DateField(default=datetime.date(2020, 3, 9)),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 3, 9, 12, 43, 53, 156165), null=True),
        ),
        migrations.AlterField(
            model_name='mlp',
            name='create_date',
            field=models.PositiveIntegerField(default=1583736233.153954),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1583736233.127577),
        ),
        migrations.CreateModel(
            name='MLPForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(blank=True, default='', max_length=256, null=True)),
                ('manager', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='GUI.Manager')),
                ('mlp', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='GUI.MLP')),
            ],
        ),
    ]
