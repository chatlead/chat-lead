from django.urls import path

from . import views
from . import api
from django.conf.urls.static import static

from chat_api.settings import MEDIA_URL, UPLOAD_PATH, STATIC_URL, STATIC_ROOT

urlpatterns = [
    path(r"vk_api/<int:pk>", views.vk_api, name="vk_api_pk"),
    path(r"vk_api_call_back/<int:pk>", views.vk_api_call_back, name="vk_api_call_back"),
    # path(r"fb_api1", views.fb_api1, name="fb_api_pk"),
    path(r"redirect/<str:id>", views.redirect_vc, name="redirect_vc"),
    path(r"fb_api", views.fb_api),
    path(r"whatsapp_webhook", views.whatsapp_webhook),
    path(r"facebook_webhook", views.facebook_webhook, name="facebook_webhook"),
    path(r"telegram_webhook/<str:token>", views.telegram_webhook, name="telegram_webhook"),
    path(r"paybox_webhook", views.paybox_webhook, name="paybox_webhook"),
    *[
        path(f"api/{ApiMethod.__name__}/", ApiMethod().view, name=ApiMethod.__name__)
        for ApiMethod in api.all_methods
    ]
] + static(STATIC_URL, document_root=STATIC_ROOT) + static(MEDIA_URL, document_root=UPLOAD_PATH)
