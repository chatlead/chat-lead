# Generated by Django 2.2.2 on 2020-01-24 21:25

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0013_auto_20200125_0309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 1, 25, 3, 25, 44, 975105), null=True),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1579901144.951151),
        ),
    ]
