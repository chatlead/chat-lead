import json
import random
import string
import traceback
from datetime import datetime

import requests
from chat_api.settings import REDIRECT_BASE as SITE_NAME
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt

from .api import UsersVk, UsersFacebook
from .models import *


def uuid():
    return f"{datetime.now().timestamp()}-" + "-".join(
        "".join(random.choices(string.ascii_letters, k=4)) for _ in range(4))


@csrf_exempt
def redirect_vc(request, id):
    link = PinLink.objects.filter(link=str(id)).first()
    print('redirect_vc!')
    if link:
        link.watch_count += 1
        link.is_redirect = link.watch_count > 1

        if link.is_redirect:
            manager = Manager.objects.filter(id=link.manager).first()

            if manager:
                set_tag = link.add_tags.split(',') if link.add_tags else []
                remove_tag = link.delete_tags.split(
                    ',') if link.delete_tags else []
                manager.get_bot(link.bot_place).update_tag(set_tag, remove_tag)

        link.save()
        print('url: ', link.text)
        return redirect(link.text)
    return HttpResponse("error")


@csrf_exempt
def telegram_webhook(request, token='empty'):
    if request.method == "GET":
        return HttpResponse(request.GET.dict().get("hub.challenge", "123456789"))

    update = json.loads(request.body.decode())
    # print('update telegram: ', update)
    managers = Manager.objects.filter(telegram=True, telegram_token=token)

    # print('count manager: ', managers.count())
    try:
        for manager in managers:
            # print('tg: ', manager, ' ', manager.get_bot('telegram').can_work(manager))
            if manager.get_bot('telegram').can_work(manager):
                try:
                    # event = json.loads(event)
                    if update.get("callback_query"):
                        manager.get_bot('telegram').proccess_callbacks(
                            update.get("callback_query"))
                        break

                    if update.get("message"):
                        event = update.get("message")
                        content_type = "text" if event.get("text") else "photo"
                        # print('message: ', event, ' ', content_type, ' ', event.get("text"))

                        if event.get("new_chat_member"):
                            manager = Manager.objects.filter(
                                telegram_token=token).first()
                            user_id = event.get("chat").get("id")

                            if TelegramUsers.objects.filter(manager=manager, user_id=user_id).exists():
                                break

                            TelegramUsers(
                                manager=manager, user_id=user_id, is_group=True).save()
                            manager.get_bot('telegram').send_message(
                                user_id, "бот добавлен")
                            break

                        # print('go')
                        manager.get_bot('telegram').proccess_messages(
                            event, content_type)
                        break

                except Exception as e:
                    print('err telegram: ', e)
    except Exception as e:
        print('err1 telegram: ', e)
    return HttpResponse("OK")


@csrf_exempt
def facebook_webhook(request):
    # print(request)
    # print('facebook_webhook start')

    if request.method == "GET":
        return HttpResponse(request.GET.dict().get("hub.challenge", "123456789"))

    update = json.loads(request.body.decode())
    # print('update fb: ', update)
    for event in update.get("entry", []):
        group_id = event.get("id")
        for manager in Manager.objects.filter(facebook=True, facebook_group_id=group_id):
            print('facebook_webhook: ', manager)
            if manager.get_bot('facebook').can_work(manager):
                try:
                    # print('event: ', event)
                    manager.get_bot('facebook').proccess_event(event)
                except Exception:
                    print('err: ', traceback.format_exc())

    return HttpResponse("OK")


@csrf_exempt
def whatsapp_webhook(request):
    if request.method == "GET":
        return HttpResponse(request.GET.dict().get("hub.challenge", "123456789"))

    update = json.loads(request.body.decode())
    # print("whatsapp_webhook: ", update)
    for manager in Manager.objects.filter(whatsapp=True, whatsapp_instance=update.get("instanceId")):
        # print("manager wp: ", manager)
        if manager.get_bot('whatsapp').can_work(manager):
            try:
                messages = update.get("messages")
                manager.get_bot('whatsapp').proccess_event(messages)
            except Exception:
                print('err: ', traceback.format_exc())

    return HttpResponse("OK")


def fb_api1(request):
    data = request.GET.dict()
    manager_id = data.get('state')
    if "code" in data:
        manager = Manager.objects.get(id=manager_id)
        code = data["code"]
        user_access_token = requests.get("https://graph.facebook.com/v3.3/oauth/access_token", params={
            "client_id": settings.FB_CLIENT_ID,
            "client_secret": settings.FB_CLIENT_SECRET,
            "redirect_uri": f"{SITE_NAME}/fb_api",
            "code": code,
            "state": manager_id
        }).json()
        user_access_token = user_access_token["access_token"]
        groups = requests.get("https://graph.facebook.com/me/accounts",
                              params={"access_token": user_access_token}).json()
        groups = groups.get("data", [])

        if len(groups) > 0:
            manager.facebook_token = groups[0]["access_token"]
            manager.facebook_group_id = groups[0]["id"]
            manager.facebook_name = groups[0]['name']
            manager.save(force_update=True)

            requests.post(f'https://graph.facebook.com/v4.0/me/subscribed_apps',
                          params={"access_token": manager.facebook_token,
                                  "subscribed_fields": "messaging_postbacks,messages,messaging_referrals,messaging_optins"})

            BotsManager.stop(manager, ['facebook'])
            BotsManager.start(manager, ['facebook'])
    return redirect(f'http://my.chatlead.io/bots/{manager_id}/setup')


def fb_api(request):
    data = request.GET.dict()
    manager_id = data.get('state')
    if "code" in data:
        manager = Manager.objects.get(id=manager_id)
        code = data["code"]
        user_access_token = requests.get("https://graph.facebook.com/v3.3/oauth/access_token", params={
            "client_id": settings.FB_CLIENT_ID,
            "client_secret": settings.FB_CLIENT_SECRET,
            "redirect_uri": f"{SITE_NAME}/fb_api",
            "code": code,
            "state": manager_id
        }).json()
        user_access_token = user_access_token.get("access_token")
        groups = requests.get("https://graph.facebook.com/me/accounts",
                              params={"access_token": user_access_token}).json()
        groups = groups.get("data", [])

        count_active = 0
        count_not_active = 0
        for x in groups:
            exists = Manager.objects.filter(
                facebook_name=x["name"], facebook_group_id=x['id']).exists()
            x['is_active'] = exists

            if exists:
                count_active += 1
            else:
                count_not_active += 1
        print(groups)
        # manager.facebook_token = groups[0]["access_token"]
        # manager.facebook_group_id = groups[0]["id"]
        # manager.facebook_name = groups[0]['name']
        # manager.save(force_update=True)

        # requests.post(f'https://graph.facebook.com/v4.0/me/subscribed_apps',
        #               params={"access_token": manager.facebook_token,
        #                       "subscribed_fields": "messaging_postbacks,messages,messaging_referrals,messaging_optins"})
    return render(request, 'select_group_fb.html', locals())


def vk_api(request, pk):
    data = request.GET.dict()
    manager_id = pk
    manager = Manager.objects.get(id=manager_id)
    print('vk_api1: ', UsersVk.users)
    print('data: ', data)
    state = UsersVk.users[manager.id]["state"]

    if state == "get_user_code":
        code = data["code"]
        response = requests.get("https://oauth.vk.com/access_token", params={
            "client_id": settings.VK_CLIENT_ID,
            "client_secret": settings.VK_CLIENT_SECRET,
            "redirect_uri": f"{SITE_NAME}/vk_api/{manager.id}",
            "code": code
        }).json()

        print("redirect_uri: ", f"{SITE_NAME}/vk_api/{manager.id}")
        UsersVk.users[manager.id].update({
            "user_access_token": response["access_token"],
            "user_id": response["user_id"],
            "state": "selec_group"
        })
        response = requests.get("https://api.vk.com/method/groups.get", params={
            "access_token": UsersVk.users[manager.id]["user_access_token"],
            "user_id": UsersVk.users[manager.id]["user_id"],
            "filter": "admin",
            "extended": 1,
            "count": 1000,
            "v": "5.102",
        }).json()

        print('response["response"]["items"]: ', response["response"]["items"])

        groups = [{"group_id": abs(group["id"]),
                   "group_name": group['name'],
                   "icon": group['photo_50'],

                   } for group in response["response"]["items"]]

        count_active = 0
        count_not_active = 0
        for x in groups:
            x['is_active'] = Manager.objects.filter(
                vk_group_id=x['group_id'], vk_name=x['group_name']).exists()
            if x['is_active']:
                count_active += 1
            else:
                count_not_active += 1
        print('groups ', groups)
        return render(request, 'select_group_vk.html', locals())
    elif state == "get_group_code":
        def request(section, method, access_token, params=None):
            if params is None:
                params = {}
            params.update({
                "access_token": access_token,
                "v": "5.102",
            })
            response = requests.post(
                f"https://api.vk.com/method/{section}.{method}", params).json()
            return response

        manager = UsersVk.users[manager.id]["manager"]
        code = data["code"]
        response = requests.get("https://oauth.vk.com/access_token", params={
            "client_id": settings.VK_CLIENT_ID,
            "client_secret": settings.VK_CLIENT_SECRET,
            "redirect_uri": f"{SITE_NAME}/vk_api/{manager.id}",
            "code": code
        }).json()
        group_id = UsersVk.users[manager.id]["group_id"]
        access_token = response[f"access_token_{group_id}"]
        manager.vk_group_access_token = access_token
        manager.vk_group_id = group_id
        manager.vk_name = UsersVk.users[manager.id]["group_name"]
        manager.save(force_update=True)

        request("groups", "setSettings", access_token, params={
            "group_id": group_id,
            "messages": 1,
            "bots_capabilities": 1,
            "bots_start_button": 1,
            "bots_add_to_chat": 0
        })

        request("groups", "setLongPollSettings", access_token, params={
            "group_id": group_id,
            "enabled": 1,
            "api_version": "5.102",
            "message_new": 1
        })

        BotsManager.stop(manager, ['vk'])
        BotsManager.start(manager, ['vk'])

        return redirect(f'https://my.chatlead.io/bots/{manager.id}/setup')

@csrf_exempt
def vk_api_call_back(request, pk):
    query = request.GET.dict()
    
    try:
        data = json.loads(request.body.decode("utf-8"))
    except JSONDecodeError:
        data = {}
    manager_id = pk
    manager = Manager.objects.get(id=manager_id)
    worker = VkBot_callback(manager)

    t = data.get("type", "")


    if query == {"param": "5"}:
        exist, less_than_ten = worker.is_call_back_server_exists(worker.manager.id, worker.group_id)
        # exist, less_than_ten = (0, 0)
        return HttpResponse(json.dumps(
            {
                "manager_id": worker.manager.id,
                "manager": worker.manager.vk,
                "call_back": {
                    "exist":exist,
                    "count":less_than_ten,
                },
                "token": worker.token,
                "group": worker.group_id,
                "secret": worker.secret_code,
                "next_confirmation": worker.return_confirm_code(worker.group_id),
                "now_confirmation": worker.confirm_handler()
            }
        ))
    elif query == {"param": "6"}:
        return HttpResponse(worker.unit_make_call_back_server())
    elif query == {"param": "7"}:
        return HttpResponse(worker.return_confirm_code(worker.group_id))
    elif data.get("type", "") == "confirmation":
        return HttpResponse(worker.confirm_handler())
    elif data.get("type", "") != "":
        worker.message_handler(data)
        return HttpResponse("ok")
    else:
        return HttpResponse("type not found")


@csrf_exempt
def paybox_webhook(request):
    data = request.POST.dict()
    # data = {}
    order_id = data.get('pg_order_id') or 38
    result = data.get('pg_result') or '1'
    payment = Transactions.objects.get(id=order_id)
    payment.status = 'SECCESS' if result == '1' else 'FAIL'

    payment.save()

    row = '{"val":' + str(payment.bots) + '}'
    row = row.replace('\'', '"')
    row = json.loads(row)

    for x in row.get('val', []):
        bot_id = x.get('bot_id')
        period = x.get('period')
        plan = x.get('plan')

        manager = Manager.objects.get(id=bot_id)
        manager.plan = plan
        manager.payed = payment.status == 'SECCESS'
        if payment.status == 'SECCESS':
            manager.payed_end_date += 30 if period == 1 else 365 if period == 12 else 0
        manager.save()

    user = payment.user
    if user.first_payed:
        user.first_payed = False

        if user.ref:
            print('ref: ', user.ref)
            ref_user = User.objects.get(id=user.ref)
            print('amount: ', user.amount)
            ref_user.amount = ref_user.amount + 5

            print('amount: ', user.amount)
            ref_user.save()
        else:
            print('empty ref')

        user.save()

    return HttpResponse('ok')
