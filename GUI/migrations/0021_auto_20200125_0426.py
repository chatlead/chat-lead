# Generated by Django 2.2.2 on 2020-01-24 22:26

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0020_auto_20200125_0426'),
    ]

    operations = [
        migrations.AddField(
            model_name='pinlink',
            name='watch_count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 1, 25, 4, 26, 39, 125255), null=True),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1579904799.101285),
        ),
    ]
