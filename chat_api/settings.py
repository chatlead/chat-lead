#
# import socket
#
# ip = socket.gethostbyname(socket.gethostname())
# port = 8443
#
# host = f"https://{ip}:{port}"
# print(host)
REDIRECT_BASE = 'https://api.chatlead.io/app'

# FB_CLIENT_ID = 2127582580874473
# FB_CLIENT_SECRET = "54e4e58e08170b214dbdd0a5f98666ba"

FB_CLIENT_ID = 2127582580874473
FB_CLIENT_SECRET = "54e4e58e08170b214dbdd0a5f98666ba"

# VK_CLIENT_ID = 7173683
VK_CLIENT_ID = 7085372
# VK_CLIENT_SECRET = "ycQJbckyfKn81SlIwgtU"
VK_CLIENT_SECRET = "ttKXFSFwNEN6LqopzC35"
SECRET_KEY = '6zpjdg&&t5i^s%skb0w%x#+9t5v75971&bs92nxq*!u3y)'
DEBUG = True
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False
SECURE_SSL_REDIRECT = False
# SECURE_PROXY_SSL_HEADER = ()

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
MEDIA_URL = '/media/'
# UPLOAD_PATH = os.path.join(BASE_DIR, 'uploads')
UPLOAD_PATH = '/home/admin/chatlead/uploads'
if not os.path.exists(UPLOAD_PATH):
    os.makedirs(UPLOAD_PATH)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

ALLOWED_HOSTS = ["localhost", "127.0.0.1", "5.23.53.17", "188.225.27.75", "92.53.127.2", "chatlead.io"]

AUTH_USER_MODEL = 'GUI.User'

LOGIN_REDIRECT_URL = '/app'
LOGOUT_REDIRECT_URL = '/app'

LOGGING_CONFIG = None
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'GUI.apps.GuiConfig',
    'GUI',
    'corsheaders',
    'django_extensions',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'chat_api.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
            ],
        },
    },
]
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
WSGI_APPLICATION = 'chat_api.wsgi.application'

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': os.environ.get('DATABASE_NAME', 'chatlead'),
    #     'USER': os.environ.get('DATABASE_USER', 'chatlead'),
    #     'PASSWORD': os.environ.get('DATABASE_PASSWORD', 'A@##324fdddkdskkmgvmmMMAK323'),
    #     'HOST': os.environ.get('DATABASE_HOST', '127.0.0.1'),
    #     'PORT': os.environ.get('DATABASE_PORT', '5432'), #'32770'),
    # },

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'chatlead_my',
        'USER': 'chatlead_new',
        'PASSWORD': 'qwerty_gh_78',
        'HOST': '127.0.0.1',
        'PORT': '6432',
    }

    # 'default': {
    #         'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #         'NAME': 'chatlead',
    #         'USER': 'chatlead',
    #         'PASSWORD': '1234',
    #         'HOST': '127.0.0.1',
    #         'PORT': '6432',
    #     }

    # 'default': {
    #         'ENGINE': 'django.db.backends.sqlite3',
    #         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    #     }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

# TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Almaty'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = False
DATA_UPLOAD_MAX_MEMORY_SIZE = 1024 * 1024 * 100
