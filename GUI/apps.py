import threading
import traceback
from datetime import datetime

from django.apps import AppConfig
import logging
import time

logger = logging.getLogger('bots')


class GuiConfig(AppConfig):
    name = 'GUI.apps'

    def ready(self):
        job()
        threading.Thread(target=self.start_bots, daemon=True).start()
        threading.Thread(target=broadcast_sender, daemon=True).start()
        threading.Thread(target=run_threaded, daemon=True).start()
        print("active_count: ", threading.active_count())

    @staticmethod
    def start_bots():
        print('launching bots')
        logger.debug('launching bots')

        from .models import Manager, BotsManager
        from django.db.models.functions import Length
        from django.db.models import Q

        for manager in Manager.objects.all().order_by('id'):  # [:10]:  filter(id=893):
            try:
                if manager.payed_end_date == 0:
                    BotsManager.stop(manager)
                    continue
                logger.debug("manager start:" + str(manager) + '-' + str(manager.payed_end_date))
                BotsManager.start(manager)
            except:
                logger.debug("manager start:" + str(traceback.format_exc()))
        print("managers are all ready")

    @staticmethod
    def stop_bots():
        from .models import Manager, BotsManager
        print('stop_bots')
        for manager in Manager.objects.all():
            BotsManager.stop(manager)


def run_threaded():
    print('Sheduler ready!')
    import schedule
    import time

    try:
        # job()
        schedule.every(3).hours.do(job)
        # schedule.every(1).minutes.do(job)
        # schedule.every().day.at("01:30").do(run_threaded, job)

        while True:
            schedule.run_pending()
            time.sleep(1)
    except:
        print('err shedule:', traceback.format_exc())


def job():
    from datetime import date
    from GUI.models import Manager, BotsManager
    from django.db.models import Q
    from datetime import timedelta
    print('start job')
    try:

        managers = Manager.objects.filter(payed_end_date__gte=1,
                                          last_dec_date__lte=(datetime.today() - timedelta(days=1)))

        for manager in managers:
            print('type:', type(date.today()))
            print('type:', type(manager.last_dec_date))
            dec = (date.today() - manager.last_dec_date).days
            print("dec: ", dec)
            dec = manager.payed_end_date - dec
            manager.payed_end_date = dec if dec > 0 else 0
            manager.last_dec_date = datetime.today()

            manager.save()

        for manager in Manager.objects.filter(payed_end_date=0):
            BotsManager.stop(manager)
        print("dec end_date secc")
    except:
        print('err dec end_date:', traceback.format_exc())


def broadcast_sender():
    from .models import BroadcastMessages
    logger.debug("broadcast_sender start - ")
    print(datetime.now().timestamp())
    while True:
        time.sleep(5)
        try:
            # print('next broadcast')
            messages = BroadcastMessages.objects.filter(sent=False, proccessing=False,
                                                        time__lte=datetime.now().timestamp())[:5]

            for broadcast in messages:
                try:
                    broadcast.proccessing = True
                    broadcast.save()
                    print(f'manager{broadcast.manager}')
                    print('broadcast_sender circle: ', broadcast.id)
                    result = broadcast.manager.broadcast(broadcast.scenario, broadcast.is_group, broadcast.tag)
                    broadcast.users_count = result
                    broadcast.save()

                except Exception as e:
                    logger.debug("ex1 " + str(e))
                    print("broadcast error: ", e)
                    print(traceback.format_exc())
                finally:
                    broadcast.sent = True
                    broadcast.proccessing = False
                    broadcast.save()
        except Exception:
            logger.debug("ex2" + e)
            print(traceback.format_exc())
