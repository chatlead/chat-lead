import functools
import io
import json
import logging
import os
import random
import re
import string
import sys
import threading
import time
import time
import traceback
import uuid
from datetime import datetime, timedelta, date
from io import BytesIO
from json import JSONDecodeError
from urllib.request import urlopen

import openpyxl
import requests
import telebot
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django_extensions.db.fields.json import JSONField
from pymessenger import Element
from pymessenger.bot import Bot as FacebookClient
from telebot import apihelper
from telebot.types import CallbackQuery
from chat_api.settings import UPLOAD_PATH
from .mail import send_message
from threading import Lock

default_message = [{'text': '', 'keyboard': []}]
default_messages = {"vk": default_message, "facebook": default_message, "telegram": default_message,
                    "whatsapp": default_message}

API_URL = 'https://api.chatlead.io'
PROJECT_NAME = "Chatlead"

bots_logger = logging.getLogger('bots')
bots_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter(
    '[%(asctime)s, %(levelname)s]: %(message)s'))
bots_logger.addHandler(handler)


# apihelper.proxy = {"https": "socks5://jJPmn1:gGnwE4@196.17.170.94:8000"}
# apihelper.proxy = {'https': 'https://196.17.170.94:8000'}

def user_directory_path(self, file):
    path = UPLOAD_PATH
    for x in ['/user_icons/', f'{self.id}/']:
        path += x
        print(path)
        if not os.path.exists(os.path.dirname(path)):
            print("create")
            os.makedirs(os.path.dirname(path))
        else:
            print("exist")
    return f'{UPLOAD_PATH}/user_icons/{self.id}/{file}'


def uuid_my():
    return uuid.uuid4()


def random_id_15():
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(15))


def date_today_to_timeshtamp():
    return datetime.combine(date.today(), datetime.min.time()).timestamp()


def get_last_main_message(bot, manager_id, user_id, message_number, text):
    last_message = WhatsAppReceivedMsg.objects.filter(manager=manager_id, user_id=user_id,
                                                      message_number__lte=message_number).order_by(
        '-message_number').first()

    scenario = bot.find_scenario(last_message.text.replace(
        'wa_', '')) if last_message else None

    return last_message if scenario and scenario.triggers.filter(caption__iexact=text).first() else None


def genchoices(*args):
    return tuple([(str(c), str(c)) for c in args])


def auto_increment():
    last = Trigger.objects.all().order_by('id').last()
    return int(last.id) + 1 if last else 10000


def auto_increment_scenario():
    last = Scenario.objects.all().order_by('id').last()
    return int(last.id) + 1 if last else 10000


def auto_increment_ride():
    last = AutoRide.objects.all().order_by('id').last()
    return int(last.id) + 1 if last else 10000


class Transactions(models.Model):
    user = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)
    to_pay = models.IntegerField(default=0)
    status = models.CharField(max_length=40, default="")
    bots = models.CharField(max_length=256, default="")
    add_date = models.IntegerField(default=datetime.now().timestamp())


class AutoRide(models.Model):
    id = models.AutoField(auto_created=True, unique=True,
                          default=auto_increment_ride, primary_key=True)
    manager = models.ForeignKey('Manager', on_delete=models.CASCADE, null=True)
    trigger_text = models.TextField(blank=True, null=True)
    scenario = models.ForeignKey(
        'Scenario', on_delete=models.CASCADE, null=True)


class Scenario(models.Model):
    id = models.AutoField(auto_created=True, unique=True,
                          default=auto_increment_scenario, primary_key=True)
    destination = models.CharField(max_length=20, default='',
                                   choices=genchoices('', 'broadcast', 'autoride', 'subscribe', 'unsubscribe'))
    manager = models.ForeignKey('Manager', on_delete=models.CASCADE, null=True)
    trigger_text = models.TextField(blank=True, null=True)

    indexes = [
        models.Index(fields=['manager', 'trigger_text', ]),
    ]


class ScenarioTriggerText(models.Model):
    scenario = models.ForeignKey(
        'Scenario', on_delete=models.CASCADE, null=True, related_name="scenario_trigger_text")
    trigger_text = models.TextField(blank=True, null=True)


class Trigger(models.Model):
    id = models.AutoField(auto_created=True, unique=True,
                          default=auto_increment, primary_key=True)
    caption = models.CharField(
        max_length=256, null=True, blank=True, default="")
    social = models.CharField(max_length=20, default='telegram',
                              choices=genchoices('telegram', 'vk', 'whatsapp', 'facebook'))
    scenario = models.ForeignKey(
        Scenario, on_delete=models.CASCADE, null=True, blank=True, related_name="triggers")
    keyboard = models.CharField(
        max_length=256, null=True, blank=True, default="{}")
    messages = JSONField(default=default_messages, null=True, blank=True)

    def __str__(self):
        return "[%s - %s]" % (self.caption, self.id)


class Tag(models.Model):
    action = models.CharField(
        max_length=20, default='add', choices=genchoices('add', 'remove'))
    tag_name = models.CharField(max_length=30, default='')
    trigger = models.ForeignKey(
        'Trigger', on_delete=models.CASCADE, related_name='tags')


class User(AbstractUser):
    email = models.CharField(
        max_length=256, blank=False, null=False, unique=True)
    username = models.CharField(
        max_length=16, blank=True, null=True, unique=False, default="")
    user_token = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    utm_source = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    ref = models.IntegerField(null=True, blank=True, unique=False)
    phone = models.CharField(max_length=20, null=True, blank=True, default="")
    photo = models.CharField(max_length=256, null=True, blank=True, default="")
    amount = models.DecimalField(default=0, max_digits=11, decimal_places=2)
    first_payed = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return f"[User: {self.email} ({self.password})]"


class BroadcastMessages(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    scenario = models.ForeignKey(
        'Scenario', on_delete=models.CASCADE, null=True)
    tag = models.CharField(max_length=256, null=True,
                           blank=True, unique=False, default="")
    time = models.IntegerField(null=True, blank=True, unique=False)
    users_count = models.IntegerField(
        null=True, blank=True, unique=False, default=0)
    sent = models.BooleanField(default=False)
    proccessing = models.BooleanField(default=False)
    is_group = models.BooleanField(default=False)

    def get_time(self):
        return datetime.fromtimestamp(self.time).strftime("%Y-%m-%d %H:%M:%S")


class UserTag(models.Model):
    manager = models.ForeignKey('Manager', on_delete=models.CASCADE)
    text = models.CharField(max_length=256, null=True,
                            blank=True, unique=False)


class PinLink(models.Model):
    link = models.CharField(
        primary_key=True, max_length=15, default=random_id_15)
    text = models.CharField(max_length=256)
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    bot_place = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    is_redirect = models.BooleanField(default=False)
    add_tags = models.CharField(max_length=1024, null=True, blank=True)
    delete_tags = models.CharField(max_length=1024, null=True, blank=True)
    watch_count = models.PositiveIntegerField(default=0)


class WhatsAppUsers(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    name = models.CharField(max_length=512, null=True,
                            blank=True, unique=False, default="")
    tag = models.CharField(max_length=256, null=True,
                           blank=True, unique=False, default="")
    mode = models.CharField(max_length=128, blank=True,
                            null=True, unique=False, default="main")
    data = models.CharField(max_length=4096, blank=True,
                            null=True, unique=False, default="main")
    user_data = models.CharField(
        max_length=4096, blank=True, null=True, unique=False, default="main")
    subscrib_data = models.IntegerField(default=date_today_to_timeshtamp)
    is_group = models.BooleanField(default=False)
    user_tag = models.ManyToManyField(UserTag, related_name='user_tags_wa')
    link = models.CharField(unique=True, max_length=36, default=uuid.uuid4)
    icon = models.ImageField(max_length=256, blank=True,
                             null=True, upload_to=user_directory_path)
    icon_url = models.URLField(max_length=256, null=True, blank=True)
    is_unsub = models.BooleanField(default=False)
    unsub_date = models.IntegerField(null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            import requests

            if self.icon_url:
                filename = self.icon_url.split('/')[-1]
                path = user_directory_path(self, filename)
                r = requests.get(self.icon_url, allow_redirects=True)
                open(path, 'wb').write(r.content)
                self.icon = path
                self.icon_url = ''
        except:
            self.icon_url = ''
            bots_logger.debug('WhatsAppUsers save: ' +
                              str(traceback.format_exc()))

        super().save()

    indexes = [
        models.Index(fields=['manager', 'subscrib_data', ]),
        models.Index(fields=['manager', 'user_id', ]),
    ]


class TelegramUsers(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    name = models.CharField(max_length=512, null=True,
                            blank=True, unique=False, default="")
    tag = models.CharField(max_length=256, null=True,
                           blank=True, unique=False, default="")
    mode = models.CharField(max_length=128, blank=True,
                            null=True, unique=False, default="main")
    data = models.CharField(max_length=4096, blank=True,
                            null=True, unique=False, default="main")
    user_data = models.CharField(
        max_length=4096, blank=True, null=True, unique=False, default="main")
    subscrib_data = models.IntegerField(default=date_today_to_timeshtamp)
    is_group = models.BooleanField(default=False)
    user_tag = models.ManyToManyField(UserTag, related_name='user_tags_tg')
    link = models.CharField(unique=True, max_length=36, default=uuid.uuid4)
    icon = models.ImageField(max_length=256, blank=True,
                             null=True, upload_to=user_directory_path)
    icon_url = models.URLField(max_length=256, null=True, blank=True)
    is_unsub = models.BooleanField(default=False)
    unsub_date = models.IntegerField(null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            if self.icon_url:
                filename = self.icon_url.split('/')[-1]
                path = user_directory_path(self, filename)
                self.icon = path
                self.icon_url = ''
        except:
            bots_logger.debug('TelegramUsers save: ' +
                              str(traceback.format_exc()))

        try:
            super().save()
        except Exception as e:
            print("err:", e)

    indexes = [
        models.Index(fields=['manager', 'subscrib_data', ]),
        models.Index(fields=['manager', 'user_id', ]),
    ]


class VkUsers(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    name = models.CharField(max_length=512, null=True,
                            blank=True, unique=False, default="")
    tag = models.CharField(max_length=256, null=True,
                           blank=True, unique=False, default="")
    mode = models.CharField(max_length=128, blank=True,
                            null=True, unique=False, default="main")
    data = models.CharField(max_length=4096, blank=True,
                            null=True, unique=False, default="main")
    user_data = models.CharField(
        max_length=4096, blank=True, null=True, unique=False, default="main")
    subscrib_data = models.IntegerField(default=date_today_to_timeshtamp)
    is_group = models.BooleanField(default=False)
    user_tag = models.ManyToManyField(UserTag, related_name='user_tags_vk')
    link = models.CharField(unique=True, max_length=36, default=uuid.uuid4)
    icon = models.ImageField(max_length=256, blank=True,
                             null=True, upload_to=user_directory_path)
    icon_url = models.URLField(max_length=256, null=True, blank=True)
    is_unsub = models.BooleanField(default=False)
    unsub_date = models.IntegerField(null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            import requests

            if self.icon_url:
                filename = self.icon_url.split('/')[-1]
                path = user_directory_path(self, filename)
                r = requests.get(self.icon_url, allow_redirects=True)
                open(path, 'wb').write(r.content)
                self.icon = path
                self.icon_url = ''
        except:
            bots_logger.debug('VkUsers save: ' + str(traceback.format_exc()))

        super().save()

    indexes = [
        models.Index(fields=['manager', 'subscrib_data', ]),
        models.Index(fields=['manager', 'user_id', ]),
    ]


class FacebookUsers(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    name = models.CharField(max_length=512, null=True,
                            blank=True, unique=False, default="")
    tag = models.CharField(max_length=256, null=True,
                           blank=True, unique=False, default="")
    mode = models.CharField(max_length=128, blank=True,
                            null=True, unique=False, default="main")
    data = models.CharField(max_length=4096, blank=True,
                            null=True, unique=False, default="main")
    user_data = models.CharField(
        max_length=4096, blank=True, null=True, unique=False, default="main")
    subscrib_data = models.IntegerField(default=date_today_to_timeshtamp)
    is_group = models.BooleanField(default=False)
    user_tag = models.ManyToManyField(UserTag, related_name='user_tags_fb')
    link = models.CharField(unique=True, max_length=36, default=uuid.uuid4)
    icon = models.ImageField(max_length=256, blank=True,
                             null=True, upload_to=user_directory_path)
    icon_url = models.URLField(max_length=1024, null=True, blank=True)
    is_unsub = models.BooleanField(default=False)
    unsub_date = models.IntegerField(null=True, blank=True)

    def save(self, *args, **kwargs):
        import wget

        if self.icon_url:
            filename = f'{self.user_id}.jpg'
            path = user_directory_path(self, filename)
            # r = requests.get(self.icon_url, allow_redirects=True)
            wget.download(self.icon_url, out=path, bar=None)
            self.icon = path
            self.icon_url = ''
        super().save()

    indexes = [
        models.Index(fields=['manager', 'subscrib_data', ]),
        models.Index(fields=['manager', 'user_id', ]),
    ]


class WhatsAppDelayMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    plan_time = models.IntegerField(null=True, blank=True, unique=False)
    sent = models.BooleanField(default=False)
    is_activity_lost = models.BooleanField(default=False)

    indexes = [
        models.Index(fields=['manager', 'sent', 'plan_time', ]),
    ]


class TelegramDelayMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    plan_time = models.IntegerField(null=True, blank=True, unique=False)
    sent = models.BooleanField(default=False)
    is_activity_lost = models.BooleanField(default=False)

    indexes = [
        models.Index(fields=['manager', 'sent', 'plan_time', ]),
    ]


class VkDelayMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    plan_time = models.IntegerField(null=True, blank=True, unique=False)
    sent = models.BooleanField(default=False)
    is_activity_lost = models.BooleanField(default=False)

    indexes = [
        models.Index(fields=['manager', 'sent', 'plan_time', ]),
    ]


class FacebookDelayMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    plan_time = models.IntegerField(null=True, blank=True, unique=False)
    sent = models.BooleanField(default=False)
    is_activity_lost = models.BooleanField(default=False)

    indexes = [
        models.Index(fields=['manager', 'sent', 'plan_time', ]),
    ]


class WhatsAppReceivedMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    text = models.TextField(null=False, blank=False, unique=False)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    sender_username = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    sender_name = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    time = models.CharField(max_length=256, null=True,
                            blank=True, unique=False)
    message_number = models.IntegerField(null=True, blank=True, unique=False)
    proccessed = models.BooleanField(default=False)
    user_data = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    is_bot = models.BooleanField(default=False)
    message_type = models.CharField(default='text', max_length=50)

    def __str__(self):
        return f"Message {self.manager}"


class TelegramReceivedMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    text = models.TextField(null=False, blank=False, unique=False)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    sender_username = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    sender_name = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    chat_title = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    time = models.CharField(max_length=256, null=True,
                            blank=True, unique=False)
    message_id = models.IntegerField(null=True, blank=True, unique=False)
    proccessed = models.BooleanField(default=False, null=True, blank=True)
    user_data = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    is_bot = models.BooleanField(default=False)
    message_type = models.CharField(
        default='text', max_length=50, null=True, blank=True)

    def __str__(self):
        return f"Message {self.manager}"


class VkReceivedMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    message_id = models.IntegerField(null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    time = models.IntegerField(null=True, blank=True, unique=False)
    user_data = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    is_bot = models.BooleanField(default=False)
    message_type = models.CharField(default='text', max_length=50)
    sender_username = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    sender_name = models.CharField(
        max_length=512, null=True, blank=True, unique=False)

    def __str__(self):
        return f"Message {self.manager}"


class FacebookReceivedMsg(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True)
    message_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    text = models.TextField(null=False, blank=False, unique=False)
    user_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    chat_id = models.CharField(
        max_length=256, null=True, blank=True, unique=False)
    time = models.IntegerField(null=True, blank=True, unique=False)
    user_data = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    is_bot = models.BooleanField(default=False)
    message_type = models.CharField(default='text', max_length=50)
    sender_username = models.CharField(
        max_length=512, null=True, blank=True, unique=False)
    sender_name = models.CharField(
        max_length=512, null=True, blank=True, unique=False)

    def __str__(self):
        return f"Message {self.manager}"


class FormLogging(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=256, null=True, blank=True)
    submit_date = models.DateField(default=date.today())
    submit_date_int = models.IntegerField(default=date_today_to_timeshtamp)
    source = models.CharField(max_length=20, null=True, blank=True)
    form = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(
        max_length=256, null=True, blank=True, default='new')


class StackMessages(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True, blank=True)
    trigger = models.CharField(max_length=256)
    current_volume = models.PositiveIntegerField(default=0)
    default_volume = models.PositiveIntegerField(default=1)
    order = models.PositiveIntegerField(default=0)
    url = models.CharField(max_length=256)


class MLP(models.Model):
    link = models.CharField(primary_key=True, unique=True,
                            max_length=36, default=uuid.uuid4)
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True, blank=True)
    setting_title = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    autoride = models.ForeignKey(
        'AutoRide', on_delete=models.SET_NULL, null=True, blank=True)
    social = models.TextField(null=True, blank=True, unique=False, default="")
    media_image = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    media_video = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    first_title = models.CharField(
        max_length=100, null=True, blank=True, unique=False, default="")
    first_phone = models.CharField(
        max_length=100, null=True, blank=True, unique=False, default="")
    second_title = models.CharField(
        max_length=100, null=True, blank=True, unique=False, default="")
    second_phone = models.CharField(
        max_length=100, null=True, blank=True, unique=False, default="")
    action_title = models.CharField(
        max_length=100, null=True, blank=True, unique=False, default="")
    script_for_head = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    script_for_body = models.CharField(
        max_length=256, null=True, blank=True, unique=False, default="")
    create_date = models.PositiveIntegerField(
        default=datetime.now().timestamp())


class MLPForm(models.Model):
    manager = models.ForeignKey(
        'Manager', on_delete=models.SET_NULL, null=True, blank=True)
    mlp = models.ForeignKey(
        'MLP', on_delete=models.SET_NULL, null=True, blank=True)
    phone = models.CharField(max_length=256, null=True,
                             blank=True, unique=False, default="")


class Bot():
    BotUsers = lambda *args, **kwargs: models.Model()
    BotDelayMsg = lambda *args, **kwargs: models.Model()
    BotReceivedMsg = lambda *args, **kwargs: models.Model()
    bot_place = ""

    @staticmethod
    def can_work(manager):
        return False

    def __init__(self, manager):
        self.manager = manager

    def start(self):
        return True

    def stop(self):
        return True

    def wait_stopping(self):
        while True:
            time.sleep(0.1)
            if not self.is_any_work():
                break

    def get_hide_link(self, text):
        return text

    def send_file(self, *args, **kwargs):
        return True

    def send_contact(self, *args, **kwargs):
        return True

    def send_message(self, *args, **kwargs):
        return True

    def send_card(self, user_id, title, text, image_url, keyboard=None):
        return True

    def get_dialogs(self):
        return False

    def read_chat(self, chat_id):
        return False

    def broadcast(self, scenario, is_group=False, tag=""):
        print("broadcast bot: ", self.bot_place, ' ', is_group)

        # bots_logger.debug("broadcast bot: " + self.bot_place, is_group)

        result = 0
        users = self.BotUsers.objects.filter(manager=self.manager) if is_group \
            else self.BotUsers.objects.filter(manager=self.manager, is_group=is_group).order_by('id')

        print('users count: ', users.count())
        tags = tag.lower().split(',')
        for user in users:
            try:
                print("userbrod: ", user)
                trigger: Trigger = scenario.triggers.first()

                if tag != '' and not self.check_tag(tags, user):
                    print('skip', user)
                    continue

                if user.is_unsub:
                    continue

                self.message_routine(trigger.messages, user, scenario)
                print("userbrod:11 ", user)
                result += 1
            except Exception as e:
                print("broadcast err: ", self.bot_place, ' ', e)
                print(traceback.format_exc())
        return result

    def set_typing(self, chat_id, delay):
        return True

    def get_dialogs_count(self):
        return 0

    def run_threads(self, threads=None, send_delay_messages=True):
        if threads is None:
            threads = []

        for thread in self.threads.get(self.manager.id, {}).values():
            if thread.is_alive():
                return False

        if "send_delay_messages" not in [thread["name"] for thread in threads]:
            threads.append({
                "name": "send_delay_messages",
                "target": self.send_delay_messages
            })

        self.threads.update({self.manager.id: {
            thread["name"]: threading.Thread(
                target=thread["target"], args=thread.get("args", ()), daemon=True
            ) for thread in threads
        }})
        for thread in self.threads.get(self.manager.id, {}).values():
            thread.start()

        return self.is_all_work()

    def is_all_work(self):
        try:
            return all(thread.is_alive() for key, thread in self.threads.get(self.manager.id, {}).items())
        except TypeError:
            return False

    def is_any_work(self):
        try:
            return any(thread.is_alive() for key, thread in self.threads.get(self.manager.id, {}).items())
        except TypeError:
            return False

    def fix_work_status(self):
        # bots_logger.debug(f"fix {self.bot_place}, {self.threads}, {self.is_all_work()} {self.is_all_work()}")
        if not self.is_any_work():
            if getattr(self.manager, self.bot_place):
                setattr(self.manager, self.bot_place, False)
                self.manager.save()
        elif self.is_any_work() and not self.is_all_work():
            self.stop()
        elif self.is_all_work():
            if not getattr(self.manager, self.bot_place):
                setattr(self.manager, self.bot_place, True)
                self.manager.save()

    def send_delay_messages(self):
        while getattr(self.manager, self.bot_place):
            try:
                if not getattr(self.manager, self.bot_place):
                    print('break')
                    break
                messages = self.BotDelayMsg.objects.filter(
                    manager=self.manager, sent=False, plan_time__lte=datetime.now().timestamp())
                # plan_time__lte=datetime.now().timestamp())[:5]
                # for message in messages:
                #     message.sent = True
                #     message.save()
                # messages.update(sent=True)

                for message in messages:
                    try:
                        message.sent = True
                        if not getattr(self.manager, self.bot_place):
                            break

                        data = json.loads(message.text)
                        scenario = self.find_scenario_by_id(
                            data['scenario_id'])
                        user = self.BotUsers.objects.filter(
                            manager=self.manager, user_id=message.chat_id).first()

                        self.message_routine(
                            data['messages'], user, scenario, json.dumps(data['fast_buttons']))
                        message.save()
                    except Exception as e:
                        # bots_logger.debug('-----------------1111\n:' + str(traceback.format_exc()))
                        bots_logger.debug('-----------------1111\n:' + str(e))
                        # print('send_delay_messages :1: ', e)
                        # print(traceback.format_exc())
                # time.sleep(1)
            except Exception as e:
                bots_logger.debug('-----------------\n:' + str(traceback.format_exc()))
                # print('send_delay_messages :2: ', e)
                # print('send_delay_messages', traceback.format_exc())
                # return

    def correct_delay_msg(self, chat_id):
        messages = self.BotDelayMsg.objects.filter(manager=self.manager, is_activity_lost=True, chat_id=chat_id,
                                                   sent=False, plan_time__gte=datetime.now().timestamp())
        try:
            for message in messages:
                message.is_activity_lost = False
                # print('correct_delay_msg1: ', message.text)
                ms = json.loads(message.text)
                ms = ms.get('messages').get(self.bot_place)

                # print('len: ', len(ms))
                if len(ms) > 1:
                    ms.pop(0)
                    message.text = json.dumps({'messages': {self.bot_place: ms},
                                               'scenario_id': json.loads(message.text).get('scenario_id'),
                                               'fast_buttons': json.loads(message.text).get('fast_buttons')})

                    # print('correct_delay_msg2: ', message.text)
                    message.plan_time = datetime.now().timestamp()
                else:
                    message.sent = True

                message.save()

        except Exception as e:
            print('correct_delay_msg err: ', e)

    @staticmethod
    def get_path_from_url(url: str) -> str:
        return '/home/admin/chatlead/uploads/' + url.split('/media/')[1]

    def find_scenario(self, trigger_text):
        scenario = Scenario.objects.filter(
            manager=self.manager, trigger_text__iexact=trigger_text).first()

        try:
            if scenario:
                return scenario

            scenario1 = Scenario.objects.filter(
                manager=self.manager, trigger_text__icontains=trigger_text)
            if scenario1.count() == 1 and scenario1[0].trigger_text.lower() == trigger_text.lower():
                return scenario1[0]

            for sc in scenario1:
                if trigger_text.lower() in [tr.lower() for tr in sc.trigger_text.split(",")]:
                    return sc

        except Exception as e:
            print('find_scenario: ', e)

    def find_scenario_by_id(self, scenario_id):
        scenario = Scenario.objects.filter(
            manager=self.manager, id=scenario_id)
        if scenario.exists():
            return scenario[0]

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        return True

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        return True

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        return True

    def find_trigger(self, scenario, trigger_id=None):
        if not trigger_id and scenario:
            trigger = scenario.triggers.all().order_by('id').first()

            if trigger:
                return trigger
            else:
                return None
        if trigger_id and scenario:
            return Trigger.objects.get(scenario=scenario, id=trigger_id)

    def find_trigger_by_id(self, trigger_id):
        if trigger_id:
            return Trigger.objects.get(id=trigger_id)

    def trigger_routine(self, scenario, trigger, user):
        if not trigger:
            return
        result = True
        for tag in trigger.tags.all():
            if tag.action == 'add' and tag.tag_name not in user.tag:
                user.tag = f"{user.tag} {tag.tag_name}"
            if tag.action == 'remove':
                user.tag = str(user.tag).replace(f"{tag.tag_name}", "").strip()
            user.save()

        result = self.message_routine(
            trigger.messages, user, scenario, trigger.keyboard)
        return result

    def check_tag(self, tags, user, is_all=False):
        from functools import reduce
        import operator
        from django.db.models import Q

        try:
            if len(tags) == 1 and tags[0] == '':
                return False

            is_exist = self.BotUsers.objects.filter(
                manager=self.manager, id=user.id)

            # print('-------------', is_all)
            # print(is_exist.first().user_tag.all().values())
            # print(tags)

            if is_all:
                arr = [x['text']
                       for x in is_exist.first().user_tag.all().values('text')]
                is_exist = True

                for x in tags:
                    # print(x, tags, x in arr)
                    is_exist = is_exist and x in arr
            else:
                is_exist = is_exist.filter(user_tag__text__in=tags).exists()

            return is_exist
        except:
            print(traceback.format_exc())
            return True

    def message_routine(self, messages, user, scenario=None, fast_buttons=None):
        try:
            if self.manager.payed_end_date < 1:
                print('if self.manager.payed_end_date < 1')
                return
            messages: list = messages.get(self.bot_place)
            if not messages:
                print("return not messages")
                return
            try:
                if fast_buttons:
                    fast_buttons = json.loads(
                        fast_buttons).get(self.bot_place, [])
                else:
                    fast_buttons = []
            except:
                fast_buttons = []
            result = True

            for tg_message in messages:
                kwargs = {}
                keyboard = tg_message.get('keyboard', []) + fast_buttons
                if keyboard:
                    for button in keyboard:
                        btn_type = button.get('type')
                        if btn_type not in kwargs:
                            kwargs[btn_type] = []

                        if button.get('secondType', '') == 'url_buttons':
                            url = button.get('payload', {}).get('url', '')
                            add_tags = button.get('addTags')
                            delete_tags = button.get('deleteTags')
                            url = self.get_redirect(
                                user, url, add_tags, delete_tags)
                            button['payload']['url'] = url

                        kwargs[btn_type].append(button)

                for field in tg_message:
                    # print("field: ", field)
                    text = tg_message.get('text')
                    tags = tg_message.get('tag', '')
                    exclude_tags = tg_message.get('exclude_tags', '')
                    is_tag_all = tg_message.get('is_tag_all', False)
                    is_exclude_tag_all = tg_message.get(
                        'is_exclude_tag_all', False)

                    if exclude_tags != '' and self.check_tag(exclude_tags.split(','), user, is_exclude_tag_all):
                        # print('skip exclude', user)
                        break

                    if tags != '' and not self.check_tag(tags.split(','), user, is_tag_all):
                        # print('skip tag', user)
                        break

                    try:
                        try:
                            user_data = json.loads(user.user_data)
                        except (JSONDecodeError, TypeError):
                            print('errrrrrror')
                            user_data = {}

                        username = user_data.get('username', '')
                        if username is None:
                            username = ''

                        first_name = user_data.get('first_name', '')
                        if first_name is None:
                            first_name = ''

                        last_name = user_data.get('last_name', '')
                        if last_name is None:
                            last_name = ''

                        phone = user_data.get('phone', '')
                        if phone is None:
                            phone = ''

                        text = str(text).replace('{uuid}', str(user.link))
                        text = str(text).replace(
                            '{chat-id}', str(user.user_id))
                        text = str(text).replace('{name}', username)
                        text = str(text).replace('{first_name}', first_name)
                        text = str(text).replace('{last_name}', last_name)
                        text = str(text).replace('{phone}', phone)
                    except Exception as e:
                        print('errrrrrror1')
                        print(traceback.format_exc())
                    if field == "timer":
                        try:
                            timing = tg_message.get('timer', {})

                            pause = timing.get('pause_delay')
                            activity_lost = timing.get('send_time')
                            send_time = timing.get('activity_lost')

                            print('pause_delay1111: ', pause)
                            if pause and type(pause) != int:
                                pause = int(pause.get('value'))

                            if activity_lost:
                                day = int(activity_lost.get('day'))
                                hours = int(activity_lost.get('hours'))
                                min = int(activity_lost.get('min'))
                                activity_lost = day + hours + min

                            print('int(datetime.now().timestamp(): ',
                                  int(datetime.now().timestamp()))
                            plan_time = int(datetime.now().timestamp() + pause) if pause \
                                else send_time if send_time \
                                else int(datetime.now().timestamp() + activity_lost) if activity_lost else None
                            print('plan_time111: ', plan_time)
                            if plan_time:
                                index = messages.index(tg_message)
                                json_obj = messages[index + 1:]
                                # {'text': tg_message.get('text'), 'keyboard': tg_message.get('keyboard')} \
                                # if activity_lost else messages[index + 1:]

                                if activity_lost:
                                    json_obj = [{'text': tg_message.get('text'),
                                                 'keyboard': tg_message.get('keyboard')}] + json_obj
                                # print('json_obj: ', json_obj)
                                text = {
                                    'messages': {self.bot_place: json_obj},
                                    'scenario_id': scenario.id, 'fast_buttons': fast_buttons}

                                print('text mr: ', text)
                                print('plan_time: ', plan_time)
                                text = json.dumps(text)
                                s = self.BotDelayMsg(
                                    manager=self.manager,
                                    chat_id=user.user_id,
                                    text=text,
                                    plan_time=plan_time,
                                    is_activity_lost=True if activity_lost else False,
                                    sent=False,
                                )
                                s.save()

                                return True
                        except Exception as e:
                            print('errrrrrror3')
                            print(traceback.format_exc())
                    elif field == 'type_processing':
                        delay = tg_message.get(
                            "type_processing").get("delay", 1)
                        result = self.set_typing(user.user_id, delay)
                    elif field == 'text':
                        result = self.send_message(
                            user.user_id, text, keyboard=kwargs)
                    elif field in ['photo', 'video', 'audio', 'file']:
                        try:
                            # print('this foto')
                            attach_set = {'photo': self.send_photo, 'video': self.send_video, 'audio': self.send_audio,
                                          'file': self.send_file}

                            url = tg_message.get(field, {})
                            result = attach_set[field](
                                user.user_id, url=f'{API_URL}{url}')
                            self.save_log(
                                user.user_id, f'{API_URL}{url}', 'media')
                        except:
                            print('errrrrrror4')
                            bots_logger.debug(
                                'error photo: ' + str(traceback.format_exc()))
                    elif field == 'call' and scenario:
                        # elif field == 'call' and scenario:
                        call = tg_message.get('call')
                        trigger_id = call.get('trigger_id')
                        scenario_id = call.get('scenario_id')

                        if trigger_id:
                            new_trigger = self.find_trigger(
                                scenario, trigger_id)
                            if new_trigger:
                                result = self.trigger_routine(
                                    scenario, new_trigger, user)
                        elif scenario_id:
                            new_scenario = self.find_scenario_by_id(
                                scenario_id)
                            new_trigger = self.find_trigger(new_scenario)
                            if new_trigger:
                                result = self.trigger_routine(
                                    scenario, new_trigger, user)
                    elif field == 'card':
                        card = tg_message.get('card')[0]
                        try:
                            self.send_card(user.user_id, card['title'], card['text'], f'{API_URL}{card["photo"]}',
                                           kwargs)
                        except:
                            print('errrrrrror5')
                            print(traceback.format_exc())
                    elif field == 'gallery':
                        gallery = tg_message.get('gallery')

                        try:
                            self.send_gallery(user.user_id, gallery, kwargs)
                        except:
                            print('errrrrrror6')
                            print(traceback.format_exc())
                    elif field == 'payment':
                        payment = tg_message.get('payment')

                        scenario_id = str(tg_message.get('trigger_id')) if tg_message.get(
                            'trigger_id') else ''
                        failure_text = tg_message.get(
                            'failure_text') if tg_message.get('failure_text') else ''
                        amount = str(tg_message.get('amount')) if tg_message.get(
                            'failure_text') else ''
                        fio = (tg_message.get('name') + ' ' + tg_message.get('patronymic')) \
                            if tg_message.get('name') and tg_message.get('patronymic') else ''

                        if scenario_id and failure_text and fio:
                            user.mode = "payment:0:{0}:{1}:{2}:{3}".format(
                                scenario_id, failure_text, amount, fio)
                            user.save()
                    elif field == 'form':
                        form = tg_message.get('form')

                        user.mode = "application:0"
                        user.data = json.dumps({
                            "fields": [field.get('caption') for field in form],
                            "types": [field.get('type') for field in form],
                            "values": []
                        })
                        user.save()
                        self.send_message(user.user_id, form[0].get('caption'))
                    elif field == 'sendLink':
                        link = tg_message.get('sendLink', {})

                        url = link.get('url')
                        filename = link.get('filename')
                        title = link.get('title')
                        description = link.get('description', '')
                        print('start link')
                        response = self.send_link(
                            user.user_id, url, filename, title, description)
                        print("response:", response)
                    elif field == 'sendContact':
                        try:
                            print('sendContact start')
                            send_contact = tg_message.get('sendContact', {})

                            contact_id = send_contact.get('contactId')
                            print('contact_id: ', contact_id)
                            res = self.send_contact(user.user_id, contact_id)
                            print(res)
                        except:
                            print("send_contact error1: ",
                                  traceback.format_exc())
                    elif field == 'updateTag':
                        tags = tg_message.get('updateTag', {})

                        set_tag = tags.get('setTag', [])
                        remove_tag = tags.get('removeTag', [])

                        self.update_tag(user.user_id, set_tag, remove_tag)
                    elif field == 'customs':
                        customs = tg_message.get('customs', {})
                        self.parse_customs(
                            messages, user, scenario, fast_buttons, customs)
                    elif field == 'sendUrl':
                        # print('field sendUrl')
                        send_url = tg_message.get('sendUrl', {})
                        url = send_url.get('url')
                        url = url.replace('{uuid}', str(user.link))
                        try:
                            set_tag = send_url.get('setTag', [])
                            if set_tag:
                                set_tag = ','.join(set_tag)

                            remove_tag = send_url.get('delTag', [])
                            if remove_tag:
                                remove_tag = ','.join(remove_tag)

                            url = self.get_redirect(
                                user, url, set_tag, remove_tag)
                            self.send_message(user.user_id, url)
                            # self.update_tag(user.user_id, set_tag, remove_tag)
                        except:
                            print('errrrrrror8')
                            print(traceback.format_exc())

                    elif field == 'nextTrigger':
                        next_trigger = tg_message.get('nextTrigger', {})
                        trigger_id = next_trigger.get('trigger_id')

                        if trigger_id and scenario:
                            # new_trigger = self.find_trigger(scenario, trigger_id)
                            new_trigger = self.find_trigger_by_id(trigger_id)
                            if new_trigger:
                                result = self.trigger_routine(
                                    scenario, new_trigger, user)
                        return result
        except:
            print('errrrrrror9')
            print(traceback.format_exc())
        return result

    def get_redirect(self, user, url, add_tags=None, delete_tags=None):
        link = PinLink()
        link.text = url
        link.manager = self.manager
        link.bot_place = self.bot_place
        link.user_id = user.user_id
        link.is_redirect = False
        link.add_tags = add_tags
        link.delete_tags = delete_tags
        link.save()

        return f'http://chatl.cc/{link.link}'

    def parse_customs(self, messages, user, scenario, fast_buttons, tg_message):
        print('parse_customs')
        for field in tg_message:
            if field == 'arrayLink':
                array_link = tg_message.get('arrayLink', {})
                trigger = str(array_link.get('trigger_id', ''))
                # trigger = self.find_trigger_by_id(trigger_id) if trigger_id else None

                if not trigger:
                    continue

                volume = array_link.get('volume')
                links = array_link.get('links', [])
                messages = StackMessages.objects.filter(
                    manager=self.manager, trigger=trigger)

                if len(links) > 0:
                    messages = messages.exclude(url__in=[x for x in links])

                messages.delete()
                # StackMessages
                count = 0
                for url in links:
                    msg = StackMessages.objects.filter(
                        manager=self.manager, trigger=trigger, url=url).first()

                    if not msg:
                        msg = StackMessages(
                            manager=self.manager, trigger=trigger, url=url)

                    msg.default_volume = volume
                    msg.order = count
                    msg.save()
                    count += 1

                stack = StackMessages.objects.filter(
                    manager=self.manager, trigger=trigger).order_by('order')

                if not stack.exists():
                    continue

                send = False
                index = 0

                if stack[index].default_volume < 1:
                    break

                while not send:
                    if index == len(stack):
                        # StackMessages.objects.filter(trigger=trigger).update(current_volume=0)
                        for x in stack:
                            x.current_volume = 0
                            x.save()
                        index = 0

                    # index = index % len(stack)
                    if stack[index].current_volume < stack[index].default_volume:
                        send = True
                        self.send_message(user.user_id, stack[index].url)
                        stack[index].current_volume += 1
                        stack[index].save()

                    index += 1

    def update_tag(self, user_id, set_tag, remove_tag):
        try:
            user = self.BotUsers.objects.get(
                manager=self.manager, user_id=user_id)
            # print('set_tag: ', set_tag)
            # print('remove_tag: ', remove_tag)
            # print("user_tags start: ", [{"id": x['id'], "text": x['text']} for x in user.user_tag.all().values()])
            # print("user_tags start: ", [x['text'] for x in user.user_tag.all().values()])
            for tag in set_tag:
                tag1 = UserTag.objects.get_or_create(
                    manager=self.manager, text=tag.lower())[0]
                user.user_tag.add(tag1)

            for tag in remove_tag:
                tag1 = UserTag.objects.filter(
                    manager=self.manager, text=tag.lower()).first()
                # print("tag1: ", tag1, tag1.text)
                if tag1:
                    user.user_tag.remove(tag1)

            user.save()
            # print("user_tags end: ", [x['text'] for x in user.user_tag.all().values()])
        except:
            print("update_tag error: ", traceback.format_exc())

        # UserTag.object.filter(manager=self.manager, text=set_tag.lower()).delete()

    def send_link(self, chat_id, url, file_path, title, description):
        return True

    def send_contact(self, chat_id, contact_id):
        print('skip send_contact')
        return True

    @staticmethod
    def regex_valid(text, regex_type):
        regex_set = {'text': r".*", 'phone': r"\+*[0-9]{10,11}",
                     'email': r"[\w-]+\@\w+\.\w+", 'digits': r"\d+(\.\d+)*"}
        regular = regex_set.get(regex_type)
        if regular:
            found = re.fullmatch(regular, text.strip())
            if found:
                return found.group()

    def proccess_message(self, message):
        print('--start_message--', message.text)

        try:
            if not self.BotUsers.objects.filter(manager=self.manager, user_id=message.user_id).exists():
                user = self.BotUsers(manager=self.manager,
                                     user_id=message.user_id)
                user.save()
                result = True

                if self.manager.welcome_message:
                    if 'scenario:' in self.manager.welcome_message.lower():
                        scenario = self.find_scenario_by_id(
                            self.manager.welcome_message.lower().replace('scenario:', ''))
                        result = self.trigger_routine(
                            scenario, self.find_trigger(scenario), user)
                    else:
                        scenario = self.find_scenario_by_id(
                            self.manager.welcome_message.lower().replace('scenario:', ''))
                        result = self.trigger_routine(
                            scenario, self.find_trigger(scenario), user)
                        # result = self.send_message(message.user_id, s.trigger_text)
                    if self.manager.send_welcome_notif and self.manager.welcome_notifQ_text and self.manager.welcome_admin_id:
                        if self.manager.get_bot('whatsapp').can_work(self.manager):
                            self.manager.get_bot('whatsapp').send_message(self.manager.welcome_admin_id,
                                                                          self.manager.welcome_notif_text)
                # return result
            else:
                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=message.user_id)

            print(f"message {self.manager}: ", message.user_data)
            if user.user_data != message.user_data:
                try:
                    user.user_data = message.user_data
                    user.save()
                except Exception as e:
                    print(e)

            # print('user mode: ', user.mode, ' ', user.user_id)

            self.correct_delay_msg(user.user_id)

            if message.text.lower() in ['подписка', 'отписка']:
                user.is_unsub = message.text.lower() == 'отписка'
                user.unsub_date = date_today_to_timeshtamp() if user.is_unsub else None
                user.save()

                scenario = self.manager.unsubscribe_message if user.is_unsub else self.manager.subscribe_message

                if scenario:
                    scenario = self.find_scenario_by_id(
                        scenario.lower().replace('scenario:', ''))
                    result = self.trigger_routine(
                        scenario, self.find_trigger(scenario), user)

            if user.mode == "main":
                if 'autoride_' in message.text:
                    autoride_name = str(message.text).replace('autoride_', '')
                    # autorides = AutoRide.objects.filter(manager=self.manager, trigger_text=autoride_name)
                    autorides = AutoRide.objects.filter(
                        manager=self.manager, id=autoride_name)
                    if autorides.exists():
                        autoride = autorides[0]
                        scenario = self.find_scenario_by_id(
                            autoride.scenario_id)
                        result = self.trigger_routine(
                            scenario, self.find_trigger(scenario), user)
                        # result = self.message_routine(autoride.scenario.messages, user)
                        try:
                            if self.manager.application_will_send:
                                notify_message = f"{autoride.trigger_text} - {user.username}"
                                for admin in str(self.manager.application_whatsapp_id).split(','):
                                    if admin:
                                        if self.manager.get_bot('whatsapp').can_work(self.manager):
                                            self.manager.get_bot('whatsapp').send_message(
                                                admin, notify_message)
                                for admin in str(self.manager.application_email).split(','):
                                    if admin:
                                        send_message(
                                            to=admin, message=notify_message, subject=autoride.trigger_text)
                        except:
                            pass
                        return result

                if 'wa_' in message.text:
                    wa_name = str(message.text).replace('wa_', '')
                    main_message = get_last_main_message(self, message.manager, message.user_id,
                                                         int(message.message_number), wa_name)
                    if main_message:
                        scenario = self.find_scenario(
                            main_message.text.replace('wa_', ''))
                        trigger = Trigger.objects.filter(
                            scenario=scenario, caption__iexact=wa_name).first()
                        if trigger:
                            result = self.trigger_routine(
                                scenario, trigger, user)
                        else:

                            trigger = self.find_trigger(scenario)
                            result = self.trigger_routine(
                                scenario, trigger, user)

                        return result

                scenario = self.find_scenario(message.text)
                trigger = self.find_trigger(scenario)

                if 'trigger_text:' in message.text:
                    trigger_id = str(message.text).replace('trigger_text:', '')
                    trigger = Trigger.objects.filter(
                        id=int(trigger_id)).first()
                    scenario = trigger.scenario

                # print('st: ', scenario, trigger)
                if scenario and trigger:
                    # result = self.trigger_routine(scenario, trigger, user)
                    # scenario = self.find_scenario_by_id(scenario.id)
                    # result = self.trigger_routine(scenario, self.find_trigger(scenario), user)
                    result = self.trigger_routine(scenario, trigger, user)
                    return result
                elif self.manager.default_response:
                    if 'scenario:' in self.manager.default_response.lower():
                        scenario = self.find_scenario_by_id(
                            self.manager.default_response.lower().replace('scenario:', ''))
                        result = self.trigger_routine(
                            scenario, self.find_trigger(scenario), user)
                    else:
                        # print(self.manager.default_response)
                        scenario = self.find_scenario_by_id(
                            self.manager.default_response)
                        result = self.trigger_routine(
                            scenario, self.find_trigger(scenario), user)

                        # text = self.manager.default_response
                        # result = self.send_message(message.user_id, text)
                else:
                    return result

            elif user.mode.startswith("application:"):
                field_index = int(user.mode.split(":")[1])
                if message.text == 'break':
                    user.mode = "main"
                    user.save()
                    return

                data = json.loads(user.data)
                if not self.regex_valid(message.text, data['types'][field_index]):
                    result = self.send_message(
                        message.user_id, data["fields"][field_index])
                    return result
                data["values"].append(message.text)
                user.data = json.dumps(data)

                if len(data["values"]) < len(data["fields"]):
                    user.mode = f"application:{field_index + 1}"
                    result = self.send_message(
                        message.user_id, data["fields"][field_index + 1])
                else:
                    reqdata = dict(zip(data["fields"], data["values"]))
                    if self.manager.bitrix_key and self.manager.bitrix_domain:
                        requests.post(f"https://{self.manager.bitrix_domain}.bitrix24.ru/rest/crm.lead.add",
                                      params={"auth": self.manager.bitrix_key}, data={
                                "fields": reqdata,
                                "params": {
                                    "REGISTER_SONET_EVENT": "Y"
                                }
                            })

                    if self.manager.id == 1:
                        self.send_message(message.user_id, str(reqdata))

                    try:
                        if self.manager.amocrm_domain:

                            state = {}
                            print('self.manager.amocrm_domain: ',
                                  self.manager.amocrm_domain)
                            amo_url = f"https://{self.manager.amocrm_domain}.amocrm.ru"

                            headers = {'user-agent': 'amoCRM-API-client/1.0',
                                       'content-type': 'application/json'}

                            auth_request = requests.post(f"{amo_url}/private/api/auth.php",
                                                         data={
                                                             "USER_LOGIN": "tohamit626@mailezee.com",
                                                             "USER_HASH": "f10c61561299b40a0bf2595ba2a613d53837723e"
                                                         })
                            print('status code', auth_request)
                            # print('status code', auth_request.status_code)

                            if auth_request.status_code == 200:
                                print('auth_request.status_code: ',
                                      auth_request.status_code)
                                state['cookies'] = auth_request.cookies

                            if state.get('cookies'):
                                res = requests.post(amo_url + '/api/v2/contacts',
                                                    headers=headers,
                                                    data={"add": [
                                                        {
                                                            "name": "Александр Крылов",
                                                            "custom_fields":
                                                                [{
                                                                    "id": "1",
                                                                    "name": "Телефон",
                                                                    "values":
                                                                        [{
                                                                            "value": "12345678",
                                                                            "enum": "WORK"
                                                                        }]
                                                                }]
                                                        }
                                                    ]},
                                                    cookies=state['cookies']).json()
                                # print("status: ", resode)
                                print("amo res: ", res)
                                print("state['cookies']: ", state['cookies'])

                                res1 = requests.post(amo_url + '/api/v2/leads',
                                                     data={"add": json.dumps([{
                                                         "name": "Покупка карандашей",
                                                         "created_at": "1508101200",
                                                         "updated_at": "1508274000",
                                                         "status_id": "13670637",
                                                         "responsible_user_id": "957083",
                                                         "sale": "5000",
                                                         "tags": "pencil, buy",
                                                         "contacts_id": [
                                                             "1099149"
                                                         ],
                                                         "company_id": "1099148",
                                                         "custom_fields": [{
                                                             "id": "4399649",
                                                             "values": [
                                                                 "3691615",
                                                                 "3691616",
                                                                 "3691617"
                                                             ]
                                                         },
                                                             {
                                                                 "id": "4399656",
                                                                 "values": [{
                                                                     "value": "2017-10-26"
                                                                 }]
                                                             },
                                                             {
                                                                 "id": "4399655",
                                                                 "values": [{
                                                                     "value": "ул. Охотный ряд, 1",
                                                                     "subtype": "address_line_1"
                                                                 },
                                                                     {
                                                                         "value": "Москва",
                                                                         "subtype": "city"
                                                                     },
                                                                     {
                                                                         "value": "101010",
                                                                         "subtype": "zip"
                                                                     },
                                                                     {
                                                                         "value": "RU",
                                                                         "subtype": "country"
                                                                     }
                                                                 ]
                                                             }
                                                         ]
                                                     }])},
                                                     cookies=state['cookies'])
                                print("amo res1: ", res1)
                                temp = {"add": json.dumps([{
                                    "name": "Покупка карандашей",
                                    "created_at": "1508101200",
                                    "updated_at": "1508274000",
                                    "status_id": "13670637",
                                    "responsible_user_id": "957083",
                                    "sale": "5000",
                                    "tags": "pencil, buy",
                                    "contacts_id": [
                                        "1099149"
                                    ],
                                    "company_id": "1099148",
                                    "custom_fields": [{
                                        "id": "4399649",
                                        "values": [
                                            "3691615",
                                            "3691616",
                                            "3691617"
                                        ]
                                    },
                                        {
                                            "id": "4399656",
                                            "values": [{
                                                "value": "2017-10-26"
                                            }]
                                        },
                                        {
                                            "id": "4399655",
                                            "values": [{
                                                "value": "ул. Охотный ряд, 1",
                                                "subtype": "address_line_1"
                                            },
                                                {
                                                    "value": "Москва",
                                                    "subtype": "city"
                                                },
                                                {
                                                    "value": "101010",
                                                    "subtype": "zip"
                                                },
                                                {
                                                    "value": "RU",
                                                    "subtype": "country"
                                                }
                                            ]
                                        }
                                    ]
                                }])}
                                # print(temp)
                            # res1 = requests.post(f"{amo_url}/api/v2/leads", data={
                            #     "add": json.dumps([reqdata])
                            # }).json()

                    except Exception as e:
                        print("amo err1: ", traceback.format_exc())
                    try:
                        url = 'http://188.225.57.229/notify'

                        notify_subject = f"[UserID: {user.id} application]"
                        notify_message = f"{self.manager.id}({self.bot_place}): {message.user_id}."
                        form = ""
                        for field, value in reqdata.items():
                            notify_message = f"{notify_message} {field}: {value}."
                            form = f"{form} {field}: {value}. \n"

                        for admin in str(self.manager.application_whatsapp_id).split(','):
                            if admin:
                                if self.manager.get_bot('whatsapp').can_work(self.manager):
                                    self.manager.get_bot('whatsapp').send_message(
                                        admin, notify_message)

                                answer = requests.post(url, json={'whatsapp_id': admin,
                                                                  'form': form,
                                                                  'sender_info': message.sender_name,
                                                                  'messenger': self.bot_place})
                                # print('seccess! ', answer)

                        for admin in str(self.manager.application_email).split(','):
                            if admin:
                                send_message(
                                    to=admin, message=notify_message, subject=notify_subject)
                                # print('admin:', admin, ' - ', notify_message, ' - ', notify_subject)
                                # answer = requests.post(url, json={'email': admin,
                                #                                   'form': form,
                                #                                   'sender_info': message.sender_name,
                                #                                   'messenger': self.bot_place})
                                # print('seccess0! ', answer)

                        for admin in str(self.manager.application_telegram_id).split(','):
                            try:
                                if admin:
                                    bot = telebot.TeleBot('990694770:AAHnFf2J0cjNOj7YTV1DxaEiLPCAPgivOvY',
                                                          threaded=False)
                                    self.bot.send_message(
                                        admin, notify_message)
                                    # print('admin:', admin)
                                    # answer = requests.post(url, json={'telegram_id': admin,
                                    #                                   'form': form,
                                    #                                   'sender_info': message.sender_name,
                                    #                                   'messenger': self.bot_place})
                                    # print('seccess1! ', answer)
                            except Exception as e:
                                print("send tg info: ", e)

                        form = FormLogging(manager=self.manager, chat_id=message.chat_id, source=self.bot_place,
                                           form=str(form))
                        form.save()
                    except Exception as e:
                        print('err: ', e)
                    user.data = "{}"
                    user.mode = f"main"
                    result = self.send_message(message.user_id, "Готово!")
                user.save()
                return result

            elif user.mode.startswith("payment:"):
                rows = user.mode.split(':')
                # print('payment: ', rows)

                user.mode = "main"
                user.save()
                try:
                    error_text = rows[3]
                    pay = rows[4]
                    name = rows[5]

                    from google.cloud import vision
                    from google.cloud.vision import types

                    # Instantiates a client
                    from GUI import doctext

                    text = ''
                    # print("path: ", message.text)
                    if self.bot_place == 'telegram':  # , 'facebook'):
                        text = doctext.parse_image_to_text(message.text, False)
                    else:
                        text = doctext.parse_image_to_text(message.text, True)
                    if str(pay) in text and name in text:
                        # print("foto seccess")
                        trigger = self.find_trigger_by_id(int(rows[2]))
                        result = self.trigger_routine(
                            trigger.scenario, trigger, user)
                    else:
                        # print("foto error")
                        self.bot.send_message(message.chat_id, error_text)

                    # print("foto end")
                except Exception as e:
                    # print("err foto: ", message.text, ' - ', e)
                    self.bot.send_message(message.chat_id, "ошибка обработки")

            else:
                return True
        except Exception as e:
            # print('err: ', message.text, ' ', e)
            return True

    def save_log(self, chat_id, text, type):
        try:
            if text:
                self.BotReceivedMsg(text=text,
                                    chat_id=chat_id,
                                    message_type=type,
                                    manager=self.manager,
                                    proccessed=False,
                                    is_bot=True,
                                    time=datetime.now().timestamp()).save()
        except Exception as err:
            print(traceback.format_exc())


class TelegramBot(Bot):
    threads = {}
    BotDelayMsg = TelegramDelayMsg
    BotUsers = TelegramUsers
    BotReceivedMsg = TelegramReceivedMsg
    bot_place = "telegram"

    @staticmethod
    def can_work(manager):
        if manager.telegram_token:
            try:
                telebot.TeleBot(manager.telegram_token).get_me()
                return True
            except Exception as e:
                bots_logger.debug(f'{manager} telegram can_work: ' + str(e))
                return False
        return False

    def __init__(self, manager):
        self.manager = manager
        self.token = manager.telegram_token
        try:
            self.bot = telebot.TeleBot(self.token, threaded=False)
            self.bot_username = self.bot.get_me().username
            # apihelper.proxy = {'http', 'socks5h://MzFETX:5JHL0S@194.32.251.42:8000'}
        except Exception as e:
            bots_logger.debug('telegram init: ' + str(traceback.format_exc()))
            self.bot, self.bot_username = None, "None"
        self.manager.telegram_name = self.bot_username

    def start(self):
        if self.manager.telegram or self.is_any_work():
            self.stop()
        # return
        self.manager.telegram = True
        self.manager.save()
        self.bot.set_webhook(
            url="https://api.chatlead.io/app/telegram_webhook/" + self.token)

        # self.bot.add_message_handler(self.bot._build_handler_dict(
        #     self.proccess_messages, content_types=["text", "pinned_message", "photo"], commands=None,
        #     func=lambda msg: msg.chat.id == msg.from_user.id, regexp=None
        # ))
        # self.bot.add_callback_query_handler(self.bot._build_handler_dict(
        #     self.proccess_callbacks, func=lambda msg: msg.message.chat.id == msg.from_user.id
        # ))
        #
        self.run_threads([{"name": "polling", "target": self.except_safe_polling}])

    def except_safe_polling(self):
        return
        while self.manager.telegram:
            try:
                self.bot.polling()
            except:
                pass

    def stop(self):
        self.manager.telegram = False
        self.manager.save()
        self.bot.delete_webhook()
        self.wait_stopping()

    def polling(self):
        return
        while self.manager.telegram:
            try:
                now = datetime.today()
                # bots_logger.debug(f"start TG polling manager_id: {self.manager.id}, token: {self.token}")
                self.bot.polling(interval=1, timeout=10)
                # print(datetime.today() - now)
                break
            except Exception as e:
                print(traceback.format_exc())

    def text_format(self, text, message):
        return text.replace(
            "{name}", message.sender_name
        ).replace(
            "{username}", f"@{message.sender_username}"
        )

    def get_hide_link(self, text):
        return f"https://t.me/{self.bot_username}?start={text}"

    def send_message(self, chat_id, text, keyboard=None, *args, **kwargs):
        try:
            if not keyboard:
                keyboard = {}
            text_buttons = keyboard.get('text_buttons', [])
            url_buttons = keyboard.get('url_buttons', [])
            fast_buttons = keyboard.get('fast_buttons', [])
            kwargs = {}

            fast_buttons_trigger = []
            fast_buttons_url = []
            fast_buttons_call = []
            for x in keyboard.get('fast_buttons', []):
                type = x.get('secondType')
                if type == 'trigger_buttons':
                    fast_buttons_trigger.append(x)
                elif type == 'url_buttons':
                    fast_buttons_url.append(x)
                elif type == 'call_buttons':
                    fast_buttons_call.append(x)

            if text_buttons:
                keyboard = telebot.types.ReplyKeyboardMarkup(
                    resize_keyboard=True, one_time_keyboard=True)
                keyboard.add(
                    *[telebot.types.KeyboardButton(text=button.get('caption')) for button in text_buttons])

                kwargs['reply_markup'] = keyboard
            elif fast_buttons or url_buttons:
                def trigger_callback(trigger_id):
                    return 'trigger_{}'.format(trigger_id)

                try:
                    keyboard = telebot.types.InlineKeyboardMarkup()
                    keyboard.add(
                        *[telebot.types.InlineKeyboardButton(text=button.get('caption'),
                                                             url=button.get('payload').get('url') if button.get(
                                                                 'payload') else None)
                          for button in url_buttons],
                        *[telebot.types.InlineKeyboardButton(text=button.get('caption'),
                                                             callback_data=trigger_callback(
                                                                 button.get('payload').get('trigger_id') if button.get(
                                                                     'payload') else None))
                          for button in fast_buttons_trigger],
                        *[telebot.types.InlineKeyboardButton(text=button.get('caption'),
                                                             url=button.get('payload').get('url') if button.get(
                                                                 'payload') else None)
                          for button in fast_buttons_url])

                    kwargs['reply_markup'] = keyboard

                except Exception as e:
                    print('err bt: ', e)

            print('send tg: ', text)

            self.bot.send_message(chat_id, text, *args, **kwargs)
            print('send tg seccess: ', text)
            self.save_log(chat_id, text, 'text')
            self.save_log(chat_id, text_buttons, 'buttons')
            self.save_log(chat_id, url_buttons, 'buttons')
            self.save_log(chat_id, fast_buttons, 'buttons')

            return True
        except Exception:
            print(traceback.format_exc())
            return False

    def get_attach(self, filename, url):
        if url:
            return self.get_attach_url(url)
        elif filename:
            return self.get_attach_file(filename)

    @staticmethod
    def get_attach_url(url):
        if url:
            # print('url11111:', url)
            from urllib.request import urlopen, Request
            # headers = {"User - Agent": 'Mozilla / 5.0(WindowsNT6.1) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 41.0.2228.Safari / 537.3'}
            headers = {'User-Agent': 'Mozilla/5.0'}
            req = Request(url=url, headers=headers)
            return urlopen(req)
            # return urlopen(url)
            # return urlopen(req).read()

    @staticmethod
    def get_attach_file(filename):
        if filename and os.path.exists(filename):
            with open(filename, 'rb') as file:
                return file

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        # print('-------------------------1', filename, url, kwargs)
        attach = self.get_attach(filename, url)
        caption = None
        if 'caption' in kwargs:
            caption = kwargs['caption']
        if attach:
            self.bot.send_photo(chat_id, attach, caption=caption)

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        attach = self.get_attach(filename, url)
        caption = None
        if 'caption' in kwargs:
            caption = kwargs['caption']
        if attach:
            self.bot.send_audio(chat_id, attach, caption=caption)

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        attach = self.get_attach(filename, url)
        caption = None
        if 'caption' in kwargs:
            caption = kwargs['caption']
        if attach:
            self.bot.send_video(chat_id, attach, caption=caption)

    def set_typing(self, chat_id, delay):
        self.bot.send_chat_action(chat_id, 'typing')

    def send_file(self, chat_id, filename=None, url=None, **kwargs):
        attach = self.get_attach(filename, url)
        caption = None
        if 'caption' in kwargs:
            caption = kwargs['caption']
        if attach:
            self.bot.send_document(chat_id, attach, caption=caption)

    def send_document(self, *args, **kwargs):
        self.bot.send_document(*args, **kwargs)

    def get_dialogs_count(self):
        return len(set(msg.chat_id for msg in TelegramReceivedMsg.objects.filter(manager=self.manager)))

    def proccess_callbacks(self, callback):
        try:
            if callback.get("data").startswith('trigger_'):
                trigger_id = int(callback.get("data").split('_')[1])

                trigger = self.find_trigger_by_id(trigger_id)
                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=callback.get("from").get("id"))
                result = self.trigger_routine(trigger.scenario, trigger, user)
                if result is True:
                    self.bot.answer_callback_query(callback.get("id"))
                    try:
                        self.bot.delete_message(callback.get("from").get("id"),
                                                callback.get("message").get("message_id"))
                    except:
                        pass
        except Exception:
            print(traceback.format_exc())

    # def proccess_callbacks(self, callback: CallbackQuery):
    #     try:
    #         if callback.data.startswith('trigger_'):
    #             trigger_id = int(callback.data.split('_')[1])
    #
    #             trigger = self.find_trigger_by_id(trigger_id)
    #             user = self.BotUsers.objects.get(manager=self.manager, user_id=callback.from_user.id)
    #             result = self.trigger_routine(trigger.scenario, trigger, user)
    #             if result is True:
    #                 self.bot.answer_callback_query(callback.id)
    #                 try:
    #                     self.bot.delete_message(callback.from_user.id, callback.message.message_id)
    #                 except:
    #                     pass
    #     except Exception:
    #         print(traceback.format_exc())

    # def proccess_messages(self, message):
    #     try:
    #         print('message tg: ', message)
    #         print('message.content_type tg: ', message.content_type)
    #
    #         text = message.text
    #
    #         if message.content_type == "photo":
    #             file_info = self.bot.get_file(message.photo[-1].file_id)
    #             downloaded_file = self.bot.download_file(file_info.file_path)
    #             text = "/home/admin/chatlead/uploads/" + file_info.file_path
    #
    #             with open(text, 'wb') as new_file:
    #                 new_file.write(downloaded_file)
    #
    #         if message.content_type in ["text", "photo"]:
    #             msg = TelegramReceivedMsg(
    #                 manager=self.manager,
    #                 text=text,
    #                 chat_id=message.chat.id,
    #                 user_id=message.from_user.id,
    #                 sender_username=message.from_user.username,
    #                 sender_name=message.from_user.first_name,
    #                 chat_title=message.chat.title,
    #                 time=message.date,
    #                 message_id=message.message_id,
    #                 user_data=json.dumps(
    #                     {'username': message.from_user.username, "first_name": message.from_user.first_name})
    #             )
    #             if msg.text.startswith("/start ") and msg.text != "/start":
    #                 msg.text = msg.text[7:]
    #             result = self.proccess_message(msg)
    #             msg.proccessed = result
    #             msg.save()
    #
    #     except Exception:
    #         print(traceback.format_exc())

    def proccess_messages(self, message, content_type='text'):
        try:
            print('message tg: ', message)
            # print('message.content_type tg: ', message.content_type)

            text = message.get("text") if message.get("text") else ''

            if content_type == "photo":
                file_info = self.bot.get_file(
                    message.get("photo")[-1].get("file_id"))
                downloaded_file = self.bot.download_file(file_info.file_path)
                text = UPLOAD_PATH + '/' + file_info.file_path

                with open(text, 'wb') as new_file:
                    new_file.write(downloaded_file)

            if content_type in ["text", "photo"]:
                msg = TelegramReceivedMsg(
                    manager=self.manager,
                    text=text,
                    chat_id=message.get("chat")["id"],
                    user_id=message.get("from")["id"],
                    sender_username=message.get("from").get(
                        "username") if message.get("from").get("username") else '',
                    sender_name=message.get("from").get("first_name") if message.get(
                        "from").get("first_name") else '',
                    chat_title='',
                    time=message.get("date"),
                    message_id=message.get("message_id"),
                    user_data=json.dumps({
                        'username': message.get("from").get("username", ''),
                        'first_name': message.get("from").get("first_name", ''),
                        'last_name': message.get("from").get("last_name", '')
                    })
                )
                if msg.text.startswith("/start ") and msg.text != "/start":
                    msg.text = msg.text[7:]

                msg.save()
                result = self.proccess_message(msg)
                msg.proccessed = result
                msg.save()

        except Exception:
            print(traceback.format_exc())

    def get_user_icon(self, link):
        user = self.BotUsers.objects.filter(link=link).first()
        photos = self.bot.get_user_profile_photos(user.user_id)
        if photos.total_count == 0:
            return user

        file_id = photos.photos[0][-1]
        print("file_id: ", file_id)
        print("id: ", user.id)
        file_info = self.bot.get_file(file_id.file_id)
        downloaded_file = self.bot.download_file(file_info.file_path)

        path = user_directory_path(
            user, file_info.file_path.split('/')[-1])  # file_info.file_path
        print("file_path: ", file_info.file_path)
        print("path: ", path)

        with open(path, 'wb') as new_file:
            new_file.write(downloaded_file)

        user.icon_url = path
        # print("iscon: ", user.icon.url)
        print("iscon: ", user.icon_url)
        user.save()
        print("tst: ", user.icon.url)
        return user

    def check_tag1(self, tags, user_id, user=None):
        try:
            user_tags = UserTag.objects.filter(
                manager=self.manager, text__in=tags)

            if not user:
                user = BotUsers.objects.filter(user_id).first()

            if len(tags) == 1 and tags[0] == '' or user_tags.filter(user_tags_tg=user).exists():
                return True
            return False
        except:
            print(traceback.format_exc())
            return True


class VkBot_callback(Bot):
    threads = {}
    BotDelayMsg = VkDelayMsg
    BotUsers = VkUsers
    BotReceivedMsg = VkReceivedMsg
    bot_place = "vk"

    @staticmethod
    def can_work(manager):
        return manager.vk_group_access_token and manager.vk_group_id

    def __init__(self, manager):
        self.manager = manager
        self.token = self.manager.vk_group_access_token
        self.group_id = self.manager.vk_group_id
        self.secret_code = self.manager.vk_secret_code
        self.confirm_code = self.manager.vk_confirmation_code

        print(f"Active {manager.id}")

        if self.secret_code == "" or "." in self.secret_code or "-" in self.secret_code:
            manager.vk_secret_code = f"{PROJECT_NAME}{int(time.time())}"
            self.secret_code = manager.vk_secret_code
            manager.save()

        super().__init__(manager)

    def configure_call_back_server(self):
        exist, count = self.is_call_back_server_exists(
            self.manager.id, self.group_id
        )
        if not exist and count:
            self.make_call_back_server(
                manager_id=self.manager.id,
                vk_group_id=self.group_id,
                url=f"{API_URL}/app/vk_api_call_back/{self.manager.id}",
                secret_key=self.secret_code
            )

    def make_call_back_server(self, manager_id=0, vk_group_id=0, url="", secret_key="", title=PROJECT_NAME):
        self.confirm_code = self.return_confirm_code(vk_group_id)
        self.manager.vk_confirmation_code = self.confirm_code
        self.manager.save()

        server_id = self.request(
            "groups",
            "addCallbackServer",
            params={
                "group_id": vk_group_id,
                "url": url,
                "title": title,
                "secret_key": secret_key,
            }
        )["response"]["server_id"]
        print(server_id)
        self.request(
            "groups",
            "setCallbackSettings",
            params={
                "group_id": vk_group_id,
                "server_id": server_id,
                "api_version": "5.110",
                "message_new": 1,
                "group_leave": 1,
                "group_join": 1,
            }
        )
        return True

    def unit_make_call_back_server(self, manager_id=0, vk_group_id=0, url="", secret_key=0, title=PROJECT_NAME):
        manager_id = self.manager.id
        vk_group_id = self.group_id
        url = f"{API_URL}/app/vk_api_call_back/{self.manager.id}"
        secret_key = self.secret_code
        server_id = self.request(
            "groups",
            "addCallbackServer",
            params={
                "group_id": vk_group_id,
                "url": url,
                "title": title,
                "secret_key": secret_key,
            }
        )["response"]["server_id"]

        return json.dumps(self.request(
            "groups",
            "setCallbackSettings",
            params={
                "group_id": vk_group_id,
                "server_id": server_id,
                "api_version": "5.102",
                "message_new": 1,
                "group_leave": 1,
                "group_join": 1,
            }
        ))

    def is_call_back_server_exists(self, manager_id=0, vk_group_id=0):
        if manager_id == 0 or vk_group_id == 0:
            return False, False

        servers = self.request(
            "groups",
            "getCallbackServers",
            params={
                "group_id": vk_group_id
            }
        )

        try:
            servers_count = int(servers["response"]["count"]) < 10
        except Exception:
            return False, 0

        for server in servers["response"]["items"]:
            bots_logger.debug(
                f'VK callback, manager_id: {self.manager.id}, group_id: {self.group_id}, url: {server["url"]}'
            )
            if f"{API_URL}/app/vk_api_call_back/{manager_id}" == server.get("url", ""):
                return True, servers_count
        return False, servers_count

    def confirm_handler(self):
        return self.confirm_code

    def return_confirm_code(self, group_id):
        return self.request(
            "groups",
            "getCallbackConfirmationCode",
        )["response"]["code"]

    def stop(self):
        self.manager.refresh_from_db()
        self.manager.vk = False
        self.manager.save()
        self.wait_stopping()

    def start(self):
        if self.manager.vk or self.is_any_work():
            self.stop()

        self.token = self.manager.vk_group_access_token
        self.group_id = self.manager.vk_group_id
        self.secret_code = self.manager.vk_secret_code
        self.confirm_code = self.manager.vk_confirmation_code

        if self.secret_code == "" or "." in self.secret_code or "-" in self.secret_code:
            manager.vk_secret_code = f"{PROJECT_NAME}{time.time()}"
            self.secret_code = manager.vk_secret_code
            manager.save()

        try:
            self.configure_call_back_server()
        except:
            self.manager.refresh_from_db()
            self.manager.vk = False
            self.manager.save()
            return

        self.manager.refresh_from_db()
        self.manager.vk = True
        self.manager.save()

        bots_logger.debug(
            f'start VK callback, manager_id: {self.manager.id}, group_id: {self.group_id}')
        # self.run_threads([{"name": "handler", "target": self.message_handler}])

    def request(self, section, method, params=None, v="5.110"):
        if params is None:
            params = {}
        params.update({
            "access_token": self.token,
            "group_id": self.group_id,
            "v": v,
        })
        response = requests.post(
            f"https://api.vk.com/method/{section}.{method}", params).json()
        return response

    def send_message(self, user_id, text, *args, keyboard=None, **kwargs):
        if not keyboard:
            keyboard = {}
        text_buttons = keyboard.get('text_buttons', [])
        url_buttons = keyboard.get('url_buttons', [])
        fast_buttons = keyboard.get('fast_buttons', [])
        if text_buttons:
            buttons = [[{
                "color": "secondary",
                "action": {
                    "type": "text",
                    "label": button.get('caption'),
                    "payload": {
                        'trigger_id': button.get("payload").get("trigger_id") if button.get("payload") else None},
                }
            } for button in text_buttons]]
        elif fast_buttons:
            buttons = [[{
                "color": "secondary",
                "action": {
                    "type": "text",
                    "label": button.get('caption'),
                    "payload": {
                        'trigger_id': button.get("payload").get("trigger_id") if button.get("payload") else None},
                }
            } for button in fast_buttons]]
        else:
            buttons = []

        resp = self.request("messages", "send", params={
            "user_id": user_id,
            "message": text,

            "keyboard": json.dumps({
                "one_time": True,
                "buttons": buttons
            }),
            "random_id": "",
        })

        self.save_log(user_id, text, 'text')
        self.save_log(user_id, text_buttons, 'buttons')
        self.save_log(user_id, url_buttons, 'buttons')
        self.save_log(user_id, fast_buttons, 'buttons')
        # bots_logger.debug(f'vk send message: {resp}')

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_file(self, chat_id, filename=None, url=None, ):
        upload_url = self.request(
            "docs",
            "getMessagesUploadServer",
            params={
                "peer_id": chat_id
            }
        )["response"]["upload_url"]
        try:
            if url is not None:
                path = self.get_path_from_url(url)
                with open(path, 'rb') as file:
                    file_string = requests.post(
                        upload_url, files={"file": file}).content.decode('utf-8')
                file_string = json.loads(file_string).get("file")
                res = self.request(
                    "docs",
                    "save",
                    params={
                        "file": file_string,
                        "title": url.split('/')[-1], "tags": ""
                    })['response']['doc']
                self.request("messages", "send", params={
                    "user_id": chat_id,
                    "attachment": f'doc{res["owner_id"]}_{res["id"]}',
                    "random_id": self.get_random_id()
                })
        except:
            print(traceback.format_exc())

    def get_random_id(self):
        return random.getrandbits(31) * random.choice([-1, 1])

    def get_dialogs_count(self):
        return len(set(msg.user_id for msg in VkReceivedMsg.objects.filter(manager=self.manager)))

    def message_handler(self, data: dict):
        print(f"Message handled {self.manager.id}")
        try:
            if (
                    (
                            data["type"] is "message_new"
                            and (
                                    len(self.manager.welcome_message) > 0
                                    or len(self.manager.default_response) > 0
                            )
                    )
                    or (
                    data["type"] is "group_leave"
                    and len(self.manager.unsubscribe_message) > 0
            )
                    or (
                    data["type"] is "group_join"
                    and len(self.manager.subscribe_message) > 0
            )
            ):
                # code, message_id, flags, peer_id, timestamp, text, ref, *args = update

                peer_id = data.get("user_id", -1)

                #  Group leave, join
                if peer_id != -1:
                    peer_id = int(data.get("user_id", -1))


                #  Other   
                else:
                    data = data["object"]

                message_id = int(data.get("id", -1))

                peer_id = int(data.get("peer_id", -1))

                # if peer_id == -1 and message_id == -1:
                #     peer_id = int(data.get("user_id", -1))

                timestamp = int(data.get("date", -1))

                text = str(data.get("text", ""))

                ref = data.get("ref", "")

                if type(ref) == dict and ref.get('ref_source'):
                    text = ref.get('ref_source')

                resp = self.request(
                    "users",
                    "get",
                    params={
                        "user_ids": peer_id,
                        "fields": 'contacts,domain,has_photo,photo_200_orig'
                    }
                )['response'][0]
                print('vk message_handler:', resp)
                user_data = {
                    'first_name': resp.get('first_name', ''),
                    'last_name': resp.get('last_name', ''),
                    'username': resp.get('domain', ''),
                    'phone': resp.get('phone', ''),
                    'icon': (
                        resp.get('photo_200_orig', '')
                        if resp.get('has_photo', 0) == 1 else
                        None
                    )
                }

                if 'payload' in ref:
                    self.process_payloads(
                        peer_id, text, json.loads(ref['payload']), user_data)
                    return 220
                msg = VkReceivedMsg(
                    manager=self.manager,
                    message_id=message_id,
                    text=text,
                    user_id=peer_id,
                    chat_id=peer_id,
                    time=timestamp,
                    user_data=json.dumps(user_data)
                )
                result = self.proccess_message(msg)
                return 200
        except Exception:
            print(traceback.format_exc())

    def process_payloads(self, peer_id, text, payload, user_data):
        if payload.get('command') == 'start':
            if not self.BotUsers.objects.filter(manager=self.manager, user_id=peer_id).exists():
                user = self.BotUsers(
                    manager=self.manager, user_id=peer_id, user_data=json.dumps(user_data))
                user.save()
            else:
                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=peer_id)

            if self.manager.welcome_message:
                if 'scenario:' in self.manager.welcome_message.lower():
                    scenario = self.find_scenario_by_id(
                        self.manager.welcome_message.lower().replace('scenario:', ''))
                    self.trigger_routine(
                        scenario, self.find_trigger(scenario), user)
                else:
                    self.send_message(peer_id, self.manager.welcome_message)
                if self.manager.send_welcome_notif and self.manager.welcome_notif_text and self.manager.welcome_admin_id:
                    if self.manager.get_bot('whatsapp').can_work(self.manager):
                        self.manager.get_bot('whatsapp').send_message(self.manager.welcome_admin_id,
                                                                      self.manager.welcome_notif_text)
        elif 'trigger_id' in payload:
            trigger_id = payload['trigger_id']
            trigger = self.find_trigger_by_id(trigger_id)

            user = self.BotUsers.objects.get(
                manager=self.manager, user_id=peer_id)
            self.trigger_routine(trigger.scenario, trigger, user)

    def get_user_icon(self, link):
        user = self.BotUsers.objects.filter(link=link).first()

        resp = self.request("users", "get", params={
            "user_id": user.user_id,
            "fields": 'first_name,last_name,photo_200_orig'
        })["response"]

        user.icon_url = resp[0].get(
            'photo_200_orig') if len(resp) > 0 else None
        user.save()
        return user

    def check_tag1(self, tags, user_id, user=None):
        try:
            user_tags = UserTag.objects.filter(
                manager=self.manager, text__in=tags)

            if not user:
                user = BotUsers.objects.filter(user_id).first()

            if len(tags) == 1 and tags[0] == '' or user_tags.filter(user_tags_vk=user).exists():
                return True
            return False
        except:
            print(traceback.format_exc())
            return True


class VkBot(Bot):
    threads = {}
    BotDelayMsg = VkDelayMsg
    BotUsers = VkUsers
    BotReceivedMsg = VkReceivedMsg
    bot_place = "vk"

    @staticmethod
    def can_work(manager):
        return manager.vk_group_access_token and manager.vk_group_id

    def __init__(self, manager):
        self.manager = manager
        self.token = self.manager.vk_group_access_token
        self.group_id = self.manager.vk_group_id
        self.lp_config = {}
        super().__init__(manager)

    def configure_long_poll_server(self):
        config = self.request("messages", "getLongPollServer", params={
            "need_pts": 1, "lp_version": 3})["response"]
        self.lp_config = config

    def stop(self):
        self.manager.refresh_from_db()
        self.manager.vk = False
        self.manager.save()
        self.wait_stopping()

    def start(self):
        if self.manager.vk or self.is_any_work():
            self.stop()

        self.token = self.manager.vk_group_access_token
        self.group_id = self.manager.vk_group_id

        try:
            self.configure_long_poll_server()
        except:
            self.manager.refresh_from_db()
            self.manager.vk = False
            self.manager.save()
            return

        self.manager.refresh_from_db()
        self.manager.vk = True
        self.manager.save()

        # bots_logger.debug(f'start VK polling, manager_id: {self.manager.id}, group_id: {self.group_id}')
        self.run_threads([{"name": "handler", "target": self.message_handler}])

    @property
    def long_poll_link(self):
        return "https://{server}?act=a_check&key={key}&ts={ts}&wait=5&mode=2&version=3".format(**self.lp_config)

    def get_updates(self):
        response = requests.get(self.long_poll_link).json()
        if response.get("failed", 0) in [1, 2, 3]:
            if response.get("failed", 0) == 1:
                self.lp_config["ts"] = response["ts"]
            else:
                self.configure_long_poll_server()
            response = requests.get(self.long_poll_link).json()
        self.lp_config["ts"] = response["ts"]
        return response["updates"]

    def request(self, section, method, params=None):
        if params is None:
            params = {}
        params.update({
            "access_token": self.token,
            "group_id": self.group_id,
            "v": "5.102",
        })
        response = requests.post(
            f"https://api.vk.com/method/{section}.{method}", params).json()
        return response

    def set_typing(self, chat_id, delay):
        self.request('messages', 'setActivity', params={
            'type': 'typing',
            'user_id': chat_id
        })

    def send_message(self, user_id, text, *args, keyboard=None, **kwargs):
        if not keyboard:
            keyboard = {}
        text_buttons = keyboard.get('text_buttons', [])
        url_buttons = keyboard.get('url_buttons', [])
        fast_buttons = keyboard.get('fast_buttons', [])
        if text_buttons:
            buttons = [[{
                "color": "secondary",
                "action": {
                    "type": "text",
                    "label": button.get('caption'),
                    "payload": {
                        'trigger_id': button.get("payload").get("trigger_id") if button.get("payload") else None},
                }
            } for button in text_buttons]]
        elif fast_buttons:
            buttons = [[{
                "color": "secondary",
                "action": {
                    "type": "text",
                    "label": button.get('caption'),
                    "payload": {
                        'trigger_id': button.get("payload").get("trigger_id") if button.get("payload") else None},
                }
            } for button in fast_buttons]]
        else:
            buttons = []

        resp = self.request("messages", "send", params={
            "user_id": user_id,
            "message": text,

            "keyboard": json.dumps({
                "one_time": True,
                "buttons": buttons
            }),
            "random_id": "",
        })

        self.save_log(user_id, text, 'text')
        self.save_log(user_id, text_buttons, 'buttons')
        self.save_log(user_id, url_buttons, 'buttons')
        self.save_log(user_id, fast_buttons, 'buttons')
        # bots_logger.debug(f'vk send message: {resp}')

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, filename, url)

    def send_file(self, chat_id, filename=None, url=None, ):
        upload_url = self.request("docs", "getMessagesUploadServer", params={"peer_id": chat_id})["response"][
            "upload_url"]
        try:
            if url is not None:
                path = self.get_path_from_url(url)
                with open(path, 'rb') as file:
                    file_string = requests.post(
                        upload_url, files={"file": file}).content.decode('utf-8')
                file_string = json.loads(file_string).get("file")
                res = self.request("docs", "save",
                                   params={"file": file_string, "title": url.split('/')[-1], "tags": ""})['response'][
                    'doc']
                self.request("messages", "send", params={
                    "user_id": chat_id,
                    "attachment": f'doc{res["owner_id"]}_{res["id"]}',
                    "random_id": ""
                })
        except:
            print(traceback.format_exc())

    def get_dialogs_count(self):
        return len(set(msg.user_id for msg in VkReceivedMsg.objects.filter(manager=self.manager)))

    def message_handler(self):
        while self.manager.vk:
            try:
                if not self.manager.vk:
                    break
                for update in self.get_updates():
                    try:
                        if not self.manager.vk:
                            break
                        code, *args = update
                        if code != 4:
                            continue
                        code, message_id, flags, peer_id, timestamp, text, ref, *args = update
                        if flags not in [1, 17]:
                            continue
                        if type(ref) == dict and ref.get('ref_source'):
                            text = ref.get('ref_source')

                        resp = self.request("users", "get",
                                            params={
                                                "user_ids": peer_id,
                                                "fields": 'contacts,domain,has_photo,photo_200_orig'
                                            }
                                            )['response'][0]
                        print('vk message_handler:', resp)
                        user_data = {
                            'first_name': resp.get('first_name', ''),
                            'last_name': resp.get('last_name', ''),
                            'username': resp.get('domain', ''),
                            'phone': resp.get('phone', ''),
                            'icon': (
                                resp.get('photo_200_orig', '')
                                if resp.get('has_photo', 0) == 1 else
                                None
                            )
                        }

                        if 'payload' in ref:
                            self.process_payloads(
                                peer_id, text, json.loads(ref['payload']), user_data)
                            continue
                        msg = VkReceivedMsg(
                            manager=self.manager,
                            message_id=message_id,
                            text=text,
                            user_id=peer_id,
                            chat_id=peer_id,
                            time=timestamp,
                            user_data=json.dumps(user_data)
                        )
                        result = self.proccess_message(msg)
                    except Exception:
                        print(traceback.format_exc())
            # time.sleep(1)
            except Exception:
                print(traceback.format_exc())

    def process_payloads(self, peer_id, text, payload, user_data):
        if payload.get('command') == 'start':
            if not self.BotUsers.objects.filter(manager=self.manager, user_id=peer_id).exists():
                user = self.BotUsers(
                    manager=self.manager, user_id=peer_id, user_data=json.dumps(user_data))
                user.save()
            else:
                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=peer_id)

            if self.manager.welcome_message:
                if 'scenario:' in self.manager.welcome_message.lower():
                    scenario = self.find_scenario_by_id(
                        self.manager.welcome_message.lower().replace('scenario:', ''))
                    self.trigger_routine(
                        scenario, self.find_trigger(scenario), user)
                else:
                    self.send_message(peer_id, self.manager.welcome_message)
                if self.manager.send_welcome_notif and self.manager.welcome_notif_text and self.manager.welcome_admin_id:
                    if self.manager.get_bot('whatsapp').can_work(self.manager):
                        self.manager.get_bot('whatsapp').send_message(self.manager.welcome_admin_id,
                                                                      self.manager.welcome_notif_text)
        elif 'trigger_id' in payload:
            trigger_id = payload['trigger_id']
            trigger = self.find_trigger_by_id(trigger_id)

            user = self.BotUsers.objects.get(
                manager=self.manager, user_id=peer_id)
            self.trigger_routine(trigger.scenario, trigger, user)

    def get_user_icon(self, link):
        user = self.BotUsers.objects.filter(link=link).first()

        resp = self.request("users", "get", params={
            "user_id": user.user_id,
            "fields": 'first_name,last_name,photo_200_orig'
        })["response"]

        user.icon_url = resp[0].get(
            'photo_200_orig') if len(resp) > 0 else None
        user.save()
        return user

    def check_tag1(self, tags, user_id, user=None):
        try:
            user_tags = UserTag.objects.filter(
                manager=self.manager, text__in=tags)

            if not user:
                user = BotUsers.objects.filter(user_id).first()

            if len(tags) == 1 and tags[0] == '' or user_tags.filter(user_tags_vk=user).exists():
                return True
            return False
        except:
            print(traceback.format_exc())
            return True


def path_from_url(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if 'url' in kwargs:
            kwargs['filename'] = Bot.get_path_from_url(kwargs['url'])
        return func(*args, **kwargs)

    return wrapper


class FacebookBot(Bot):
    threads = {}
    BotDelayMsg = FacebookDelayMsg
    BotUsers = FacebookUsers
    BotReceivedMsg = FacebookReceivedMsg
    bot_place = "facebook"

    @staticmethod
    def can_work(manager):
        return manager.facebook_token and manager.facebook_group_id

    def __init__(self, manager):
        self.manager = manager
        self.token = manager.facebook_token
        self.group_id = manager.facebook_group_id
        self.bot = FacebookClient(self.token)
        super().__init__(manager)

    def stop(self):
        self.manager.refresh_from_db()
        self.manager.facebook = False
        self.manager.save()
        self.wait_stopping()

    def start(self):
        self.manager.refresh_from_db()
        if self.manager.facebook:
            self.stop()

        self.token = self.manager.facebook_token
        self.group_id = self.manager.facebook_group_id
        self.manager.facebook = True
        self.manager.save()
        # bots_logger.debug(f'start FB webhook manager_id: {self.manager.id}, group_id: {self.group_id}')
        self.run_threads()

    def set_typing(self, chat_id, delay):
        self.bot.send_action(chat_id, 'typing_on')

    def get_hide_link(self, text):
        return f"http://m.me/{self.group_id}?ref={text}"

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        self.bot.send_image_url(chat_id, url)

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        self.bot.send_audio_url(chat_id, url)

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        self.bot.send_video_url(chat_id, url)

    def send_raw(self, payload):
        request_endpoint = '{0}/me/messages'.format(self.bot.graph_url)
        response = requests.post(
            request_endpoint,
            params=self.bot.auth_args,
            json=payload
        )
        result = response.json()
        return result

    def send_replies(self, recipient_id, text, buttons):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            "messaging_type": "RESPONSE",
            "message": {
                "text": text,
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": btn.get('caption'),
                        "payload": f'trigger_id:{btn["payload"]["trigger_id"]}',
                    } for btn in buttons
                ]
            }
        }
        a = self.send_raw(payload)
        return a

    def send_card(self, user_id, title, text, image_url, keyboard=None):
        # print('send_card start')
        if not keyboard:
            keyboard = {}
        text_buttons = keyboard.get('text_buttons', [])
        url_buttons = keyboard.get('url_buttons', [])
        fast_buttons = keyboard.get('fast_buttons', [])

        if fast_buttons:
            return self.send_raw(self.send_replies(user_id, text, fast_buttons))
        if text_buttons is None:
            text_buttons = []
        if url_buttons is None:
            url_buttons = []

        buttons = [
            {"type": "postback", "title": button.get('caption'),
             "payload": f'trigger_text:{button.get("trigger_text")}'} for
            button in text_buttons]
        buttons = buttons + [{"type": "web_url", "url": button.get('url'), "title": button.get('caption')} for button in
                             url_buttons]

        if len(buttons) > 0:
            payload = {
                "recipient": {
                    "id": user_id
                },
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [{
                                "title": title,
                                "image_url": image_url,
                                "subtitle": text,
                                "buttons": buttons
                            }]
                        }
                    }
                }
            }
        else:
            payload = {
                "recipient": {
                    "id": user_id
                },
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [{
                                "title": title,
                                "image_url": image_url,
                                "subtitle": text
                            }]
                        }
                    }
                }
            }
        # print('payload fb: ', payload)
        print('result fb: ', self.bot.send_raw(payload))

    def send_gallery(self, user_id, gallery, keyboard=None, url=''):
        # print('send_card gal')
        if not keyboard:
            keyboard = {}
        text_buttons = keyboard.get('text_buttons', [])
        url_buttons = keyboard.get('url_buttons', [])
        fast_buttons = keyboard.get('fast_buttons', [])

        if fast_buttons:
            return self.send_raw(self.send_replies(user_id, text, fast_buttons))
        if text_buttons is None:
            text_buttons = []
        if url_buttons is None:
            url_buttons = []

        buttons = [
            {"type": "postback", "title": button.get('caption', '1'),
             "payload": f'trigger_text:{button.get("trigger_text")}'} for
            button in text_buttons]
        buttons = buttons + [{"type": "web_url", "url": button.get('url'), "title": button.get('caption')} for button in
                             url_buttons]

        if len(buttons) > 0:
            payload = {
                "recipient": {
                    "id": user_id
                },
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [{
                                "title": el.get('title', ' '),
                                "image_url": f'{API_URL}{el["photo"]}',
                                "subtitle": el.get('text', ' '),
                                "buttons": buttons
                            }
                                for el in gallery
                            ]
                        }
                    }
                }
            }
        else:
            payload = {
                "recipient": {
                    "id": user_id
                },
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [{
                                "title": el.get('title', ' '),
                                "image_url": f'{API_URL}{el["photo"]}',
                                "subtitle": el.get('text', ' ')
                            }
                                for el in gallery
                            ]
                        }
                    }
                }
            }
        # print('payload fb1: ', payload)
        print('result fb1: ', self.bot.send_raw(payload))

    def send_message(self, user_id, text, *args, keyboard=None, **kwargs):
        # print("text fb: ", text)
        if not keyboard:
            keyboard = {}
        text_buttons = keyboard.get('text_buttons', [])
        url_buttons = keyboard.get('url_buttons', [])
        fast_buttons = keyboard.get('fast_buttons', [])
        call_buttons = keyboard.get('call_buttons', [])

        if fast_buttons:
            return self.send_raw(self.send_replies(user_id, text, fast_buttons))
        if (text_buttons is None or len(text_buttons) <= 0) \
                and (url_buttons is None or len(url_buttons) <= 0):
            return self.bot.send_text_message(user_id, text)
        if text_buttons is None:
            text_buttons = []
        if url_buttons is None:
            url_buttons = []
        if url_buttons is None:
            call_buttons = []

        try:
            buttons = [
                {"type": "postback", "title": button.get('caption'),
                 "payload": f'trigger_text:{button.get("payload").get("trigger_id") if button.get("payload") else None}'}
                for
                button in text_buttons]
            buttons = buttons + [
                {"type": "web_url", "url": button.get("payload").get('url') if button.get("payload") else None,
                 "title": button.get('caption')} for button in url_buttons]
            buttons = buttons + [
                {"type": "phone_number",
                 "payload": button.get("payload").get('call') if button.get("payload") else None,
                 "title": button.get('caption')} for button in call_buttons]

            # print('buttons: ', buttons)
            result = self.bot.send_button_message(user_id, text, buttons)

            self.save_log(user_id, text, 'text')
            self.save_log(user_id, text_buttons, 'buttons')
            self.save_log(user_id, url_buttons, 'buttons')
            self.save_log(user_id, fast_buttons, 'buttons')
            return result
        except Exception as e:
            # print('err fb: ', e)
            return

    def send_file(self, user_id, filename=None, url=None):
        return self.bot.send_file_url(user_id, url)

    def get_dialogs_count(self):
        return len(set(msg.user_id for msg in FacebookReceivedMsg.objects.filter(manager=self.manager)))

    def proccess_event(self, event):
        # bots_logger.debug(f'FB event: {event}')
        for msg in event['messaging']:
            # print('msg: ', msg)
            # print('attachments: ', msg.get("message", {}).get("attachments"))

            if "quick_reply" in msg and msg["quick_reply"]["payload"].startswith("trigger_id"):
                trigger_id = int(msg['quick_reply']
                                 ['payload'].replace('trigger_id:', ''))
                trigger = self.find_trigger_by_id(trigger_id)

                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=msg['sender']['id'])
                self.trigger_routine(trigger.scenario, trigger, user)
                continue
            elif "text" in msg.get("message", {}):
                text, mid = msg["message"]["text"], msg["message"]["mid"]
            elif msg.get("postback") and msg.get("postback").get("referral"):
                # "referral" in msg:
                text, mid = msg.get("postback").get(
                    "referral").get("ref"), None
            elif "referral" in msg:
                text, mid = msg["referral"]["ref"], None
            elif "postback" in msg and msg["postback"]["payload"].startswith("trigger_text"):
                # print('postback!')
                text, mid = msg["postback"]["payload"].replace(
                    'trigger_text:', ''), None
                trigger = self.find_trigger_by_id(text)

                user = self.BotUsers.objects.get(
                    manager=self.manager, user_id=msg['sender']['id'])
                self.trigger_routine(trigger.scenario, trigger, user)
                # print('ttttext: ', text, ' ', trigger.scenario, ' ', trigger)
            elif msg.get("message", {}).get("attachments") \
                    and msg.get("message", {}).get("attachments")[0].get("type") == 'image':
                text, mid = msg["message"]["attachments"][0]["payload"].get(
                    "url", ''), msg["message"]["mid"]
            else:
                # print('empty')
                continue

            resp = requests.get(f'https://graph.facebook.com/{msg["sender"]["id"]}', params={
                'fields': 'first_name,last_name',
                'access_token': self.token
            }).json()

            resp['username'] = msg['sender']['id']
            message = FacebookReceivedMsg(
                manager=self.manager,
                message_id=mid,
                time=msg["timestamp"] / 1000,
                user_id=msg["sender"]["id"],
                chat_id=msg["sender"]["id"],
                text=text,
                user_data=json.dumps(resp)
            )
            message.save()
            self.proccess_message(message)
            message.save()

    def get_user_icon(self, link):
        import wget
        user = self.BotUsers.objects.filter(link=link).first()
        # resp = requests.post(f'https://graph.facebook.com/{user.user_id}/picture', params={
        #     "access_token":self.token
        #     # ,'type': 'large'
        # }).json()

        user.icon_url = f'https://graph.facebook.com/{user.user_id}/picture?type=large&access_token={self.token}'
        user.save()
        return user

    def file_get_contents(self, url):
        url = str(url).replace(" ", "+")  # just in case, no space in url
        hdr = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Accept-Language': 'en-US,en;q=0.8',
            'Connection': 'keep-alive'}
        req = urllib2.Request(url, headers=hdr)
        try:
            page = urllib2.urlopen(req)
            return page.read()
        except:
            bots_logger.debug('WhatsAppUsers save: ' +
                              str(traceback.format_exc()))
        return ''

    def check_tag1(self, tags, user_id, user=None):
        try:
            user_tags = UserTag.objects.filter(
                manager=self.manager, text__in=tags)

            if not user:
                user = BotUsers.objects.filter(user_id).first()

            if len(tags) == 1 and tags[0] == '' or user_tags.filter(user_tags_fb=user).exists():
                return True
            return False
        except:
            print(traceback.format_exc())
            return True


class WhatsAppBot(Bot):
    threads = {}
    BotDelayMsg = WhatsAppDelayMsg
    BotUsers = WhatsAppUsers
    BotReceivedMsg = WhatsAppReceivedMsg
    bot_place = "whatsapp"

    @staticmethod
    def can_work(manager):
        return manager.whatsapp_instance  # and manager.whatsapp_token

    def __init__(self, manager):
        self.manager = manager
        self.instance = manager.whatsapp_instance
        self.token = manager.whatsapp_token
        # self.endpoint = f"https://api.chat-api.com/instance{self.instance}"
        self.endpoint = f"http://109.68.212.178/{self.instance}/"

        if self.can_work(self.manager) and not WhatsAppReceivedMsg.objects.filter(manager=self.manager).exists():
            self.load_last_messages()

    def stop(self):
        self.manager.whatsapp = False
        # self.delete_webhook()
        # self.set_webhook("https://api.chatlead.io/app/whatsapp_webhook1")
        self.manager.save()
        self.wait_stopping()

    def start(self):
        if self.manager.whatsapp or self.is_any_work():
            self.stop()

        # self.set_webhook("https://api.chatlead.io/app/whatsapp_webhook")
        self.update_status()
        self.manager.whatsapp = True
        self.manager.save()

        # bots_logger.debug(f"start WA webhook manager_id: {self.manager.id}, token: {self.token}")

        self.run_threads([{"name": "handler", "target": self.message_handler}])

    def text_format(self, text, message):
        return text.replace(
            "{name}", message.sender_name
        ).replace(
            "{phone}", message.user_id.strip()[:-5]
        )

    def send_message(self, chat_id, text, *args, **kwargs):
        start = time.time()
        resp = requests.post(f"{self.endpoint}/sendMessage", params={
            "token": self.token, 'chatId': chat_id, 'body': text
        }).json()

        self.save_log(chat_id, text, 'text')
        # self.save_log(chat_id, text_buttons, 'buttons')
        # self.save_log(chat_id, url_buttons, 'buttons')
        # self.save_log(chat_id, fast_buttons, 'buttons')
        # print("whatsapp send_message", time.time() - start, resp)
        if not resp.get('sent', False):
            return False
        return True

    def set_webhook(self, url):
        resp = requests.post(f"{self.endpoint}/webhook",
                             params={"token": self.token, 'webhookUrl': url}).json()
        print("resp: ", resp)
        return resp.get("set", False)

    def delete_webhook(self):
        resp = requests.post(f"{self.endpoint}/webhook",
                             params={"token": self.token, 'webhookUrl': ''}).json()
        return resp.get("set", False)

    def get_attach(self, filename, url):
        if url:
            return f"https://chatlead.io/{url}"
        # if filename and os.path.exists(filename):
        #     with open(filename, 'rb') as file:
        #         return file.read()

    def send_file(self, chat_id, filename=None, url=None, **kwargs):
        # return True
        try:
            if not url:
                url = self.get_attach(filename, url)

            params = {"token": self.token, "chatId": chat_id,
                      "body": url, "filename": "file.jpg"}
            # print("params: ", params)
            res = requests.post(f"{self.endpoint}/sendFile",
                                params=params).json()
            # print('res send_file: ', res, url)

            return res['sent']

        except:
            print("send_file error", traceback.format_exc())
            return True

    def send_photo(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, url=url)

    def send_audio(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, url=url)

    def send_video(self, chat_id, filename=None, url=None, **kwargs):
        return self.send_file(chat_id, url=url)

    def broadcast(self, scenario, is_group=False, tag=""):
        # "metadata": {"isGroup": false}
        print("broadcast manager: whatsapp")
        count = 0
        try:
            dialogs = self.get_dialogs()

            trigger: Trigger = scenario.triggers.first()
            tags = tag.lower().split(',')

            for chat in dialogs:
                if count % 80 == 0:
                    bots_logger.debug('next 80')
                    time.sleep(60)
                if chat.get("metadata").get("isGroup") == is_group:
                    if is_group:
                        self.message_routine(trigger.messages, self.BotUsers(manager=self.manager, user_id=chat["id"]),
                                             scenario)
                        count += 1
                    else:
                        user = self.BotUsers(
                            manager=self.manager, user_id=chat["id"])

                        if tag != '' and not self.check_tag(tags, user):
                            print('skip')
                            continue

                        if user.is_unsub:
                            continue

                        self.message_routine(trigger.messages, user, scenario)
                        count += 1
        except Exception as e:
            print("broadcast whatsapp error: ", e)

            bots_logger.debug('WhatsAppUsers save: ' +
                              str(traceback.format_exc()))
        finally:
            # print("wa count: ", count)
            return count

    def send_link(self, chat_id, url, file_path, title, description):
        try:
            import base64
            with open(file_path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())

            data = {
                'previewBase64': encoded_string.decode("utf-8"),
                # 'token': self.token,
                'chatId': chat_id,
                'title': title,
                'description': description,
            }
            data = json.dumps(data)
            file = encoded_string.decode("utf-8")
            params = {
                'token': self.token,
                'body': url,
                'chatId': chat_id,
                'title': title,
                'description': description,
                'previewBase64': file if len(file) < 20000 else file[0:19999],
            }

            self.save_log(chat_id, url, 'text')
            return requests.post(f"{self.endpoint}/sendLink", params=params, data=data).json()
        except Exception as e:
            print('send_link error:', e)
            print(traceback.format_exc())
            return False

    def send_contact(self, chat_id, contact_id):
        try:
            print('wa send_contact')
            return requests.get(f"{self.endpoint}/sendContact", params={"token": self.token,
                                                                        "chatId": chat_id,
                                                                        "contactId": str(contact_id) + '@c.us'}).json()
        except:
            print("send_contact error: ", traceback.format_exc())
            return False

    def get_dialogs_count(self):
        try:
            return len(self.get_dialogs())
        except Exception:
            return 0

    def exports_dialogs(self):
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet["A1"] = "name"
        sheet["B1"] = "id"
        sheet["C1"] = "image"
        dialogs = self.get_dialogs()
        for i, chat in enumerate(dialogs, 2):
            sheet[f"A{i}"] = chat.get("name", "")
            sheet[f"B{i}"] = chat.get("id", "")
            sheet[f"C{i}"] = chat.get("image", "")
        wb.save(os.path.join(settings.DASE_DIR, "GUI", "static",
                             "dialogs", f"{self.manager.id}_whatsapp.xlsx"))

    def get_dialogs(self):
        return requests.get(f"{self.endpoint}/dialogs", params={"token": self.token}).json()["dialogs"]

    def read_chat(self, chat_id):
        if str(chat_id).find('@c.us') == -1:
            chat_id = str(chat_id) + '@c.us'
        return requests.get(f"{self.endpoint}/readChat", params={"token": self.token, "chatId": chat_id}).json()

    def get_screenshot(self):
        return requests.Request("GET", f"{self.endpoint}/screenshot", params={"token": self.token}).prepare().url

    def save_screenshot(self):
        with open(os.path.join(settings.BASE_DIR, "GUI", "static", "screenshots", f"{self.manager.id}.png"),
                  "wb") as file:
            file.write(requests.get(self.get_screenshot()).content)

    def get_qr(self):
        return requests.Request("GET", f"{self.endpoint}/qr_code", params={"token": self.token}).prepare().url

    def save_qr(self):
        with open(os.path.join(settings.BASE_DIR, "GUI", "static", "qr", f"{self.manager.id}.png"), "wb") as file:
            file.write(requests.get(self.get_qr()).content)

    def update_status(self):
        response = requests.get(
            f"{self.endpoint}/status", params={"token": self.token}).json()
        status = response.get("accountStatus", "")
        manager = Manager.objects.get(id=self.manager.id)
        manager.whatsapp_status = status
        manager.save()

    def logout(self):
        response = requests.get(
            f"{self.endpoint}/logout", params={"token": self.token}).json()
        status = response.get("accountStatus", "")
        manager = Manager.objects.get(id=self.manager.id)
        manager.whatsapp_status = status
        manager.save()

    def get_status(self):
        response = requests.get(
            f"{self.endpoint}/status", params={"token": self.token, "full": True}).json()
        status = response.get("accountStatus", "")
        substatus = response.get("statusData", {}).get("substatus", "")
        title = response.get("statusData", {}).get("title", "")
        msg = response.get("statusData", {}).get("msg", "")
        submsg = response.get("statusData", {}).get("submsg", "")
        link = response.get("statusData", {}).get(
            "actions", {}).get("learn_more", {}).get("link", "")
        logout = response.get("statusData", {}).get(
            "actions", {}).get("logout", {}).get("act", "")
        return {"status": status,
                "substatus": substatus,
                "title": title,
                "msg": msg,
                "submsg": submsg,
                "link": link,
                "logout": logout}

    def get_updates(self):
        lastMessageNumber = WhatsAppReceivedMsg.objects.filter(manager=self.manager).aggregate(
            models.Max('message_number')
        )['message_number__max'] or 0

        wa = WhatsAppReceivedMsg.objects.filter(
            manager=self.manager, message_number=lastMessageNumber).first()
        messages = None

        while True:
            messages = requests.get(f"{self.endpoint}/messages", params={
                "token": self.token,
                "lastMessageNumber": lastMessageNumber,
            }).json()

            if 'error' in messages:
                # print('raise ConnectionRefusedError!')
                raise ConnectionRefusedError()

            if len(messages['messages']) == 0:
                return messages['messages']

            for message in messages['messages']:
                if message["fromMe"] or message["chatId"] != message["author"]:
                    continue
                else:
                    return messages['messages']

            lastMessageNumber += len(messages['messages'])

        return messages['messages'] if messages else ''

    def proccess_messages_without_answers(self):
        lastMessageNumber = 0
        received = []
        sended = []
        while True:
            response = requests.get(f"{self.endpoint}/messages", params={
                "token": self.token,
                "lastMessageNumber": lastMessageNumber
            }).json()
            # print(response.get("lastMessageNumber", 0))
            if lastMessageNumber >= response.get("lastMessageNumber", 0):
                break
            lastMessageNumber = response["lastMessageNumber"]
            # print(lastMessageNumber)

            for msg in response["messages"]:
                # print(len(sended), len(received))
                if msg["fromMe"]:
                    sended.append(msg)
                else:
                    received.append(msg)

        for msg in received:
            proc = False
            for send_msg in sended:
                if send_msg["chatId"] == msg["chatId"] and send_msg["time"] >= msg["time"]:
                    # print(msg["messageNumber"], "could be proccessed by", send_msg["messageNumber"])
                    proc = True
                    break
            if not proc:
                if not WhatsAppReceivedMsg.objects.filter(
                        manager=self.manager, message_number=msg["messageNumber"], chat_id=msg["chatId"]
                ).exists():
                    # print("new proccess", msg["messageNumber"])
                    message = WhatsAppReceivedMsg(
                        manager=self.manager,
                        text=msg["body"],
                        chat_id=msg["chatId"],
                        user_id=msg["author"],
                        sender_name=msg["senderName"],
                        time=msg["time"],
                        message_number=msg["messageNumber"],
                        user_data=json.dumps({'username': msg["senderName"]}),
                    )
                    # print("proccess0000000000000000000000")
                    message.save()
                    result = self.proccess_message(message)
                    message.proccessed = result
                    message.save()
                else:
                    # print("proccess11111111111111111111111111", msg["messageNumber"])
                    result = self.proccess_message(WhatsAppReceivedMsg.objects.get(
                        manager=self.manager, message_number=msg["messageNumber"], chat_id=msg["chatId"]
                    ))

    def load_last_messages(self):
        messages = requests.get(f"{self.endpoint}/messages", params={
            "token": self.token, "last": "true",
        }).json()["messages"]
        for message in messages:
            if not WhatsAppReceivedMsg.objects.filter(manager=self.manager,
                                                      message_number=message["messageNumber"]).exists():
                WhatsAppReceivedMsg(
                    manager=self.manager,
                    text=message["body"],
                    chat_id=message["chatId"],
                    user_id=message["author"],
                    sender_name=message["senderName"],
                    time=message["time"],
                    message_number=message["messageNumber"],
                    user_data=json.dumps({'username': message["senderName"]}),
                    proccessed=True,
                ).save()
            else:
                msg = WhatsAppReceivedMsg.objects.get(
                    manager=self.manager, message_number=message["messageNumber"])
                msg.proccessed = True
                msg.save()

    def proccess_event(self, messages):
        try:
            for message in messages:
                try:
                    if not self.manager.whatsapp:
                        break
                    if message["fromMe"] or message["chatId"] != message["author"]:
                        continue

                    msg = WhatsAppReceivedMsg(
                        manager=self.manager,
                        text=message["body"],
                        chat_id=message["chatId"],
                        user_id=message["author"],
                        sender_name=message["senderName"],
                        time=message["time"],
                        message_number=message["messageNumber"],
                        user_data=json.dumps(
                            {'username': message["senderName"]}),
                    )
                    msg.save()
                    result = self.proccess_message(msg)
                    if result:
                        msg.proccessed = result
                        msg.save()
                except Exception:
                    print(traceback.format_exc())
        # time.sleep(1)
        except ConnectionRefusedError:
            self.manager.whatsapp = False
            self.manager.save()
            return
        except Exception:
            print(traceback.format_exc())
            return

    def message_handler(self):
        return
        while self.manager.whatsapp:
            a = 1
            # try:
            #     for message in self.get_updates():
            #
            #         try:
            #             if not self.manager.whatsapp:
            #                 break
            #             if message["fromMe"] or message["chatId"] != message["author"]:
            #                 continue
            #             print('mw: ', message)
            #             print("WhatsAppReceivedMsg: ", message["chatId"],
            #                   datetime.now().timestamp() - int(message["time"]))
            #
            #             # last_message = get_last_main_message(self, self.manager, message["author"],
            #             #                                      int(message["messageNumber"]) - 1, message["body"])
            #             msg = WhatsAppReceivedMsg(
            #                 manager=self.manager,
            #                 text=message["body"],
            #                 # if (last_message and 'wa_' in last_message.text) or not last_message else 'wa_' + message["body"],
            #                 chat_id=message["chatId"],
            #                 user_id=message["author"],
            #                 sender_name=message["senderName"],
            #                 time=message["time"],
            #                 message_number=message["messageNumber"],
            #                 user_data=json.dumps({'username': message["senderName"]}),
            #             )
            #
            #             result = self.proccess_message(msg)
            #             if result:
            #                 msg.proccessed = result
            #                 msg.save()
            #         except Exception:
            #             print(traceback.format_exc())
            # # time.sleep(1)
            # except ConnectionRefusedError:
            #     self.manager.whatsapp = False
            #     self.manager.save()
            #     return
            # except Exception:
            #     print(traceback.format_exc())
            #     return

    def get_user_icon(self, link):
        user = self.BotUsers.objects.filter(link=link).first()

        try:
            for dialog in self.get_dialogs():
                # print("id dialogs:", dialog)
                if dialog.get('id') == user.user_id:
                    print("my dialogs:", dialog)
                    user.icon_url = dialog.get('image')
                    user.save()
                    break
        except:
            pass
        return user

    def check_tag1(self, tags, user_id, user=None):
        try:
            user_tags = UserTag.objects.filter(
                manager=self.manager, text__in=tags)

            if not user:
                user = BotUsers.objects.filter(user_id).first()

            if len(tags) == 1 and tags[0] == '' or user_tags.filter(user_tags_wa=user).exists():
                return True
            return False
        except:
            print(traceback.format_exc())
            return True


class BotsManager():
    bots_manager = {}
    bots_classes = {
        "whatsapp": WhatsAppBot,
        "telegram": TelegramBot,
        "vk": VkBot,
        "facebook": FacebookBot,
    }

    @classmethod
    def init(cls, manager, bots="all"):
        if bots == "all":
            bots = ["whatsapp", "telegram", "vk", "facebook"]

        # cls.stop(manager, bots)

        for bot_place in bots:
            cls.bots_manager.setdefault(manager.id, {}).update({
                bot_place: cls.bots_classes[bot_place](manager)
            })

    @classmethod
    def start(cls, manager, bots="all"):
        if bots == "all":
            bots = ["whatsapp", "telegram", "vk", "facebook"]

        for bot_place in bots:
            if bot_place not in cls.bots_manager.setdefault(manager.id, {}):
                if not cls.bots_classes[bot_place].can_work(manager):
                    setattr(manager, bot_place, False)
                    manager.save()
                    continue
                cls.init(manager, bots=[bot_place])
            cls.bots_manager.get(manager.id, {}).get(bot_place, None).start()

    @classmethod
    def stop(cls, manager, bots="all"):
        if bots == "all":
            bots = ["whatsapp", "telegram", "vk", "facebook"]
        # bots_logger.debug(f"Stop in bots manager, manager_id: {manager.id}, bots: {bots}")

        for bot_place in bots:
            if bot_place in cls.bots_manager.get(manager.id, {}):
                cls.bots_manager.get(manager.id, {}).get(
                    bot_place, None).stop()

    @classmethod
    def fix_work_status(cls, manager, bots="all"):
        if bots == "all":
            bots = ["whatsapp", "telegram", "vk", "facebook"]

        for bot_place in bots:
            if bot_place not in cls.bots_manager.setdefault(manager.id, {}):
                if not cls.bots_classes[bot_place].can_work(manager):
                    setattr(manager, bot_place, False)
                    manager.save()
                    continue
                cls.init(manager, bots=[bot_place])
            cls.bots_manager.get(manager.id, {}).get(
                bot_place, None).fix_work_status()

    @classmethod
    def broadcast(cls, manager, scenario, is_group=False, tag="", bots="all"):
        print("broadcast manager: ", bots)
        if bots == "all":
            bots = ["telegram", "vk", "facebook", "whatsapp"]
            # bots = ["whatsapp"]
        count = 0
        for bot_place in bots:
            try:
                if manager.get_bot(bot_place).can_work(manager):
                    bots_logger.debug('bot_place start: ' + bot_place)
                    trigger = scenario.triggers.all()
                    print(trigger.exists())
                    if not trigger.exists():
                        return 0
                else:
                    print(bot_place + ' is cant work')
                    continue

                count += manager.get_bot(bot_place).broadcast(scenario,
                                                              is_group, tag)
                print('count: ', count)
            except Exception as e:
                print('111111111: ', traceback.format_exc())
                bots_logger.debug(str(e))
        return count

    @classmethod
    def get_dialogs_count(cls, manager, bots="all"):
        if bots == "all":
            bots = ["whatsapp", "telegram", "vk", "facebook"]

        count = 0
        for bot_place in bots:
            if bot_place in cls.bots_manager.setdefault(manager.id, {}):
                count += cls.bots_manager.get(manager.id, {}
                                              ).get(bot_place, None).get_dialogs_count()
        return count


class Manager(models.Model):
    user = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")

    bitrix_key = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    bitrix_domain = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    amocrm_domain = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    application_email = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    application_whatsapp_id = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    application_telegram_id = models.CharField(max_length=1024, null=True, blank=True, unique=False, default="")
    application_will_send = models.BooleanField(default=False)
    whatsapp = models.BooleanField(default=False)
    telegram = models.BooleanField(default=False)
    vk = models.BooleanField(default=False)
    facebook = models.BooleanField(default=False)

    whatsapp_status = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    whatsapp_token = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    whatsapp_instance = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")

    telegram_token = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    telegram_name = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")

    vk_group_id = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    vk_group_access_token = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    vk_name = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    vk_secret_code = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    vk_confirmation_code = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")

    facebook_token = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    facebook_group_id = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")
    facebook_name = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")

    send_welcome_notif = models.BooleanField(default=False)
    welcome_notif_text = models.CharField(max_length=512, null=True, blank=True, unique=False, default="")
    welcome_admin_id = models.CharField(max_length=256, null=True, blank=True, unique=False, default="")

    default_response = models.CharField(max_length=4096, null=True, blank=True, unique=False, default="")
    welcome_message = models.CharField(max_length=4096, null=True, blank=True, unique=False, default="")
    subscribe_message = models.CharField(max_length=4096, null=True, blank=True, unique=False, default="")
    unsubscribe_message = models.CharField(max_length=4096, null=True, blank=True, unique=False, default="")
    payed = models.BooleanField(default=False)
    plan = models.CharField(max_length=30, null=True, blank=True, default="trial")
    payed_end_date = models.IntegerField(null=True, blank=True, unique=False, default=3)
    last_dec_date = models.DateField(null=True, blank=True, unique=False, default=datetime.today())

    def __str__(self):
        return f"[#{self.id}]"

    def get_pay_link(self, amount=100, description="Бот"):
        return f"https://api.paybox.money/payment.php?pg_merchant_id=518428&pg_amount={amount}&pg_currency=KZT&pg_description={description}&pg_salt=S1IpuJvrOSQqqjph&pg_language=ru&pg_sig=44fea46f0f958736d97bae83ff4e071b"

    def get_absolute_url(self):
        return reverse('manager', args=[str(self.id)])

    def get_bot(self, bot):
        return BotsManager.bots_manager.get(self.id, {}).get(bot, BotsManager.bots_classes.get(bot, None)(self))

    @property
    def get_whatsapp(self):
        return self.get_bot("whatsapp")

    def start(self, bots="all"):
        BotsManager.start(self, bots=bots)

    def stop(self, bots="all"):
        BotsManager.stop(self, bots=bots)

    def fix_work_status(self, bots="all"):
        BotsManager.fix_work_status(self, bots=bots)

    def broadcast(self, scenario, is_group=False, tag="", bots="all"):
        return BotsManager.broadcast(self, scenario, is_group=is_group, tag=tag, bots=bots)

    def get_dialogs_count(self, bots="all"):
        return BotsManager.get_dialogs_count(self, bots=bots)

    def reload_bot(self, bot):
        if BotsManager.bots_manager.get(self.id, {}).get(bot):
            BotsManager.bots_manager.get(self.id, {})[bot].stop()
        BotsManager.bots_manager.get(self.id, {})[
            bot] = BotsManager.bots_classes[bot](self)


class PayRef(models.Model):
    user = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)
    amount = models.DecimalField(default=0, max_digits=11, decimal_places=2)
    card_number = models.CharField(max_length=30)
    type = models.CharField(max_length=50, null=True, blank=True)
    request_date = models.IntegerField(default=date_today_to_timeshtamp)
    is_approved = models.BooleanField(default=False)
