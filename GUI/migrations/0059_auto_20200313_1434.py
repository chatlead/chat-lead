# Generated by Django 2.2.2 on 2020-03-13 08:34

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0058_auto_20200313_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='facebookusers',
            name='is_unsub',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='facebookusers',
            name='unsub_date',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='telegramusers',
            name='is_unsub',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='telegramusers',
            name='unsub_date',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='vkusers',
            name='is_unsub',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='vkusers',
            name='unsub_date',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='whatsappusers',
            name='is_unsub',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='whatsappusers',
            name='unsub_date',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 3, 13, 14, 34, 47, 279660), null=True),
        ),
        migrations.AlterField(
            model_name='mlp',
            name='create_date',
            field=models.PositiveIntegerField(default=1584088487.277602),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1584088487.254442),
        ),
    ]
