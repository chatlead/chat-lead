# Generated by Django 2.2.2 on 2020-03-01 20:04

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0050_auto_20200228_1954'),
    ]

    operations = [
        migrations.AddField(
            model_name='facebookreceivedmsg',
            name='message_type',
            field=models.CharField(default='text', max_length=50),
        ),
        migrations.AddField(
            model_name='telegramreceivedmsg',
            name='message_type',
            field=models.CharField(default='text', max_length=50),
        ),
        migrations.AddField(
            model_name='vkreceivedmsg',
            name='message_type',
            field=models.CharField(default='text', max_length=50),
        ),
        migrations.AddField(
            model_name='whatsappreceivedmsg',
            name='message_type',
            field=models.CharField(default='text', max_length=50),
        ),
        migrations.AlterField(
            model_name='formlogging',
            name='submit_date',
            field=models.DateField(default=datetime.date(2020, 3, 2)),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 3, 2, 2, 4, 35, 858790), null=True),
        ),
        migrations.AlterField(
            model_name='mlp',
            name='create_date',
            field=models.PositiveIntegerField(default=1583093075.857066),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1583093075.832965),
        ),
    ]
