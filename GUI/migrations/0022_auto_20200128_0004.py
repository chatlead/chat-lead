# Generated by Django 2.2.2 on 2020-01-27 18:04

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GUI', '0021_auto_20200125_0426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formlogging',
            name='submit_date',
            field=models.DateField(default=datetime.date(2020, 1, 28)),
        ),
        migrations.AlterField(
            model_name='manager',
            name='last_dec_date',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 1, 28, 0, 4, 7, 250537), null=True),
        ),
        migrations.AlterField(
            model_name='transactions',
            name='add_date',
            field=models.IntegerField(default=1580148247.231414),
        ),
    ]
